import { Component, OnInit } from '@angular/core';
import { PessoasCentralRxjsService } from '../../services/pessoasCentralRxjsService.service';

@Component({
  selector: 'ons-comandbar-pessoas',
  templateUrl: './comandBarPessoas.component.html',
  styleUrls: ['./comandBarPessoas.component.scss']
})


export class ComandBarPessoasComponent implements OnInit {
  Pesquisa: string;
  constructor(public observerSrv: PessoasCentralRxjsService) {
    this.observerSrv.pesquisaGrid.subscribe((s) => {
      console.log(s);
    });


  }

  ngOnInit() {
  }

  eventPesquisa(valorDePesquisa) {
    // console.log(valorDePesquisa);
    this.observerSrv.pesquisaGrid = valorDePesquisa;
  }

}
