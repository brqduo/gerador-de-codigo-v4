import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ons-pessoa-entity',
  templateUrl: 'pessoaEntity.component.html',
  styleUrls: ['./pessoaEntity.component.scss']
})

export class PessoaEntityComponent implements OnInit {
  constructor(public route: Router) {}
  ngOnInit() { }


  GotoHome() {
    this.route.navigateByUrl('/home');
  }
}

