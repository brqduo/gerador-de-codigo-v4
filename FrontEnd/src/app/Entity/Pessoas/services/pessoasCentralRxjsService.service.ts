import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class PessoasCentralRxjsService {
  private _pesquisaGrid = new Subject<string>();
  constructor() { }


  public set pesquisaGrid(valorParaPesquisa: any) {
    this._pesquisaGrid.next(valorParaPesquisa);
  }

  public get pesquisaGrid() {
    return this._pesquisaGrid;
  }

}

