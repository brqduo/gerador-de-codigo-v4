import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ons-pesquisa-usuarios',
  templateUrl: './pesquisaUsuarios.component.html',
  styleUrls: ['./pesquisaUsuarios.component.scss']
})
export class PesquisaUsuariosComponent implements OnInit {
  buscaAvancada: any = true;
  inputPesquisa1: any;
  inputPesquisa2: any;
  inputPesquisa3: any;
  inputPesquisaAvancada: any;
  animateStringAvancada = 'hideAdvancedSearch';
  animateStringSimples = 'showAdvancedSearch';
  btnPesquisaLabel = 'Pesquisa Avançada';
  Component: any;
  constructor() { }

  ngOnInit() {
  }



  pesquisar(target) {
    if (this.inputPesquisa1 !== undefined && this.inputPesquisa1.trim() !== '') {
      const valores = {
        inputPesquisa: this.inputPesquisa1.trim(),
        inputPesquisaAvancada: ((this.inputPesquisaAvancada === undefined) ? '' : this.inputPesquisaAvancada)
      };
      console.log(valores);
      // this.utilSrv.ChamarFuncao(valores, this.ComponentId, Config.OBJETOS_GRID.GRID_FILTRO_DESCRICAO, this.PesquisaId);
      // this.utilSrv.ChamarFuncao(valores, this.ComponentId, Config.IDSUBJECT.pesquisa, this.PesquisaId);
    } else {
      // this.utilSrv.alerta('Campo pesquisa não pode ser vazio.', 'Pesquisa Vazia', Config.TIPOMENSAGEM.error);
    }
  }

  pesquisaAvancada() {
    console.log('o q eu recebo?', this.buscaAvancada);

    if (this.buscaAvancada === true) {
      this.btnPesquisaLabel = 'Pesquisa Simples';
      this.animateStringSimples = 'hideAdvancedSearch';
      this.animateStringAvancada = 'showAdvancedSearch';
      this.buscaAvancada = false;
    } else {
      this.animateStringAvancada = 'hideAdvancedSearch';
      this.animateStringSimples = 'showAdvancedSearch';
      this.btnPesquisaLabel = 'Pesquisa Avançada';
      this.buscaAvancada = true;
    }

    /*    setTimeout(() => {
         this.buscaAvancada = !this.buscaAvancada;
         this.animateString = 'showAdvancedSearch';
       }, 300); */
  }

  onEnterKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
      // console.log(event.target.value);
    }
  }

  onKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
      // console.log(event.target.value);
    }
    // (keyup)="onKeyDown($event)"
  }
}
