

import { Component, OnInit } from '@angular/core';
import { GridOptions, ColDef, GridApi } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { UsuariosCentralRxjsService } from '../../services/usuariosCentralRxjsService.service';
@Component({
  selector: 'ons-grid-usuarios',
  templateUrl: './gridUsuarios.component.html',
  styleUrls: ['./gridUsuarios.component.scss']
})
export class GridUsuariosComponent implements OnInit {
  grid: GridOptions;
  filtro = '';
  constructor(
    public http: HttpClient,
    public observerSrv: UsuariosCentralRxjsService
  ) {
    this.observerSrv.pesquisaGrid.subscribe((res: string) => {
      console.log(res);
      this.filtro = res;
      this.grid.api.onFilterChanged();
    });

  }

  ngOnInit() {
    this.grid = {
      rowSelection: 'single',
      defaultColDef: {
        sortable: true
      },
      rowData: null,
      columnDefs: this.inicializarColunas(),
      isExternalFilterPresent: this.externalFilterPresent.bind(this),
      doesExternalFilterPass: this.externalFilterPass.bind(this),
      onGridReady: ((params) => {
        params.api.sizeColumnsToFit();
      })
    };
    this.start();
  }

  externalFilterPresent() {
    return this.filtro.toLowerCase() !== '';
  }

  externalFilterPass(node: any) {
    if (this.filtro !== '') {
      if (((node.data.UsuarId + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
        ((node.data.PrivilegioId + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
        ((node.data.UsuarNome + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
        ((node.data.UsuarSenha + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1)
      ) {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  inicializarColunas() {
    const coluna: ColDef[] = [
      {
        headerName: 'UserID',
        field: 'UsuarId',
        width: 100
      },
      {
        headerName: 'Privilegio',
        field: 'PrivilegioId',
        width: 100
      },
      {
        headerName: 'Nome do Usuario',
        field: 'UsuarNome',
        width: 100
      },
      {
        headerName: 'Senha do Usuario',
        field: 'UsuarSenha',
        width: 100
      },
    ];

    return coluna;
  }


  async start() {
    const res = await this.http.get<any>('https://my.api.mockaroo.com/getData?key=a5e40240').toPromise();
    this.grid.api.setRowData(res);
  }
}
