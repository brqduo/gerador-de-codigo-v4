import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ons-usuarioentity',
  templateUrl: 'usuarioEntity.component.html',
  styleUrls: ['./usuarioEntity.component.scss']
})

export class UsuarioEntityComponent implements OnInit {
  constructor(public route: Router) { }

  ngOnInit() { }

  GotoHome() {
    this.route.navigateByUrl('/home');
  }
}
