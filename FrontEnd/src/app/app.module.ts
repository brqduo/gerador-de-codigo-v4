import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { UsuarioEntityModule } from './Entity/Usuarios/usuarioEntity.module';
import { PessoaEntityModule } from './Entity/Pessoas/pessoaEntity.module';

// Ag-Grid
import { AgGridModule } from 'ag-grid-angular';

// Bibliotecas de Ui, ou Scss frameworks
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimeNgModule } from './PrimeNg.module';

// Home
import { HomeComponent } from './Home/home.component';
import { FormsModule } from '@angular/forms';
import { PessoasCentralRxjsService } from './Entity/Pessoas/services/pessoasCentralRxjsService.service';
import { UsuariosCentralRxjsService } from './Entity/Usuarios/services/usuariosCentralRxjsService.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    AgGridModule.withComponents([]),
    FormsModule,
    // Bibliotecas de Ui
    MDBBootstrapModule.forRoot(),
    PrimeNgModule,
    BrowserAnimationsModule,
    // Modulos de Entidade
    /*    UsuarioEntityModule.forRoot(),
       PessoaEntityModule.forRoot(), */
    BrowserModule,
    RoutingModule
  ],
  providers: [PessoasCentralRxjsService, UsuariosCentralRxjsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
