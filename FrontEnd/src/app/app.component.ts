import { Component } from '@angular/core';

@Component({
  selector: 'ons-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'GeradorV4';
}
