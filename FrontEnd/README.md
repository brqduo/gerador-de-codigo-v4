# GeradorV4

Este projeto foi gerado usando a versão 7.3.0 do Angular CLI
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Executando para desenvolvimento

Execute o comando  `ng serve`. Navegue até `http://localhost: 4200`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

## ↣ Arquitetura

- Os componentes estão usando Lazy Loading, o que deixa o aplicativo mais rapido, ja que ele carrega apenas o que o usuario usa, e não tudo de uma vez
- Possui uma central de redirecionamento das funções, impedindo assim repetição de codigo desnescessario. Cada Entidade possui a sua propria central
- Usa como motor de grid o Ag-Grid. Um potente pacote npm especializado em grids, podendo trabalhar com até mais de 10 mil itens em tela.
 - Com o navBar da biblioteca UI Material Design Bootstrap(MDB), as telas ficam totalmente responsivas. A NavBar irá se modificar automaticamente para um HamburguerMenu caso a viewPort fique menor que 900px
 - Usando como base os Botões do biblioteca UI PrimeNg, criamos um set de Botões feitos para esta aplicação, usando a palheta de cores do ONS

## ↣ Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.



## ↣ Para mais ajuda

Para maiores informações ou ajuda, procure a equipe de mobile da BRQ
