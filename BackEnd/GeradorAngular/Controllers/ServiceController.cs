﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using GeradorAngular.Models;
using Newtonsoft.Json;
using ons.intunica.entidade;
using RestSharp;

namespace GeradorAngular.Controllers
{
    [System.Web.Http.RoutePrefix("api/service")]
    public class ServiceController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("data/{skipD}/{topD}")]
        // GET api/<controller>
        public async Task<List<object>> Get(int skipD,int topD)
        {


            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:","").Replace(" ", "").Trim();

            return GetLista(cookie, skipD, topD);

        }

        private List<object> GetLista(string cookie,int skipD, int topD)
        {
          
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Get?skip="+ skipD + "&top="+ topD);
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();

            var responseText = ReadResponse(response);

            var result = JsonConvert.DeserializeObject<List<object>>(responseText); //response.Content
            return result;
        }

        private List<object> GetListaFiltroSimples(int skipD, int topD, string filtroSimples)
        {

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Get?skip=" + skipD + "&top=" + topD);
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    TextoFiltroSimples = filtroSimples
                });

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<List<object>>(result);
            }
        }


        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("data")]
        private int GetCount()
        {
            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/GetCount");
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";
            request.Referer = "http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/PerfUsuLista.html";
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();

            var responseText = ReadResponse(response);

            var result = Int32.Parse(responseText); //response.Content
            return result;
        }

        private Token Authentication()
        {
            var client = new RestClient("http://popdsv.ons.org.br/ons.pop.federation/oauth2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "99cc9fd9-0c30-499c-b689-cfb6f7cb7a56");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Origin", "http://local.ons.org.br:8100");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("undefined", "username=ONS%5CNilton.brq&password=MOBILEteam2018!!&client_id=Mobile.Contatos&grant_type=password&undefined=", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var result = JsonConvert.DeserializeObject<Token>(response.Content);
            return result;

        }

        private JwtSecurityToken getDecodeToken(string authorizationField)
        {
            var handler = new JwtSecurityTokenHandler();
            return handler.ReadToken(authorizationField) as JwtSecurityToken;
        }

        private System.Security.Claims.Claim getClaim(string key, JwtSecurityToken token)
        {
            System.Security.Claims.Claim claim = null;
            foreach (System.Security.Claims.Claim c in token.Claims)
            {
                if (c.Type == key)
                {
                    claim = c;
                    break;
                }
            }
            return claim;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                }
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                {
                    streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }
    }
}