﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using GeradorAngular.Models;
using Newtonsoft.Json;
using ons.intunica.entidade;
using RestSharp;

namespace GeradorAngular.Controllers
{

    public class PerfUsu2Controller : ApiController
    {
        // GET api/<controller>
        public int Get()
        {
            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/GetCount");
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";
            request.Referer = "http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/PerfUsuLista.html";
            request.ContentLength = 0;
            var response = (HttpWebResponse)request.GetResponse();

            var responseText = ReadResponse(response);

            var result = Int32.Parse(responseText); //response.Content
            return result;
        }


        // POST api/<controller>
        public List<object> Post([FromUri] int skipD, [FromUri] int topD, [FromBody] PerfUsuFiltro filtroSimples)
        {

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Get?skip=" + skipD + "&top=" + topD);
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    TextoFiltroSimples = filtroSimples.TextoFiltroSimples
                });

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<List<object>>(result);
            }


        }

        public object Post([FromBody] PerfUsu perfUsu)
        {
            System.Net.ServicePointManager.Expect100Continue = false;

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Post");
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(perfUsu);

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<object>(result);
            }


        }

        public PerfUsu Post(Int32 key)
        {
            System.Net.ServicePointManager.Expect100Continue = false;

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/GetItem?key=" + key);
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "GET";
            request.ContentType = "application/json";

            request.ContentLength = 0;


            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<PerfUsu>(result);
            }


        }

        // PUT api/<controller>/5
        public object Put(int id, [FromBody]PerfUsu perfUsu)
        {
            System.Net.ServicePointManager.Expect100Continue = false;

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Patch?key=" + id);
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "PATCH";
            request.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(perfUsu);

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<object>(result);
            }
        }

        // DELETE api/<controller>/5
        public object Delete([FromBody]List<PerfUsu> ListaperfUsu)
        {

            System.Net.ServicePointManager.Expect100Continue = false;

            var acessTk = Authentication();
            var token = getDecodeToken(acessTk.access_token);
            var cookie = ConfigurationManager.AppSettings["CookieTag"] + "=" +
                         getClaim("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket", token).ToString().Replace("http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket:", "").Replace(" ", "").Trim();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://local.ons.org.br/ons.intunica.iu.web/Views/PerfUsu/Excluir");
            request.Headers.Set(HttpRequestHeader.Cookie, cookie);
            request.Method = "POST";
            request.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(ListaperfUsu);

                streamWriter.Write(json);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return JsonConvert.DeserializeObject<object>(result);
            }
        }

        private Token Authentication()
        {
            var client = new RestClient("http://popdsv.ons.org.br/ons.pop.federation/oauth2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "99cc9fd9-0c30-499c-b689-cfb6f7cb7a56");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Origin", "http://local.ons.org.br:8100");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("undefined", "username=ONS%5CNilton.brq&password=MOBILEteam2018!!&client_id=Mobile.Contatos&grant_type=password&undefined=", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var result = JsonConvert.DeserializeObject<Token>(response.Content);
            return result;

        }

        private JwtSecurityToken getDecodeToken(string authorizationField)
        {
            var handler = new JwtSecurityTokenHandler();
            return handler.ReadToken(authorizationField) as JwtSecurityToken;
        }

        private System.Security.Claims.Claim getClaim(string key, JwtSecurityToken token)
        {
            System.Security.Claims.Claim claim = null;
            foreach (System.Security.Claims.Claim c in token.Claims)
            {
                if (c.Type == key)
                {
                    claim = c;
                    break;
                }
            }
            return claim;
        }

        private static string ReadResponse(HttpWebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                Stream streamToRead = responseStream;
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    streamToRead = new GZipStream(streamToRead, CompressionMode.Decompress);
                }
                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                {
                    streamToRead = new DeflateStream(streamToRead, CompressionMode.Decompress);
                }

                using (StreamReader streamReader = new StreamReader(streamToRead, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }
    }
}