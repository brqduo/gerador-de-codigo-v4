using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.intunica.comum.ioc;
using ons.intunica.comum;
using ons.intunica.comum.Reflection;
using ons.intunica.servico.ioc;
using ons.intunica.negocio.contrato;
using ons.intunica.entidade;
using ons.intunica.servico.ioc.webapi;
using GeradorAngular.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace GeradorAngular.Controllers
{

    /// <summary>
    /// Controller API para: 
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
    [System.Web.Http.RoutePrefix("api/perfusu")]
    public partial class PerfUsuController : ons.intunica.servico.ioc.webapi.BaseApiController
    {
        private IPerfUsuNegocio PerfUsuNegocio;
        private ons.common.providers.IHelperPOP PopHelper;
        private IPerfilNegocio PerfilNegocio;
        private ICosNegocio CosNegocio;
        private IUsuarNegocio UsuarNegocio;

        public PerfUsuController(ons.common.providers.IHelperPOP popHelper, IPerfUsuNegocio perfUsuNegocio, IPerfilNegocio perfilNegocio, IUsuarNegocio usuarNegocio, ICosNegocio cosNegocio)
        {
            PopHelper = popHelper;
            PerfUsuNegocio = perfUsuNegocio;
            PerfilNegocio = perfilNegocio;
            UsuarNegocio = usuarNegocio;
            CosNegocio = cosNegocio;

        }

        [POPAuthorize("Consultar PerfUsu")]
        [HttpPost]
        public IEnumerable<dynamic> Get(PerfUsuFiltro Filtro, int skip = 0, int top = 0)
        {
            return PerfUsuNegocio.Query().Include(a => a.Perfil).Include(a => a.Usuar).Where(Filtro).Page(skip, top).Select(a => new { a.PerfusuId, a.UsuarId, a.PerfilId, AgeId = a.AgeId == null ? null : a.Age.NomeCurto, CosId = a.CosId == null ? null : a.Cos.NomeCurto, Perfil = a.PerfilId == null ? null : a.Perfil.Nome, Usuar = a.UsuarId == null ? null : a.Usuar.UsuarNome }).ToList().Select(a => new { a.PerfusuId, a.UsuarId, a.PerfilId, a.AgeId, a.CosId, a.Perfil, a.Usuar }).ToList();
        }

        [POPAuthorize("Consultar PerfUsu")]
        [HttpPost]
        public int GetCount(PerfUsuFiltro Filtro)
        {
            return PerfUsuNegocio.Query().Where(Filtro).Count();
        }

        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar PerfUsu"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar PerfUsu"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar PerfUsu"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir PerfUsu"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar PerfUsu")
            };
        }

        [POPCRUDAPIAuthorize()]
        public PerfUsu GetItem(int key)
        {

            return PerfUsuNegocio.Query().Where(item => item.PerfusuId.Equals(key)).ToList().Select(a => new PerfUsu() { PerfusuId = a.PerfusuId, UsuarId = a.UsuarId, PerfilId = a.PerfilId, AgeId = a.AgeId, CosId = a.CosId }).FirstOrDefault();
        }
        private PerfUsu ObterItem(int key)
        {
            return PerfUsuNegocio.Query().Where(item => item.PerfusuId.Equals(key)).FirstOrDefault();
        }

        [POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            return new
            {

                PerfilLista = PerfilNegocio.Query().OrderBy(a => a.Nome).Select(a => new { Valor = a.PerfilId, Texto = a.Nome }).ToList(),
                CosLista = CosNegocio.Query().Select(a => new { Valor = a.CosId, Texto = a.NomeCurto }).ToList()
                // UsuarLista = UsuarNegocio.Query().OrderBy(a=> a.PrivilegioId).Select(a => new { Valor = a.UsuarId, Texto = a.PrivilegioId}).ToList()
            };
        }

        [POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            return new
            {
                PerfilLista = PerfilNegocio.Query().OrderBy(a => a.Nome).Select(a => new { Valor = a.PerfilId, Texto = a.Nome }).ToList(),
               // UsuarLista = UsuarNegocio.Query().OrderBy(a => a.PrivilegioId).Select(a => new { Valor = a.UsuarId, Texto = a.PrivilegioId }).ToList()
            };
        }

        // POST: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]PerfUsu perfUsu)
        {
            PerfUsuNegocio.Criar(perfUsu);
            PerfUsuNegocio.Salvar();
            return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]PerfUsu perfUsu)
        {
            PerfUsu original = ObterItem(key);
            ReflectionUtil.Fill(perfUsu, original);

            PerfUsuNegocio.Atualizar(original);
            PerfUsuNegocio.Salvar();

            return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] perfUsuLista)
        {
            if (perfUsuLista != null && perfUsuLista.Length > 0)
            {
                foreach (var item in perfUsuLista)
                {
                    PerfUsu original = ObterItem((int)item.PerfusuId);
                    PerfUsuNegocio.Excluir(original);
                }
                PerfUsuNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }

        protected override void Dispose(bool disposing)
        {
            PerfUsuNegocio = null;
            PerfilNegocio = null;
            UsuarNegocio = null;
            base.Dispose(disposing);
        }

    }
}
