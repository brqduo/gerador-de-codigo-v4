using ons.common.data.filter;
using ons.intunica.entidade;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace GeradorAngular.Models
{

    /// <summary>
    /// 
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
    [Serializable()] 
	[DataContract(Name = "LogAcessoFiltro", Namespace = "http://schemas.ons.org.br/2013/04/LogAcessoFiltro")]
	public partial class LogAcessoFiltro : DefaultFilter<LogAcessoFiltro, LogAcesso>
	{
	
		public LogAcessoFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
		[DataMember(Name = "FiltroAvancado")]
		public virtual bool FiltroAvancado { get; set; }

		/// <summary>
		/// Valor a ser filtrado de forma simples
		/// </summary>
		[DataMember(Name = "TextoFiltroSimples")]
		public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdLogAcesso")]
		public virtual int?  IdLogAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataAcesso")]
		public virtual DateTime?  DataAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "NomeLogin")]
		public virtual string  NomeLogin { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "FlgAcesso")]
		public virtual string  FlgAcesso { get; set; }
	

		#endregion
		public override void Map()
		{
			//int
			AddFilter(f => f.FiltroAvancado && f.IdLogAcesso.HasValue, u => u.IdLogAcesso == Filter.IdLogAcesso);
			//DateTime?
			AddFilter(f => f.FiltroAvancado && f.DataAcesso.HasValue, u => u.DataAcesso == Filter.DataAcesso);
			//string
			AddFilter(f => f.FiltroAvancado && f.NomeLogin != null & f.NomeLogin != "", u => u.NomeLogin.Contains(Filter.NomeLogin));
			//string
			AddFilter(f => f.FiltroAvancado && f.FlgAcesso != null & f.FlgAcesso != "", u => u.FlgAcesso.Contains(Filter.FlgAcesso));
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.NomeLogin.Contains(Filter.TextoFiltroSimples) | u.FlgAcesso.Contains(Filter.TextoFiltroSimples));
		}
	}
}
