using ons.common.data.filter;
using ons.intunica.entidade;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace GeradorAngular.Models
{

    /// <summary>
    /// 
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
    [Serializable()] 
	[DataContract(Name = "LogsistemasFiltro", Namespace = "http://schemas.ons.org.br/2013/04/LogsistemasFiltro")]
	public partial class LogsistemasFiltro : DefaultFilter<LogsistemasFiltro, Logsistemas>
	{
	
		public LogsistemasFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdLogsistemas")]
		public virtual int?  IdLogsistemas { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataAcesso")]
		public virtual DateTime?  DataAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarId")]
		public virtual string  UsuarId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfilId")]
		public virtual string  PerfilId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "CosId")]
		public virtual string  CosId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "AgeId")]
		public virtual string  AgeId { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdLogsistemas.HasValue, u => u.IdLogsistemas == Filter.IdLogsistemas);
            //DateTime?
            AddFilter(f => f.FiltroAvancado && f.DataAcesso.HasValue, u => u.DataAcesso == Filter.DataAcesso);
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarId != null & f.UsuarId != "", u => u.UsuarId.Contains(Filter.UsuarId));
            //string
            AddFilter(f => f.FiltroAvancado && f.PerfilId != null & f.PerfilId != "", u => u.PerfilId.Contains(Filter.PerfilId));
            //string
            AddFilter(f => f.FiltroAvancado && f.CosId != null & f.CosId != "", u => u.CosId.Contains(Filter.CosId));
            //string
            AddFilter(f => f.FiltroAvancado && f.AgeId != null & f.AgeId != "", u => u.AgeId.Contains(Filter.AgeId));
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.UsuarId.Contains(Filter.TextoFiltroSimples) | u.PerfilId.Contains(Filter.TextoFiltroSimples) | u.CosId.Contains(Filter.TextoFiltroSimples) | u.AgeId.Contains(Filter.TextoFiltroSimples) | u.Perfil.Nome.Contains(Filter.TextoFiltroSimples) | u.Usuar.PrivilegioId.Contains(Filter.TextoFiltroSimples));
		}
	}
}
