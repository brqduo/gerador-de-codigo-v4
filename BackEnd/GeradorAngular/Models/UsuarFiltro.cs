using ons.common.data.filter;
using ons.intunica.entidade;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace GeradorAngular.Models
{

    /// <summary>
    /// 
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:04
    /// </remarks>
    [Serializable()] 
	[DataContract(Name = "UsuarFiltro", Namespace = "http://schemas.ons.org.br/2013/04/UsuarFiltro")]
	public partial class UsuarFiltro : DefaultFilter<UsuarFiltro, Usuar>
	{
	
		public UsuarFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarId")]
		public virtual string  UsuarId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PrivilegioId")]
		public virtual string  PrivilegioId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarNome")]
		public virtual string  UsuarNome { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarSenha")]
		public virtual string  UsuarSenha { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarEmail")]
		public virtual string  UsuarEmail { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarSenhaN")]
		public virtual string  UsuarSenhaN { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdTipoStatususuar")]
		public virtual int?  IdTipoStatususuar { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataStatususuar")]
		public virtual DateTime?  DataStatususuar { get; set; }
	

		#endregion
		public override void Map()
        {
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarId != null & f.UsuarId != "", u => u.UsuarId.Contains(Filter.UsuarId));
            //string
            AddFilter(f => f.FiltroAvancado && f.PrivilegioId != null & f.PrivilegioId != "", u => u.PrivilegioId.Contains(Filter.PrivilegioId));
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarNome != null & f.UsuarNome != "", u => u.UsuarNome.Contains(Filter.UsuarNome));
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarSenha != null & f.UsuarSenha != "", u => u.UsuarSenha.Contains(Filter.UsuarSenha));
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarEmail != null & f.UsuarEmail != "", u => u.UsuarEmail.Contains(Filter.UsuarEmail));
            //string
            AddFilter(f => f.FiltroAvancado && f.UsuarSenhaN != null & f.UsuarSenhaN != "", u => u.UsuarSenhaN.Contains(Filter.UsuarSenhaN));
            //int?
            AddFilter(f => f.FiltroAvancado && f.IdTipoStatususuar.HasValue, u => u.IdTipoStatususuar == Filter.IdTipoStatususuar);
            //DateTime?
            AddFilter(f => f.FiltroAvancado && f.DataStatususuar.HasValue, u => u.DataStatususuar == Filter.DataStatususuar);
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.UsuarId.Contains(Filter.TextoFiltroSimples) | u.PrivilegioId.Contains(Filter.TextoFiltroSimples) | u.UsuarNome.Contains(Filter.TextoFiltroSimples) | u.UsuarSenha.Contains(Filter.TextoFiltroSimples) | u.UsuarEmail.Contains(Filter.TextoFiltroSimples) | u.UsuarSenhaN.Contains(Filter.TextoFiltroSimples) | u.TipoStatususuar.DescricaoTipoStatususuar.Contains(Filter.TextoFiltroSimples));
		}
	}
}
