﻿using Castle.DynamicProxy;
using log4net;
using System;
using System.Diagnostics;

namespace ons.intunica.negocio.ioc
{

	/// <summary>
	/// Interface do interceptador do negocio
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
	public interface INegocioInterceptor : IInterceptor
	{

	}
}
