using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons.intunica.negocio.ioc;
using ons.intunica.negocio.contrato;
using ons.intunica.negocio;
namespace ons.intunica.negocio.ioc
{

	/// <summary>
	/// Mapeameto da inversão de controle do negocio
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public class NegocioContainer
	{
        private static readonly ILog Log = LogManager.GetLogger(typeof(NegocioContainer));
		
        #region IInterceptor Members
		
		/// <summary>
        /// Registra o mapa de inversão de controle do negocio e suas dependencias.
        /// </summary>
        /// <param name="container">Container principal</param>
        public static void RegisterConfigure(IWindsorContainer container)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //
            try
            {
				//Exemplo de customização de mapeamento
				//if (ConfigurationManager.AppSettings["UsaProxy"] != null && ConfigurationManager.AppSettings["UsaProxy"] == "1")
                //{
                //    container.Register(Component.For<INegocionegocio.iocCustom>().Interceptors<Inegocio.iocInterceptor>().ImplementedBy<Negocionegocio.iocCustomProxy>().LifestylePerWebRequest());
                //}
                //else
                //{
                //    container.Register(Component.For<I<Negocionegocio.iocCustom>().Interceptors<Inegocio.iocInterceptor>().ImplementedBy<Negocionegocio.iocEntity>().LifestylePerWebRequest());
                //}
				contexto.ioc.DbContextContainer.RegisterConfigure(container);

				Log.Debug("NegocioContainer|RegisterConfigure|Registrando mapa de classes do negocio");
				//Interceptador
                container.Register(Component.For<INegocioInterceptor>().ImplementedBy<NegocioInterceptor>().LifeStyle.Transient);

				//Classes
				container.Register(Component.For<IMenuNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<MenuNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IModuloNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<ModuloNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IOperacaoNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<OperacaoNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPerfMenuNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PerfMenuNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPerfUsuNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PerfUsuNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPerfilNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PerfilNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPermissaoNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PermissaoNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IServMenuNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<ServMenuNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IServicoNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<ServicoNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<ISistemaNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<SistemaNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<ILogAcessoNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<LogAcessoNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<ILogsistemasNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<LogsistemasNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IServidorsistemaNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<ServidorsistemaNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<ITipoStatususuarNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<TipoStatususuarNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IUsuarNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<UsuarNegocio>().LifestylePerWebRequest());
                container.Register(Component.For<IAgeNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<AgeNegocio>().LifestylePerWebRequest());
                container.Register(Component.For<ICosNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<CosNegocio>().LifestylePerWebRequest());


            }
            finally
            {
                stopwatch.Stop();
                Log.DebugFormat("NegocioContainer|RegisterConfigure|Terminou o register do mapa de classes do negocio.|{0}", stopwatch.ElapsedMilliseconds.ToString());
            }
        }

        #endregion

	}
}
