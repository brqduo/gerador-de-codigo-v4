﻿using Castle.DynamicProxy;
using log4net;
using System;
using System.Diagnostics;

namespace ons.intunica.contexto.ioc
{

	/// <summary>
	/// Interface do interceptador do repositório
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public interface IDbContextInterceptor : IInterceptor
	{

	}
}
