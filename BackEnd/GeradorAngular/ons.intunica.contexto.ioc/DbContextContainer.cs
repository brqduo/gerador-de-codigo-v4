using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons.intunica.contexto.ioc;
using System.Configuration;
using ons.intunica.contexto.proxy;
using ons.intunica.contexto.contrato;
using ons.intunica.contexto;
namespace ons.intunica.contexto.ioc
{

	/// <summary>
	/// Mapeameto da inversão de controle do DbContext
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public class DbContextContainer
	{
        private static readonly ILog Log = LogManager.GetLogger(typeof(DbContextContainer));
		
        #region IInterceptor Members
		
		/// <summary>
        /// Registra o mapa de inversão de controle do DbContext e suas dependencias.
        /// </summary>
        /// <param name="container">Container principal</param>
        public static void RegisterConfigure(IWindsorContainer container)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //
            try
            {
				//Exemplo de customização de mapeamento
				//if (ConfigurationManager.AppSettings["UsaProxy"] != null && ConfigurationManager.AppSettings["UsaProxy"] == "1")
                //{
                //    container.Register(Component.For<IDbContextcontexto.iocCustom>().Interceptors<Icontexto.iocInterceptor>().ImplementedBy<DbContextcontexto.iocCustomProxy>().LifestylePerWebRequest());
                //}
                //else
                //{
                //    container.Register(Component.For<I<DbContextcontexto.iocCustom>().Interceptors<Icontexto.iocInterceptor>().ImplementedBy<DbContextcontexto.iocEntity>().LifestylePerWebRequest());
                //}

				Log.Debug("DbContextContainer|RegisterConfigure|Registrando mapa de classes do DbContext");
				//Interceptador
                container.Register(Component.For<IDbContextInterceptor>().ImplementedBy<DbContextInterceptor>().LifeStyle.Transient);

				//Classes

				if (ConfigurationManager.AppSettings["UsaProxy"] != null && ConfigurationManager.AppSettings["UsaProxy"] == "1")
				{
					container.Register(Component.For<IIntunicaDbContext>().Interceptors<IDbContextInterceptor>().ImplementedBy<IntunicaServiceDbContext>().LifestylePerWebRequest());
				}
				else
				{
					container.Register(Component.For<IIntunicaDbContext>().Interceptors<IDbContextInterceptor>().ImplementedBy<IntunicaDbContext>().LifestylePerWebRequest());
				}
				
				container.Register(Component.For<ons.common.providers.IHelperPOP>().ImplementedBy<ons.common.providers.HelperPOP>().LifestyleSingleton());

                
            }
            finally
            {
                stopwatch.Stop();
                Log.DebugFormat("DbContextContainer|RegisterConfigure|Terminou o register do mapa de classes do DbContext.|{0}", stopwatch.ElapsedMilliseconds.ToString());
            }
        }

        #endregion

	}
}
