﻿using Castle.DynamicProxy;
using log4net;
using ons.common.context;
using ons.common.security;
using System;
using System.Diagnostics;
using System.Text;

namespace ons.intunica.contexto.ioc
{

	/// <summary>
	/// Interceptador do repositório
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public class DbContextInterceptor : IDbContextInterceptor
	{
        private static readonly ILog Log = LogManager.GetLogger(typeof(DbContextInterceptor));
        #region IInterceptor Members

		/// <summary>
        /// Interceptador. 
        /// </summary>
        /// <param name="invocation">Método que está sendo invocado.</param>
        public void Intercept(IInvocation invocation)
        {
           
            Stopwatch stopwatch = new Stopwatch();
            string fullName = invocation.Method.DeclaringType != null ? invocation.Method.DeclaringType.Name + "|" + invocation.Method.Name : invocation.Method.Name;

            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("{0}|Usuário '{1}' Executou o metodo do contexto com os parametros: {2}.", fullName, Security.GetUserName(), ArgsToString(invocation.Arguments));
                stopwatch.Start();
            }

            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                Log.ErrorFormat("{0}|Erro ao executar o método do contexto pelo usuário '{2}'. Error details: {1}.", fullName, e.Message, Security.GetUserName());
                throw;
            }
            finally
            {
                if (Log.IsDebugEnabled)
                {
                    stopwatch.Stop();
                    Log.DebugFormat("{0}|Método do contexto finalizado pelo usuário '{1}'.|{2}", fullName, Security.GetUserName(), stopwatch.ElapsedMilliseconds.ToString());
                }
            }
        }

        /// <summary>
        /// Metodo auxiliar para converter em string os parametros passados para um método.
        /// </summary>
        /// <param name="args">parametros passados para o método.</param>
        /// <returns>String com os parametros.</returns>
        public static string ArgsToString(object[] args)
        {
            var builder = new StringBuilder();

            if (args != null)
            {
				for (var i = 0; i < args.Length; i++)
				{
					if (i == args.Length - 1)
					{
                        builder.Append('\'').Append(ArgToString(args[i])).Append('\'');
					}
					else
					{
                        builder.Append('\'').Append(ArgToString(args[i])).Append('\'').Append(", ");
					}
				}
			}

			var result = builder.ToString();
			return (result == string.Empty) ? "''" : result;
		}
		
		/// <summary>
		/// Metodo auxiliar para converter em string o parametro.
        /// </summary>
        /// <param name="arg">parametro como Object.</param>
        /// <returns>String com o parametro</returns>
        public static string ArgToString(object arg)
        {
            string retorno = "";

            if(arg != null)
            {
                retorno = arg.ToString().Replace(Environment.NewLine, "");
            }

            return retorno;
        }



        #endregion

	}
}
