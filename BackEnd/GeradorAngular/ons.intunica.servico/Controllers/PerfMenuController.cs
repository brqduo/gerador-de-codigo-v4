using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfMenuController : BaseController<PerfMenu>
	{
        private IPerfMenuNegocio PerfMenuNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PerfMenuController(IPerfMenuNegocio perfMenuNegocio) : base(perfMenuNegocio)
		{
			PerfMenuNegocio = perfMenuNegocio;
		}
		
		// GET: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<PerfMenu> Get()
        {
            return Query();
        }

        // GET: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<PerfMenu> Get([FromODataUri] int key)
        {
            return Carregar(item => item.PerfmenuId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.PerfmenuId == key, navigationProperty, type);
        }

		// PUT: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<PerfMenu> delta)
        {
			return base.Put(item => item.PerfmenuId == key, delta);
        }

        // POST: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(PerfMenu perfMenu)
        {
			return base.Post(perfMenu);
        }

        // PATCH: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<PerfMenu> delta)
        {
			return base.Patch(item => item.PerfmenuId == key, delta);
        }

        // DELETE: odata/PerfMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.PerfmenuId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PerfMenuNegocio = null;
            base.Dispose(disposing);
        }
	}
}
