using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class LogAcessoController : BaseController<LogAcesso>
	{
        private ILogAcessoNegocio LogAcessoNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public LogAcessoController(ILogAcessoNegocio logAcessoNegocio) : base(logAcessoNegocio)
		{
			LogAcessoNegocio = logAcessoNegocio;
		}
		
		// GET: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<LogAcesso> Get()
        {
            return Query();
        }

        // GET: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<LogAcesso> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdLogAcesso == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdLogAcesso == key, navigationProperty, type);
        }

		// PUT: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<LogAcesso> delta)
        {
			return base.Put(item => item.IdLogAcesso == key, delta);
        }

        // POST: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(LogAcesso logAcesso)
        {
			return base.Post(logAcesso);
        }

        // PATCH: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<LogAcesso> delta)
        {
			return base.Patch(item => item.IdLogAcesso == key, delta);
        }

        // DELETE: odata/LogAcesso
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdLogAcesso == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            LogAcessoNegocio = null;
            base.Dispose(disposing);
        }
	}
}
