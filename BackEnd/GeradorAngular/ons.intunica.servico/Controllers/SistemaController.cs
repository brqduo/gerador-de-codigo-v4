using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class SistemaController : BaseController<Sistema>
	{
        private ISistemaNegocio SistemaNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public SistemaController(ISistemaNegocio sistemaNegocio) : base(sistemaNegocio)
		{
			SistemaNegocio = sistemaNegocio;
		}
		
		// GET: odata/Sistema
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Sistema> Get()
        {
            return Query();
        }

        // GET: odata/Sistema
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Sistema> Get([FromODataUri] string key)
        {
            return Carregar(item => item.SistemaId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.SistemaId == key, navigationProperty, type);
        }

		// PUT: odata/Sistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Sistema> delta)
        {
			return base.Put(item => item.SistemaId == key, delta);
        }

        // POST: odata/Sistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Sistema sistema)
        {
			return base.Post(sistema);
        }

        // PATCH: odata/Sistema
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Sistema> delta)
        {
			return base.Patch(item => item.SistemaId == key, delta);
        }

        // DELETE: odata/Sistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.SistemaId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            SistemaNegocio = null;
            base.Dispose(disposing);
        }
	}
}
