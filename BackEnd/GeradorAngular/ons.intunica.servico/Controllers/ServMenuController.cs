using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServMenuController : BaseController<ServMenu>
	{
        private IServMenuNegocio ServMenuNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public ServMenuController(IServMenuNegocio servMenuNegocio) : base(servMenuNegocio)
		{
			ServMenuNegocio = servMenuNegocio;
		}
		
		// GET: odata/ServMenu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<ServMenu> Get()
        {
            return Query();
        }

        // GET: odata/ServMenu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<ServMenu> Get([FromODataUri] int key)
        {
            return Carregar(item => item.ServMenuId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.ServMenuId == key, navigationProperty, type);
        }

		// PUT: odata/ServMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<ServMenu> delta)
        {
			return base.Put(item => item.ServMenuId == key, delta);
        }

        // POST: odata/ServMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(ServMenu servMenu)
        {
			return base.Post(servMenu);
        }

        // PATCH: odata/ServMenu
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<ServMenu> delta)
        {
			return base.Patch(item => item.ServMenuId == key, delta);
        }

        // DELETE: odata/ServMenu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.ServMenuId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            ServMenuNegocio = null;
            base.Dispose(disposing);
        }
	}
}
