using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class LogsistemasController : BaseController<Logsistemas>
	{
        private ILogsistemasNegocio LogsistemasNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public LogsistemasController(ILogsistemasNegocio logsistemasNegocio) : base(logsistemasNegocio)
		{
			LogsistemasNegocio = logsistemasNegocio;
		}
		
		// GET: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Logsistemas> Get()
        {
            return Query();
        }

        // GET: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Logsistemas> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdLogsistemas == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdLogsistemas == key, navigationProperty, type);
        }

		// PUT: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Logsistemas> delta)
        {
			return base.Put(item => item.IdLogsistemas == key, delta);
        }

        // POST: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Logsistemas logsistemas)
        {
			return base.Post(logsistemas);
        }

        // PATCH: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Logsistemas> delta)
        {
			return base.Patch(item => item.IdLogsistemas == key, delta);
        }

        // DELETE: odata/Logsistemas
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdLogsistemas == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            LogsistemasNegocio = null;
            base.Dispose(disposing);
        }
	}
}
