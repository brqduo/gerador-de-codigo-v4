using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class ModuloController : BaseController<Modulo>
	{
        private IModuloNegocio ModuloNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public ModuloController(IModuloNegocio moduloNegocio) : base(moduloNegocio)
		{
			ModuloNegocio = moduloNegocio;
		}
		
		// GET: odata/Modulo
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Modulo> Get()
        {
            return Query();
        }

        // GET: odata/Modulo
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Modulo> Get([FromODataUri] string key)
        {
            return Carregar(item => item.ModuloId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.ModuloId == key, navigationProperty, type);
        }

		// PUT: odata/Modulo
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Modulo> delta)
        {
			return base.Put(item => item.ModuloId == key, delta);
        }

        // POST: odata/Modulo
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Modulo modulo)
        {
			return base.Post(modulo);
        }

        // PATCH: odata/Modulo
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Modulo> delta)
        {
			return base.Patch(item => item.ModuloId == key, delta);
        }

        // DELETE: odata/Modulo
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.ModuloId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            ModuloNegocio = null;
            base.Dispose(disposing);
        }
	}
}
