using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServicoController : BaseController<Servico>
	{
        private IServicoNegocio ServicoNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public ServicoController(IServicoNegocio servicoNegocio) : base(servicoNegocio)
		{
			ServicoNegocio = servicoNegocio;
		}
		
		// GET: odata/Servico
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Servico> Get()
        {
            return Query();
        }

        // GET: odata/Servico
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Servico> Get([FromODataUri] string key)
        {
            return Carregar(item => item.ServicoId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.ServicoId == key, navigationProperty, type);
        }

		// PUT: odata/Servico
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Servico> delta)
        {
			return base.Put(item => item.ServicoId == key, delta);
        }

        // POST: odata/Servico
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Servico servico)
        {
			return base.Post(servico);
        }

        // PATCH: odata/Servico
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Servico> delta)
        {
			return base.Patch(item => item.ServicoId == key, delta);
        }

        // DELETE: odata/Servico
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.ServicoId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            ServicoNegocio = null;
            base.Dispose(disposing);
        }
	}
}
