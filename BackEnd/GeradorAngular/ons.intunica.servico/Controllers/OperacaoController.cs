using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class OperacaoController : BaseController<Operacao>
	{
        private IOperacaoNegocio OperacaoNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public OperacaoController(IOperacaoNegocio operacaoNegocio) : base(operacaoNegocio)
		{
			OperacaoNegocio = operacaoNegocio;
		}
		
		// GET: odata/Operacao
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Operacao> Get()
        {
            return Query();
        }

        // GET: odata/Operacao
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Operacao> Get([FromODataUri] string key)
        {
            return Carregar(item => item.OperacaoId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.OperacaoId == key, navigationProperty, type);
        }

		// PUT: odata/Operacao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Operacao> delta)
        {
			return base.Put(item => item.OperacaoId == key, delta);
        }

        // POST: odata/Operacao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Operacao operacao)
        {
			return base.Post(operacao);
        }

        // PATCH: odata/Operacao
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Operacao> delta)
        {
			return base.Patch(item => item.OperacaoId == key, delta);
        }

        // DELETE: odata/Operacao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.OperacaoId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            OperacaoNegocio = null;
            base.Dispose(disposing);
        }
	}
}
