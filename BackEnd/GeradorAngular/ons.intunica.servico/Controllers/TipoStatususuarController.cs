using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class TipoStatususuarController : BaseController<TipoStatususuar>
	{
        private ITipoStatususuarNegocio TipoStatususuarNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public TipoStatususuarController(ITipoStatususuarNegocio tipoStatususuarNegocio) : base(tipoStatususuarNegocio)
		{
			TipoStatususuarNegocio = tipoStatususuarNegocio;
		}
		
		// GET: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<TipoStatususuar> Get()
        {
            return Query();
        }

        // GET: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<TipoStatususuar> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdTipoStatususuar == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdTipoStatususuar == key, navigationProperty, type);
        }

		// PUT: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<TipoStatususuar> delta)
        {
			return base.Put(item => item.IdTipoStatususuar == key, delta);
        }

        // POST: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(TipoStatususuar tipoStatususuar)
        {
			return base.Post(tipoStatususuar);
        }

        // PATCH: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<TipoStatususuar> delta)
        {
			return base.Patch(item => item.IdTipoStatususuar == key, delta);
        }

        // DELETE: odata/TipoStatususuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdTipoStatususuar == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            TipoStatususuarNegocio = null;
            base.Dispose(disposing);
        }
	}
}
