using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PermissaoController : BaseController<Permissao>
	{
        private IPermissaoNegocio PermissaoNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PermissaoController(IPermissaoNegocio permissaoNegocio) : base(permissaoNegocio)
		{
			PermissaoNegocio = permissaoNegocio;
		}
		
		// GET: odata/Permissao
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Permissao> Get()
        {
            return Query();
        }

        // GET: odata/Permissao
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Permissao> Get([FromODataUri] int key)
        {
            return Carregar(item => item.PermissaoId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.PermissaoId == key, navigationProperty, type);
        }

		// PUT: odata/Permissao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Permissao> delta)
        {
			return base.Put(item => item.PermissaoId == key, delta);
        }

        // POST: odata/Permissao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Permissao permissao)
        {
			return base.Post(permissao);
        }

        // PATCH: odata/Permissao
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Permissao> delta)
        {
			return base.Patch(item => item.PermissaoId == key, delta);
        }

        // DELETE: odata/Permissao
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.PermissaoId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PermissaoNegocio = null;
            base.Dispose(disposing);
        }
	}
}
