using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfilController : BaseController<Perfil>
	{
        private IPerfilNegocio PerfilNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PerfilController(IPerfilNegocio perfilNegocio) : base(perfilNegocio)
		{
			PerfilNegocio = perfilNegocio;
		}
		
		// GET: odata/Perfil
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Perfil> Get()
        {
            return Query();
        }

        // GET: odata/Perfil
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Perfil> Get([FromODataUri] string key)
        {
            return Carregar(item => item.PerfilId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.PerfilId == key, navigationProperty, type);
        }

		// PUT: odata/Perfil
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Perfil> delta)
        {
			return base.Put(item => item.PerfilId == key, delta);
        }

        // POST: odata/Perfil
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Perfil perfil)
        {
			return base.Post(perfil);
        }

        // PATCH: odata/Perfil
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Perfil> delta)
        {
			return base.Patch(item => item.PerfilId == key, delta);
        }

        // DELETE: odata/Perfil
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.PerfilId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PerfilNegocio = null;
            base.Dispose(disposing);
        }
	}
}
