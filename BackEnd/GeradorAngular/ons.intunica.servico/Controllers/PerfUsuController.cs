using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfUsuController : BaseController<PerfUsu>
	{
        private IPerfUsuNegocio PerfUsuNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PerfUsuController(IPerfUsuNegocio perfUsuNegocio) : base(perfUsuNegocio)
		{
			PerfUsuNegocio = perfUsuNegocio;
		}
		
		// GET: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<PerfUsu> Get()
        {
            return Query();
        }

        // GET: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<PerfUsu> Get([FromODataUri] int key)
        {
            return Carregar(item => item.PerfusuId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.PerfusuId == key, navigationProperty, type);
        }

		// PUT: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<PerfUsu> delta)
        {
			return base.Put(item => item.PerfusuId == key, delta);
        }

        // POST: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(PerfUsu perfUsu)
        {
			return base.Post(perfUsu);
        }

        // PATCH: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<PerfUsu> delta)
        {
			return base.Patch(item => item.PerfusuId == key, delta);
        }

        // DELETE: odata/PerfUsu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.PerfusuId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PerfUsuNegocio = null;
            base.Dispose(disposing);
        }
	}
}
