using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class MenuController : BaseController<Menu>
	{
        private IMenuNegocio MenuNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public MenuController(IMenuNegocio menuNegocio) : base(menuNegocio)
		{
			MenuNegocio = menuNegocio;
		}
		
		// GET: odata/Menu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Menu> Get()
        {
            return Query();
        }

        // GET: odata/Menu
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Menu> Get([FromODataUri] int key)
        {
            return Carregar(item => item.MenuId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.MenuId == key, navigationProperty, type);
        }

		// PUT: odata/Menu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Menu> delta)
        {
			return base.Put(item => item.MenuId == key, delta);
        }

        // POST: odata/Menu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Menu menu)
        {
			return base.Post(menu);
        }

        // PATCH: odata/Menu
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Menu> delta)
        {
			return base.Patch(item => item.MenuId == key, delta);
        }

        // DELETE: odata/Menu
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.MenuId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            MenuNegocio = null;
            base.Dispose(disposing);
        }
	}
}
