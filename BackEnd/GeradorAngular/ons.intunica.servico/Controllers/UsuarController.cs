using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:04
    /// </remarks>
	public partial class UsuarController : BaseController<Usuar>
	{
        private IUsuarNegocio UsuarNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public UsuarController(IUsuarNegocio usuarNegocio) : base(usuarNegocio)
		{
			UsuarNegocio = usuarNegocio;
		}
		
		// GET: odata/Usuar
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Usuar> Get()
        {
            return Query();
        }

        // GET: odata/Usuar
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Usuar> Get([FromODataUri] string key)
        {
            return Carregar(item => item.UsuarId == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] string key, string navigationProperty, string type)
        {
            return GetProperty(item => item.UsuarId == key, navigationProperty, type);
        }

		// PUT: odata/Usuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] string key, Delta<Usuar> delta)
        {
			return base.Put(item => item.UsuarId == key, delta);
        }

        // POST: odata/Usuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Usuar usuar)
        {
			return base.Post(usuar);
        }

        // PATCH: odata/Usuar
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Usuar> delta)
        {
			return base.Patch(item => item.UsuarId == key, delta);
        }

        // DELETE: odata/Usuar
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] string key)
        {
			return base.Delete(item => item.UsuarId == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            UsuarNegocio = null;
            base.Dispose(disposing);
        }
	}
}
