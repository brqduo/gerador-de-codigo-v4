using ons.intunica.entidade;
using ons.intunica.negocio.contrato;
using ons.intunica.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.intunica.servico
{

	/// <summary>
	/// Controller Wep Api para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServidorsistemaController : BaseController<Servidorsistema>
	{
        private IServidorsistemaNegocio ServidorsistemaNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public ServidorsistemaController(IServidorsistemaNegocio servidorsistemaNegocio) : base(servidorsistemaNegocio)
		{
			ServidorsistemaNegocio = servidorsistemaNegocio;
		}
		
		// GET: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Servidorsistema> Get()
        {
            return Query();
        }

        // GET: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Servidorsistema> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdServidorsistema == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdServidorsistema == key, navigationProperty, type);
        }

		// PUT: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Servidorsistema> delta)
        {
			return base.Put(item => item.IdServidorsistema == key, delta);
        }

        // POST: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Servidorsistema servidorsistema)
        {
			return base.Post(servidorsistema);
        }

        // PATCH: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Servidorsistema> delta)
        {
			return base.Patch(item => item.IdServidorsistema == key, delta);
        }

        // DELETE: odata/Servidorsistema
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdServidorsistema == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            ServidorsistemaNegocio = null;
            base.Dispose(disposing);
        }
	}
}
