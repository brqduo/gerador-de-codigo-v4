using ons.intunica.entidade;
using ons.intunica.servico.ioc.webapi;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData.Batch;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Routing.Conventions;

namespace ons.intunica.servico
{
	/// <summary>
	/// WebApiConfig de acesso a base intunica
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:47:58
    /// </remarks>
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var builder = new ODataConventionModelBuilder();
			
			//Menu
			var entityTypeMenu = builder.EntitySet<Menu>("Menu").EntityType;
			entityTypeMenu.HasKey(t => t.MenuId);
			//Exemplo de método customizado.
			//var actionConfigMenu = entityTypeMenu.Collection.Action("ObterMenu");
            //actionConfigMenu.ReturnsCollectionFromEntitySet<Menu>("Menu");
            //actionConfigMenu.Parameter<int>("[Filtro]");
			
			//Modulo
			var entityTypeModulo = builder.EntitySet<Modulo>("Modulo").EntityType;
			entityTypeModulo.HasKey(t => t.ModuloId);
			//Exemplo de método customizado.
			//var actionConfigModulo = entityTypeModulo.Collection.Action("ObterModulo");
            //actionConfigModulo.ReturnsCollectionFromEntitySet<Modulo>("Modulo");
            //actionConfigModulo.Parameter<int>("[Filtro]");
			
			//Operacao
			var entityTypeOperacao = builder.EntitySet<Operacao>("Operacao").EntityType;
			entityTypeOperacao.HasKey(t => t.OperacaoId);
			//Exemplo de método customizado.
			//var actionConfigOperacao = entityTypeOperacao.Collection.Action("ObterOperacao");
            //actionConfigOperacao.ReturnsCollectionFromEntitySet<Operacao>("Operacao");
            //actionConfigOperacao.Parameter<int>("[Filtro]");
			
			//PerfMenu
			var entityTypePerfMenu = builder.EntitySet<PerfMenu>("PerfMenu").EntityType;
			entityTypePerfMenu.HasKey(t => t.PerfmenuId);
			//Exemplo de método customizado.
			//var actionConfigPerfMenu = entityTypePerfMenu.Collection.Action("ObterPerfMenu");
            //actionConfigPerfMenu.ReturnsCollectionFromEntitySet<PerfMenu>("PerfMenu");
            //actionConfigPerfMenu.Parameter<int>("[Filtro]");
			
			//PerfUsu
			var entityTypePerfUsu = builder.EntitySet<PerfUsu>("PerfUsu").EntityType;
			entityTypePerfUsu.HasKey(t => t.PerfusuId);
			//Exemplo de método customizado.
			//var actionConfigPerfUsu = entityTypePerfUsu.Collection.Action("ObterPerfUsu");
            //actionConfigPerfUsu.ReturnsCollectionFromEntitySet<PerfUsu>("PerfUsu");
            //actionConfigPerfUsu.Parameter<int>("[Filtro]");
			
			//Perfil
			var entityTypePerfil = builder.EntitySet<Perfil>("Perfil").EntityType;
			entityTypePerfil.HasKey(t => t.PerfilId);
			//Exemplo de método customizado.
			//var actionConfigPerfil = entityTypePerfil.Collection.Action("ObterPerfil");
            //actionConfigPerfil.ReturnsCollectionFromEntitySet<Perfil>("Perfil");
            //actionConfigPerfil.Parameter<int>("[Filtro]");
			
			//Permissao
			var entityTypePermissao = builder.EntitySet<Permissao>("Permissao").EntityType;
			entityTypePermissao.HasKey(t => t.PermissaoId);
			//Exemplo de método customizado.
			//var actionConfigPermissao = entityTypePermissao.Collection.Action("ObterPermissao");
            //actionConfigPermissao.ReturnsCollectionFromEntitySet<Permissao>("Permissao");
            //actionConfigPermissao.Parameter<int>("[Filtro]");
			
			//ServMenu
			var entityTypeServMenu = builder.EntitySet<ServMenu>("ServMenu").EntityType;
			entityTypeServMenu.HasKey(t => t.ServMenuId);
			//Exemplo de método customizado.
			//var actionConfigServMenu = entityTypeServMenu.Collection.Action("ObterServMenu");
            //actionConfigServMenu.ReturnsCollectionFromEntitySet<ServMenu>("ServMenu");
            //actionConfigServMenu.Parameter<int>("[Filtro]");
			
			//Servico
			var entityTypeServico = builder.EntitySet<Servico>("Servico").EntityType;
			entityTypeServico.HasKey(t => t.ServicoId);
			//Exemplo de método customizado.
			//var actionConfigServico = entityTypeServico.Collection.Action("ObterServico");
            //actionConfigServico.ReturnsCollectionFromEntitySet<Servico>("Servico");
            //actionConfigServico.Parameter<int>("[Filtro]");
			
			//Sistema
			var entityTypeSistema = builder.EntitySet<Sistema>("Sistema").EntityType;
			entityTypeSistema.HasKey(t => t.SistemaId);
			//Exemplo de método customizado.
			//var actionConfigSistema = entityTypeSistema.Collection.Action("ObterSistema");
            //actionConfigSistema.ReturnsCollectionFromEntitySet<Sistema>("Sistema");
            //actionConfigSistema.Parameter<int>("[Filtro]");
			
			//LogAcesso
			var entityTypeLogAcesso = builder.EntitySet<LogAcesso>("LogAcesso").EntityType;
			entityTypeLogAcesso.HasKey(t => t.IdLogAcesso);
			//Exemplo de método customizado.
			//var actionConfigLogAcesso = entityTypeLogAcesso.Collection.Action("ObterLogAcesso");
            //actionConfigLogAcesso.ReturnsCollectionFromEntitySet<LogAcesso>("LogAcesso");
            //actionConfigLogAcesso.Parameter<int>("[Filtro]");
			
			//Logsistemas
			var entityTypeLogsistemas = builder.EntitySet<Logsistemas>("Logsistemas").EntityType;
			entityTypeLogsistemas.HasKey(t => t.IdLogsistemas);
			//Exemplo de método customizado.
			//var actionConfigLogsistemas = entityTypeLogsistemas.Collection.Action("ObterLogsistemas");
            //actionConfigLogsistemas.ReturnsCollectionFromEntitySet<Logsistemas>("Logsistemas");
            //actionConfigLogsistemas.Parameter<int>("[Filtro]");
			
			//Servidorsistema
			var entityTypeServidorsistema = builder.EntitySet<Servidorsistema>("Servidorsistema").EntityType;
			entityTypeServidorsistema.HasKey(t => t.IdServidorsistema);
			//Exemplo de método customizado.
			//var actionConfigServidorsistema = entityTypeServidorsistema.Collection.Action("ObterServidorsistema");
            //actionConfigServidorsistema.ReturnsCollectionFromEntitySet<Servidorsistema>("Servidorsistema");
            //actionConfigServidorsistema.Parameter<int>("[Filtro]");
			
			//TipoStatususuar
			var entityTypeTipoStatususuar = builder.EntitySet<TipoStatususuar>("TipoStatususuar").EntityType;
			entityTypeTipoStatususuar.HasKey(t => t.IdTipoStatususuar);
			//Exemplo de método customizado.
			//var actionConfigTipoStatususuar = entityTypeTipoStatususuar.Collection.Action("ObterTipoStatususuar");
            //actionConfigTipoStatususuar.ReturnsCollectionFromEntitySet<TipoStatususuar>("TipoStatususuar");
            //actionConfigTipoStatususuar.Parameter<int>("[Filtro]");
			
			//Usuar
			var entityTypeUsuar = builder.EntitySet<Usuar>("Usuar").EntityType;
			entityTypeUsuar.HasKey(t => t.UsuarId);
			//Exemplo de método customizado.
			//var actionConfigUsuar = entityTypeUsuar.Collection.Action("ObterUsuar");
            //actionConfigUsuar.ReturnsCollectionFromEntitySet<Usuar>("Usuar");
            //actionConfigUsuar.Parameter<int>("[Filtro]");
			
			


            var batchHandler = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);
            batchHandler.MessageQuotas.MaxOperationsPerChangeset = 1500;

            IList<IODataRoutingConvention> routingConventions = ODataRoutingConventions.CreateDefault();
            routingConventions.Insert(0, new CountODataRoutingConvention());
			routingConventions.Insert(0, new CompositeKeyRoutingConvention());

             config.Routes.MapODataServiceRoute("odata", "odata/v1", builder.GetEdmModel(), new CountODataPathHandler(), routingConventions, batchHandler);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "api",
                routeTemplate: "api/v1/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}

