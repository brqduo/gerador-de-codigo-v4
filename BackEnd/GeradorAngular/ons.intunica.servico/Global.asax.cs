﻿using ons.intunica.comum.ioc;
using ons.intunica.servico.ioc;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace ons.intunica.servico
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            if (log4net.LogManager.GetCurrentLoggers().Length == 0)
            {
                log4net.Config.XmlConfigurator.Configure();
            }

            IoC.Initialize(new ODataContainer());
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(IoC.Kernel));
        }
    }
}
