using ons.intunica.contexto.contrato;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ons.intunica.contexto.proxy
{

	/// <summary>
	/// DbContext de acesso a base intunica
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class IntunicaServiceDbContext : ServiceDbContext, IIntunicaDbContext
	{

		public IntunicaServiceDbContext(ons.common.providers.IHelperPOP popHelper): base(popHelper)
		{
		}

        //protected override DataServiceContext Container
        //{
        //    get
        //    {
        //        if (container == null)
        //        {
        //            var uri = new Uri(PopHelper.ObterParametro("INTUNICAServiceUri"));
        //            
        //            container = new DataServiceContext(uri, System.Data.Services.Common.DataServiceProtocolVersion.V3);
        //            container.Credentials = new NetworkCredential(PopHelper.ObterParametro("INTUNICAServiceUserName"), PopHelper.ObterParametro("INTUNICAServicePassword", true));
        //            container.SendingRequest2 += Container_SendingRequestTicket;
        //            container.IgnoreMissingProperties = true;
        //        }
        //        return container;
        //    }
        //}
		
        public override string urlServico
        {
            get
			{
				return PopHelper.ObterParametro("INTUNICAServiceUri");
			}
        }

        public override string LoginServico
        {
            get
			{
				return PopHelper.ObterParametro("INTUNICAServiceUserName");
			}
        }

        public override string SenhaServico
        {
            get
			{
				return PopHelper.ObterParametro("INTUNICAServicePassword", true);
			}
        }
		
		public override void Dispose()
        {
            container = null;
			base.Dispose();
		}
	}
}
