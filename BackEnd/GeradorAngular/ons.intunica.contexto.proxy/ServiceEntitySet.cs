﻿using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.contexto.proxy
{
    public class ServiceEntitySet<TEntity> : IEntitySet<TEntity>
        where TEntity : class, new()
    {
        private DataServiceQuery<TEntity> ServiceQuery;
        private DataServiceContext Container;
        private string GetEntitySetName()
        {
            return typeof(TEntity).Name;// +"s";
        }
        public ServiceEntitySet(DataServiceContext container)
        {
            Container = container;
            ServiceQuery = Container.CreateQuery<TEntity>(GetEntitySetName());
        }

        public TEntity Add(TEntity entity)
        {
            Container.AddObject(GetEntitySetName(), entity);

            return entity;
        }

        public TEntity Attach(TEntity entity)
        {
            Container.AttachTo(GetEntitySetName(), entity);

            return entity;
        }

        /// <summary>
        /// Atualiza no contexto a entidade (entity). e marca como alterada.
        /// </summary>
        /// <param name="entity">Entity to attach</param>
        public void Update(TEntity entity)
        {
            var entry = Container.GetEntityDescriptor(entity);
            if (entry.State == EntityStates.Detached)
                Container.AttachTo(GetEntitySetName(), entity);
            Container.UpdateObject(entity);
        }

        public TProp GetOriginalValues<TProp>(TEntity entity, System.Linq.Expressions.Expression<Func<TEntity, TProp>> propertyName)
        {
            TProp retorno = default(TProp);

            var propOriginal = Container.LoadProperty(entity, propertyName.Body.ToString().Replace(propertyName.Parameters[0].Name + ".", ""));

            retorno = propOriginal.Cast<TProp>().FirstOrDefault();

            return retorno;
        }

        public TEntity Create()
        {
            return new TEntity();
        }

        public void Remove(TEntity entity)
        {
            Container.DeleteObject(entity);
        }

        public IQueryable<TEntity> Query()
        {
            return ServiceQuery;
        }

        public void Dispose()
        {
            Container = null;
            ServiceQuery = null;
        }
    }
}
