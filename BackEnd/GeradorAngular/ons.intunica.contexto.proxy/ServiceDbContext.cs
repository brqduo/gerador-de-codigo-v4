﻿using log4net;
using ons.common.context;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ons.intunica.contexto.proxy
{
    public abstract class ServiceDbContext : IDbContext
    {
        protected static readonly ILog Logger = LogManager.GetLogger(typeof(ServiceDbContext));
        protected ons.common.providers.IHelperPOP PopHelper;
        protected Type Classe;
        protected DataServiceContext container = null;

        public ServiceDbContext(ons.common.providers.IHelperPOP popHelper)
        {
            PopHelper = popHelper;
        }
        protected virtual DataServiceContext Container
        {
            get
            {
                if (container == null)
                {
                    var uri = new Uri(urlServico + "odata/v1/");
                    //container = new Container(uri);
                    container = new DataServiceContext(uri, System.Data.Services.Common.DataServiceProtocolVersion.V3);
                    container.Credentials = new NetworkCredential(LoginServico, SenhaServico);
                    container.SendingRequest2 += Container_SendingRequestTicket;
                    container.IgnoreMissingProperties = true;
                }

                return container;
            }
        }
        protected virtual HttpClient ObterClient()
        {
            HttpClientHandler handler = new HttpClientHandler { Credentials = new NetworkCredential(LoginServico, SenhaServico) };
            var client = new HttpClient(handler);
            client.BaseAddress = new Uri(urlServico + "api/v1/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public abstract string urlServico
        {
            get;
        }

        public abstract string LoginServico
        {
            get;
        }

        public abstract string SenhaServico
        {
            get;
        }

        public virtual bool SaveChanges<TPrincipal>()
        {
            Classe = typeof(TPrincipal);
            Container.SendingRequest2 += Container_SendingRequestTPrincipal;

            var result = Container.SaveChanges(SaveChangesOptions.Batch);

            return result.Count(o => o.StatusCode != (int)HttpStatusCode.NoContent) == 0;
        }


        protected void Container_SendingRequestTPrincipal(object sender, SendingRequest2EventArgs e)
        {
            e.RequestMessage.SetHeader("POP-Header-TPrincipal", Classe.FullName);
        }
        protected void Container_SendingRequestTicket(object sender, SendingRequest2EventArgs e)
        {
            Logger.DebugFormat("ServiceDbContext|{0}|{1}", e.RequestMessage.Method, e.RequestMessage.Url); 
            e.RequestMessage.SetHeader("POP-Header-App", PopHelper.ApplicationName);
            e.RequestMessage.SetHeader("POP-Header-Ticket", PopHelper.TicketUsuario);
            e.RequestMessage.SetHeader("Cookie", string.Format("{0}={1}", FormsAuthentication.FormsCookieName, PopHelper.TicketUsuario));
        }

        public virtual IEntitySet<TEntity> GetEntitySet<TEntity>() where TEntity : class, new()
        {
            return new ServiceEntitySet<TEntity>(Container);
        }
        public virtual IEnumerable<TEntity> Execute<TEntity>(string command, params KeyValuePair<string, object>[] parameters)
        {
            IEnumerable<TEntity> retorno;

            var parametros = parameters.Select(p => new BodyOperationParameter(p.Key, p.Value)).ToArray();

            var actionUri = new Uri(Container.BaseUri, command);

            retorno = Container.Execute<TEntity>(actionUri, "POST", false, parametros);

            return retorno;
        }

        public TEntityResponse Execute<TRequest, TEntityResponse>(string command, TRequest parameters)
        {
            TEntityResponse outValue = default(TEntityResponse);

            using (var client = ObterClient())
            {
                HttpResponseMessage response = client.PostAsJsonAsync(command, parameters).Result;

                if (response.IsSuccessStatusCode)
                {
                    outValue = response.Content.ReadAsAsync<TEntityResponse>().Result;
                }
                else
                {
                    response.EnsureSuccessStatusCode();
                }

                client.Dispose();
            }

            return outValue;
        }

        public TEntityResponse Execute<TEntityResponse>(string command)
        {
            TEntityResponse outValue = default(TEntityResponse);

            using (var client = ObterClient())
            {
                HttpResponseMessage response = client.GetAsync(command).Result;

                if (response.IsSuccessStatusCode)
                {
                    outValue = response.Content.ReadAsAsync<TEntityResponse>().Result;
                }
                else
                {
                    response.EnsureSuccessStatusCode();
                }

                client.Dispose();
            }

            return outValue;
        }

        public virtual void Dispose()
        {
            container = null;
        }

    }
}
