﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using log4net;
using System.Diagnostics;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace ons.intunica.servico.ioc.webapi
{
    public class LogActionFilter : ActionFilterAttribute
    {
        private readonly static ILog log = LogManager.GetLogger(typeof(LogActionFilter));

        private const string MsgInicio = "{2}Controller|{3}|INÍCIO REQUISIÇÃO[Path:{0}, QueryString:{1}]";
        private const string MsgFim = "{2}Controller|{3}|FIM REQUISIÇÃO[Path:{0}, QueryString:{1}]|{4}";
        private const string ParamTempoIni = "request.tempo.ini";

        private Stopwatch _stopwatch;

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var routeData = actionContext.ControllerContext.RouteData;
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];

            var request = actionContext.Request;

            _stopwatch = new Stopwatch();
            _stopwatch.Start();

            log.DebugFormat(MsgInicio, request.RequestUri.AbsolutePath, request.RequestUri.Query, controllerName, actionName);
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var routeData = actionExecutedContext.ActionContext.ControllerContext.RouteData;
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];

            var request = actionExecutedContext.ActionContext.Request;

            _stopwatch.Stop();

            log.DebugFormat(MsgFim, request.RequestUri.AbsolutePath, request.RequestUri.Query, controllerName, actionName, _stopwatch.ElapsedMilliseconds);
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}
