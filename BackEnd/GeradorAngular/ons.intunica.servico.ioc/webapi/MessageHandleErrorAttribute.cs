﻿using log4net;
using ons.common.utilities.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Data.Services.Client;
using ons.common.utilities.Xml;

namespace ons.intunica.servico.ioc.webapi
{
    public class MessageHandleErrorAttribute : ExceptionFilterAttribute
    {
        private readonly static ILog log = LogManager.GetLogger(typeof(MessageHandleErrorAttribute));

        public string CurrentView { get; set; }

        public override void OnException(HttpActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                bool exibirMsgGenerica = true;

                var ex = filterContext.Exception;

                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                if (ex is BusinessException)
                {
                    filterContext.Response = filterContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.ExpectationFailed, ex.Message);
                    exibirMsgGenerica = false;
                }
                else if (ex is DataServiceClientException)
                {
                    var innerEx = (DataServiceClientException)ex;
                    if (innerEx.StatusCode == (int)System.Net.HttpStatusCode.ExpectationFailed)
                    {
                        try
                        {
                            Error msg = innerEx.Message.XmlDeserialize<Error>();
                            filterContext.Response = filterContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.ExpectationFailed, msg.message.Value);
                            exibirMsgGenerica = false;
                        }
                        catch
                        {
                            exibirMsgGenerica = true;
                        }
                    }
                }

                if (exibirMsgGenerica)
                {
                    filterContext.Response = filterContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.ExpectationFailed, "Aconteceu um erro inesperado. Por favor tente mais tarde.");
                    LogHelper.Registrar(ex, this, ex.Message, DefaultErrorEventCodes.ServiceError);
                    log.Error("MessageHandleErrorAttribute|OnException|" + ex.Message, ex);
                }
            }
        }
    }
}
