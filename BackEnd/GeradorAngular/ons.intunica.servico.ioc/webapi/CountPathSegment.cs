﻿using Microsoft.Data.Edm;
using Microsoft.Data.Edm.Library;

namespace ons.intunica.servico.ioc.webapi
{
    public class CountPathSegment : System.Web.Http.OData.Routing.ODataPathSegment
    {
        public override string SegmentKind
        {
            get
            {
                return "$count";
            }
        }

        public override IEdmType GetEdmType(IEdmType previousEdmType)
        {
            return EdmCoreModel.Instance.FindDeclaredType("Edm.Int32");
        }

        public override IEdmEntitySet GetEntitySet(IEdmEntitySet previousEntitySet)
        {
            return previousEntitySet;
        }

        public override string ToString()
        {
            return "$count";
        }
    }
}
