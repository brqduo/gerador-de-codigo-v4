﻿using ons.intunica.negocio.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using ons.intunica.comum;

namespace ons.intunica.servico.ioc.webapi
{
    [LogActionFilter]
    [MessageHandleError]
    public class BaseController<TEntity> : ODataController
        where TEntity : class, new()
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        protected readonly INegocioBase<TEntity> Negocio;

        public BaseController(INegocioBase<TEntity> negocio)
        {
            Negocio = negocio;
        }

        private void ObterParametrosContexto()
        {
            if (Request != null)
            {
                GetHeader("POP-Header-TPrincipal");
                GetHeader("POP-Header-Ticket");
                GetHeader("POP-Header-App");
            }
        }

        private void GetHeader(string headerName)
        {
            var tPrincipalHeader = Request.Headers.AsQueryable().Where(a => a.Key == headerName).FirstOrDefault();

            if (tPrincipalHeader.Value != null)
            {
                if (!ons.common.context.RequestContext.Current.Itens.ContainsKey(headerName))
                    ons.common.context.RequestContext.Current.Itens.Add(headerName, tPrincipalHeader.Value.FirstOrDefault());
                else
                    ons.common.context.RequestContext.Current.Itens[headerName] = tPrincipalHeader.Value.FirstOrDefault();
            }
        }

        protected IQueryable<TEntity> Query()
        {
            ObterParametrosContexto();
            return Negocio.Query();
        }

        protected SingleResult<TEntity> Carregar(Expression<Func<TEntity, bool>> query)
        {
            ObterParametrosContexto();
            return SingleResult.Create<TEntity>(Negocio.Query().Where(query));
        }

        //[POPCRUDAPIAuthorize]
        //public IHttpActionResult GetCount(ODataQueryOptions<TEntity> queryOptions)
        //{
        //    IQueryable<TEntity> queryResults = queryOptions.ApplyTo(Query()) as IQueryable<TEntity>;
        //    return Ok<int>(queryResults.Count());
        //}

        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetCount(ODataQueryOptions<TEntity> queryOptions)
        {
            ObterParametrosContexto();
            IQueryable<TEntity> queryResults = queryOptions.ApplyTo(Query()) as IQueryable<TEntity>;
            //int count = queryResults.Count();
            //return Ok<int>(queryResults.Count());
            int count = queryResults.Count();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(count.ToString(), Encoding.UTF8, "text/plain");
            return response;
        }

        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty(Expression<Func<TEntity, bool>> query, string navigationProperty, string type)
        {
            ObterParametrosContexto();
            Type tipo = Type.GetType(type);

            object valorPropriedade = this.GetType().GetMethods().Where(m => m.Name.Equals("GetProperty") && m.IsGenericMethod).SingleOrDefault().MakeGenericMethod(tipo).Invoke(this, new object[] { query, navigationProperty });

            string valorPropriedadeString = null;

            if (valorPropriedade != null)
                valorPropriedadeString = valorPropriedade.ToString();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(valorPropriedadeString, Encoding.UTF8, "text/plain");
            return response;
        }

        public string GetProperty<TProp>(Expression<Func<TEntity, bool>> query, string navigationProperty)
        {
            ObterParametrosContexto();

            IQueryable<TProp> queryResults = Negocio.Query().Where(query).SelectProperty<TEntity, TProp>(navigationProperty);

            return queryResults.FirstOrDefault().ToString();
        }

        protected IHttpActionResult Put(Expression<Func<TEntity, bool>> query, Delta<TEntity> delta)
        {
            ObterParametrosContexto();
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TEntity entity = default(TEntity);
            if (query == null
                || (entity = Negocio.Query().Where(query).SingleOrDefault()) == null)
            {
                return NotFound();
            }

            delta.Put(entity);
            Negocio.Atualizar(entity);
            Negocio.Salvar();

            return Updated(entity);
        }

        protected IHttpActionResult Post(TEntity entity)
        {
            ObterParametrosContexto();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var retorno = Negocio.Criar(entity);
            Negocio.Salvar();

            return Created(retorno);
        }

        protected IHttpActionResult Patch(Expression<Func<TEntity, bool>> query, Delta<TEntity> delta)
        {
            ObterParametrosContexto();
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TEntity entity = default(TEntity);
            if (query == null
                || (entity = Negocio.Query().Where(query).SingleOrDefault()) == null)
            {
                return NotFound();
            }

            delta.Patch(entity);
            Negocio.Atualizar(entity);

            Negocio.Salvar();

            return Updated(entity);
        }

        protected IHttpActionResult Delete(Expression<Func<TEntity, bool>> query)
        {
            ObterParametrosContexto();
            TEntity entity = default(TEntity);
            if (query == null
                || (entity = Negocio.Query().Where(query).SingleOrDefault()) == null)
            {
                return NotFound();
            }

            Negocio.Excluir(entity);
            Negocio.Salvar();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (Negocio != null)
            {
                Negocio.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
