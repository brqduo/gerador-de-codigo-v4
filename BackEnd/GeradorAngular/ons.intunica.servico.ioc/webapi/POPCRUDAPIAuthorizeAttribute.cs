﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace ons.intunica.servico.ioc.webapi
{
    public class POPCRUDAPIAuthorizeAttribute : POPAuthorizeAttribute
    {
        private const string OPER_CREATE = "Criar ";
        private const string OPER_READ = "Consultar ";
        private const string OPER_UPDATE = "Editar ";
        private const string OPER_DELETE = "Excluir ";

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            PreencherOperacao(actionContext.Request.Method.Method, actionContext.ActionDescriptor.ControllerDescriptor.ControllerName);
            return base.IsAuthorized(actionContext);
        }

        private void PreencherOperacao(string metodo, string controller)
        {
            string operacao = null;
            switch (metodo)
            {
                case "POST":
                    operacao = OPER_CREATE;
                    break;
                case "GET":
                    operacao = OPER_READ;
                    break;
                case "PUT":
                case "PATCH":
                case "MERGE":
                    operacao = OPER_UPDATE;
                    break;
                case "DELETE":
                    operacao = OPER_DELETE;
                    break;
            }

            if (!string.IsNullOrEmpty(operacao))
                this.Permissions = new string[] { string.Format("{0}{1}", operacao, controller) };
            else
                throw new ArgumentNullException("Operação não preenchida!");
        }
    }
}
