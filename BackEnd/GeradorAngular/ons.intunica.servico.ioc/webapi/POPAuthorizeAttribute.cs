﻿using log4net;
using ons.common.providers;
using ons.intunica.comum.ioc;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ons.intunica.servico.ioc.webapi
{
    public class POPAuthorizeAttribute : AuthorizeAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string[] Permissions { get; protected set; }

        public POPAuthorizeAttribute(params string[] permissions)
        {
            this.Permissions = permissions;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
#if false
            bool retorno = true;
#else
            bool retorno = false;

            Stopwatch stopwatch = new Stopwatch();

            string argumentos = "";

            if (Log.IsDebugEnabled)
            {
                argumentos = Permissions.ArgsToString();

                Log.DebugFormat("POPAuthorizeAttribute|IsAuthorized|Verificando no POP se o usuário '{0}' pode executar {1}.", POPHelper.LoginUsuario, argumentos);
                stopwatch.Start();
            }

            try
            {
                if (base.IsAuthorized(actionContext))
                {
                    retorno = POPHelper.VerificarAcessoQualquerEscopo(Permissions);
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("POPAuthorizeAttribute|IsAuthorized|Erro ao acessar o POP. usuário: '{0}' pode executar {1}.", e.Message, POPHelper.LoginUsuario, Permissions.ArgsToString());
                throw;
            }
            finally
            {
                if (Log.IsDebugEnabled)
                {
                    stopwatch.Stop();
                    Log.DebugFormat("POPAuthorizeAttribute|IsAuthorized|Verificado no POP se o usuário '{0}' pode executar {1}. Resultado: {2} |{3}", POPHelper.LoginUsuario, argumentos, retorno, stopwatch.ElapsedMilliseconds);
                }
            }
#endif
            return retorno;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent(string.Format("O usuário não tem permissão para executar a ação {0}: {1}.",
                                                                actionContext.Request.Method.Method,
                                                                actionContext.ActionDescriptor.ControllerDescriptor.ControllerName))
                };
            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
        }
    }
}
