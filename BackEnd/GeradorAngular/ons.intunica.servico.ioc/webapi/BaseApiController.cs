﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ons.intunica.servico.ioc.webapi
{
    [LogActionFilter]
    [MessageHandleError]
    public class BaseApiController : ApiController
    {
        public dynamic GetAppConfig()
        {
            return new { ExibeLog = (System.Configuration.ConfigurationManager.AppSettings["ExibeLog"] == "1"), LoginUrl = System.Web.Security.FormsAuthentication.LoginUrl };
        }

    }
}
