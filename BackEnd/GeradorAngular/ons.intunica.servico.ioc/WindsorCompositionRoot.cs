﻿using Castle.MicroKernel;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace ons.intunica.servico.ioc
{
    public class WindsorCompositionRoot : IHttpControllerActivator
    {
        private readonly IKernel kernel;

        public WindsorCompositionRoot(IKernel kernel)
        {
            this.kernel = kernel;
        }

        public IHttpController Create(
            HttpRequestMessage request,
            HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            var controller =
                (IHttpController)this.kernel.Resolve(controllerType);

            request.RegisterForDispose(
                new Release(
                    () => this.kernel.ReleaseComponent(controller)));

            return controller;
        }

        private class Release : IDisposable
        {
            private readonly Action release;

            public Release(Action release)
            {
                this.release = release;
            }

            public void Dispose()
            {
                this.release();
            }
        }
    }
}