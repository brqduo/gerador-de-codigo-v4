﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using System.Diagnostics;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;

namespace ons.intunica.servico.ioc
{
    public class ODataContainer : WindsorContainer
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ODataContainer()
        {
            RegisterConfigure(this);
        }

        public static void RegisterConfigure(WindsorContainer container)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                Logger.Debug("APIContainer|RegisterConfigure|Iniciou o register do mapa de classes que a API depende.");

                ons.intunica.negocio.ioc.NegocioContainer.RegisterConfigure(container);

                Logger.Debug("APIContainer|RegisterConfigure|Registrando IHttpController");
                container.Register(Classes.FromAssembly(Assembly.Load("ons.intunica.servico")).BasedOn<IHttpController>().LifestyleTransient());
                container.Register(Classes.FromAssembly(Assembly.Load("System.Web.Http.OData")).BasedOn<ODataMetadataController>().LifestyleTransient());
            }
            finally
            {
                stopwatch.Stop();
                Logger.DebugFormat("APIContainer|RegisterConfigure|Terminou o register do mapa de classes que a API depende.|{0}", stopwatch.ElapsedMilliseconds.ToString());
            }
        }
    }
}