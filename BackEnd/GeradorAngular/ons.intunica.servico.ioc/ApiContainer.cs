﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using ons.intunica.comum.ioc;
using ons.intunica.comum.Reflection;
using ons.intunica.negocio;
using ons.intunica.negocio.ioc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;

namespace GeradorAngular.ioc
{
    public class WebContainer : WindsorContainer
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public WebContainer()
        {
            RegisterConfigure(this);
        }

        public static void RegisterConfigure(WindsorContainer container)
        {
            //log4net.Config.XmlConfigurator.Configure();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                Logger.Debug("WebContainer|RegisterConfigure|Iniciou o register do mapa de classes que a UI depende.");

                NegocioContainer.RegisterConfigure(container);
                Logger.Debug("WebContainer|RegisterConfigure|Registrando IControllers");
                container.Register(Classes.FromAssembly(Assembly.Load("GeradorAngular")).BasedOn<IHttpController>().LifestyleTransient());
            }
            finally
            {
                stopwatch.Stop();
                Logger.DebugFormat("WebContainer|RegisterConfigure|Terminou o register do mapa de classes que a UI depende.|{0}", stopwatch.ElapsedMilliseconds.ToString());
            }
        }
    }
}
