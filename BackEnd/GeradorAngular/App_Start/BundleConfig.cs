﻿using System.Web;
using System.Web.Optimization;

namespace GeradorAngular
{
    public class BundleConfig
    {
        // Para obter mais informações sobre o agrupamento, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use a versão em desenvolvimento do Modernizr para desenvolver e aprender. Em seguida, quando estiver
            // pronto para a produção, utilize a ferramenta de build em https://modernizr.com para escolher somente os testes que precisa.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new Bundle("~/bundles/angular").Include(
                       "~/Scripts/libs/runtime.*",
                        "~/Scripts/libs/polyfills.*",
                        "~/Scripts/libs/styles.*",
                       "~/Scripts/libs/vendor.*",
                       "~/Scripts/libs/main.*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     // "~/Content/bootstrap.css",
                      "~/src/assets/ag-grid.css",
                      "~/node_modules/@fortawesome/fontawesome-free/scss/fontawesome.css",
                      "~/node_modules/@fortawesome/fontawesome-free/scss/solid.css",
                      "~/node_modules/@fortawesome/fontawesome-free/scss/regular.css",
                      "~/node_modules/@fortawesome/fontawesome-free/scss/brands.css",
                      "~/node_modules/angular-bootstrap-md/scss/bootstrap/bootstrap.css",
                      "~/node_modules/angular-bootstrap-md/scss/mdb-free.css",
                      "~/node_modules/primeng/resources/themes/nova-light/theme.css",
                      "~/node_modules/primeng/resources/primeng.min.css",
                      "~/node_modules/primeicons/primeicons.css",
                      "~/node_modules/@fortawesome/fontawesome-free/css/all.css"
                      ));//"~/Content/Site.css"
        }
    }
}
