﻿using ons.common.context;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.contexto
{
    public class EntitySet<TEntity> : IEntitySet<TEntity>
        where TEntity : class, new()
    {
        private DbSet<TEntity> DbSet;
        DbContextAuditado Contexto;
        public EntitySet(DbContextAuditado contexto)
        {
            Contexto = contexto;
            DbSet = Contexto.Set<TEntity>();
            ObterParametrosContexto();
        }

        public TEntity Add(TEntity entity)
        {
            return DbSet.Add(entity);
        }

        public TEntity Attach(TEntity entity)
        {
            return DbSet.Attach(entity);
        }

        public TEntity Create()
        {
            return DbSet.Create();
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public void Update(TEntity entity)
        {
            var entry = Contexto.Entry(entity);
            if (entry.State == EntityState.Detached)
                DbSet.Attach(entity);
            entry.State = EntityState.Modified;
        }

        public TProp GetOriginalValues<TProp>(TEntity entity, System.Linq.Expressions.Expression<Func<TEntity, TProp>> propertyName)
        {
            var entry = Contexto.Entry(entity);
            var retorno = entry.OriginalValues.GetValue<TProp>(propertyName.Body.ToString().Replace(propertyName.Parameters[0].Name + ".", ""));

            return retorno;
        }

        public IQueryable<TEntity> Query()
        {
            return DbSet;
        }

        private void ObterParametrosContexto()
        {
            Contexto.NomeClasse = GetContextos("POP-Header-TPrincipal");
            Contexto.TicketExecutor = GetContextos("POP-Header-Ticket");
            GetContextos("POP-Header-App");
        }

        private string GetContextos(string headerName)
        {
            string retorno = null;
            if (RequestContext.Current.Itens.ContainsKey(headerName))//tenta achar o informado na requisição
            {
                retorno = RequestContext.Current.Itens[headerName].ToString();
            }
            return retorno;
        }
        public void Dispose()
        {
            Contexto = null;
            DbSet = null;
        }
    }
}
