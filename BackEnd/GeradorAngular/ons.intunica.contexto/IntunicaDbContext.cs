using ons.common.auditoria;
using ons.common.auditoria.Models;
using ons.common.utilities.Xml;
using ons.intunica.contexto.mapeamento;
using ons.intunica.contexto.contrato;
using ons.intunica.entidade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;

namespace ons.intunica.contexto
{

	/// <summary>
	/// DbContext de acesso a base intunica
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:47:59
    /// </remarks>
	public partial class IntunicaDbContext : DbContextAuditado, IIntunicaDbContext
	{
		
		public IntunicaDbContext(ons.common.providers.IHelperPOP popHelper): base(popHelper, "Name=IntunicaDbContext")
		{
            Database.SetInitializer<IntunicaDbContext>(null);
            //this.Configuration.AutoDetectChangesEnabled = false;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
		}


		#region "OnModelCreating"
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new MenuMap());
			modelBuilder.Configurations.Add(new ModuloMap());
			modelBuilder.Configurations.Add(new OperacaoMap());
			modelBuilder.Configurations.Add(new PerfMenuMap());
			modelBuilder.Configurations.Add(new PerfUsuMap());
			modelBuilder.Configurations.Add(new PerfilMap());
			modelBuilder.Configurations.Add(new PermissaoMap());
			modelBuilder.Configurations.Add(new ServMenuMap());
			modelBuilder.Configurations.Add(new ServicoMap());
			modelBuilder.Configurations.Add(new SistemaMap());
			modelBuilder.Configurations.Add(new LogAcessoMap());
			modelBuilder.Configurations.Add(new LogsistemasMap());
			modelBuilder.Configurations.Add(new ServidorsistemaMap());
			modelBuilder.Configurations.Add(new TipoStatususuarMap());
			modelBuilder.Configurations.Add(new UsuarMap());

		}
		#endregion


		public override void MontarMapa()
        {
            if (!mapaGerado)
            {
                lock (_lock)
                {
                    if (!mapaGerado)
                    {
                        base.MontarMapa();
                        var mapa = ons.common.auditoria.Models.Mapa.Instance;

						MenuMap(mapa);
						ModuloMap(mapa);
						OperacaoMap(mapa);
						PerfMenuMap(mapa);
						PerfUsuMap(mapa);
						PerfilMap(mapa);
						PermissaoMap(mapa);
						ServMenuMap(mapa);
						ServicoMap(mapa);
						SistemaMap(mapa);
						LogAcessoMap(mapa);
						LogsistemasMap(mapa);
						ServidorsistemaMap(mapa);
						TipoStatususuarMap(mapa);
						UsuarMap(mapa);


                        mapaGerado = true;
                    }
                }
            }
        }
		private void MenuMap(Mapa mapa)
        {
            var classeMenu = mapa.RecuperarClasse<Menu>();
            classeMenu.AdicionarCaminhoExibicao(c => c.Tipo);
            classeMenu.NomeExterno = @"Menu";

            var propMenuId = classeMenu.RecuperarPropriedade(c => c.MenuId);
            propMenuId.NomeExterno = @"Chave";

            var propMenuIdPai = classeMenu.RecuperarPropriedade(c => c.MenuIdPai);
            propMenuIdPai.NomeExterno = @"Chave Pai";

            var propOrdem = classeMenu.RecuperarPropriedade(c => c.Ordem);
            propOrdem.NomeExterno = @"Ordem";

            var propTipo = classeMenu.RecuperarPropriedade(c => c.Tipo);
            propTipo.NomeExterno = @"Tipo";

            var propTexto = classeMenu.RecuperarPropriedade(c => c.Texto);
            propTexto.NomeExterno = @"Texto";



        }	

		private void ModuloMap(Mapa mapa)
        {
            var classeModulo = mapa.RecuperarClasse<Modulo>();
            classeModulo.AdicionarCaminhoExibicao(c => c.Descricao);
            classeModulo.NomeExterno = @"Módulo";

            var propModuloId = classeModulo.RecuperarPropriedade(c => c.ModuloId);
            propModuloId.NomeExterno = @"Chave";

            var propSistemaId = classeModulo.RecuperarPropriedade(c => c.SistemaId);
            propSistemaId.NomeExterno = @"Aplicação Chave";

            var propDescricao = classeModulo.RecuperarPropriedade(c => c.Descricao);
            propDescricao.NomeExterno = @"Descrição";



        }	

		private void OperacaoMap(Mapa mapa)
        {
            var classeOperacao = mapa.RecuperarClasse<Operacao>();
            classeOperacao.AdicionarCaminhoExibicao(c => c.Descricao);
            classeOperacao.NomeExterno = @"Operação";

            var propOperacaoId = classeOperacao.RecuperarPropriedade(c => c.OperacaoId);
            propOperacaoId.NomeExterno = @"Chave";

            var propDescricao = classeOperacao.RecuperarPropriedade(c => c.Descricao);
            propDescricao.NomeExterno = @"Descrição";



        }	

		private void PerfMenuMap(Mapa mapa)
        {
            var classePerfMenu = mapa.RecuperarClasse<PerfMenu>();
            classePerfMenu.AdicionarCaminhoExibicao(c => c.PerfilId);
            classePerfMenu.NomeExterno = @"Perf Menu";

            var propPerfmenuId = classePerfMenu.RecuperarPropriedade(c => c.PerfmenuId);
            propPerfmenuId.NomeExterno = @"Chave";

            var propPerfilId = classePerfMenu.RecuperarPropriedade(c => c.PerfilId);
            propPerfilId.NomeExterno = @"Perfil Chave";

            var propMenuId = classePerfMenu.RecuperarPropriedade(c => c.MenuId);
            propMenuId.NomeExterno = @"Menu Chave";



        }	

		private void PerfUsuMap(Mapa mapa)
        {
            var classePerfUsu = mapa.RecuperarClasse<PerfUsu>();
            classePerfUsu.AdicionarCaminhoExibicao(c => c.AgeId);
            classePerfUsu.NomeExterno = @"Perf Usu";

            var propPerfusuId = classePerfUsu.RecuperarPropriedade(c => c.PerfusuId);
            propPerfusuId.NomeExterno = @"Chave";

            var propUsuarId = classePerfUsu.RecuperarPropriedade(c => c.UsuarId);
            propUsuarId.NomeExterno = @"Usuar Chave";

            var propPerfilId = classePerfUsu.RecuperarPropriedade(c => c.PerfilId);
            propPerfilId.NomeExterno = @"Perfil Chave";

            var propAgeId = classePerfUsu.RecuperarPropriedade(c => c.AgeId);
            propAgeId.NomeExterno = @"Agente Chave";

            var propCosId = classePerfUsu.RecuperarPropriedade(c => c.CosId);
            propCosId.NomeExterno = @"Cos Chave";



        }	

		private void PerfilMap(Mapa mapa)
        {
            var classePerfil = mapa.RecuperarClasse<Perfil>();
            classePerfil.AdicionarCaminhoExibicao(c => c.Nome);
            classePerfil.NomeExterno = @"Perfil";

            var propPerfilId = classePerfil.RecuperarPropriedade(c => c.PerfilId);
            propPerfilId.NomeExterno = @"Chave";

            var propNome = classePerfil.RecuperarPropriedade(c => c.Nome);
            propNome.NomeExterno = @"Nome";

            var propSistemaId = classePerfil.RecuperarPropriedade(c => c.SistemaId);
            propSistemaId.NomeExterno = @"Aplicação Chave";



        }	

		private void PermissaoMap(Mapa mapa)
        {
            var classePermissao = mapa.RecuperarClasse<Permissao>();
            classePermissao.AdicionarCaminhoExibicao(c => c.OperacaoId);
            classePermissao.NomeExterno = @"Permissão";

            var propPermissaoId = classePermissao.RecuperarPropriedade(c => c.PermissaoId);
            propPermissaoId.NomeExterno = @"Chave";

            var propOperacaoId = classePermissao.RecuperarPropriedade(c => c.OperacaoId);
            propOperacaoId.NomeExterno = @"Operação Chave";

            var propUsuarId = classePermissao.RecuperarPropriedade(c => c.UsuarId);
            propUsuarId.NomeExterno = @"Usuar Chave";

            var propServicoId = classePermissao.RecuperarPropriedade(c => c.ServicoId);
            propServicoId.NomeExterno = @"Serviço Chave";

            var propPerfilId = classePermissao.RecuperarPropriedade(c => c.PerfilId);
            propPerfilId.NomeExterno = @"Perfil Chave";



        }	

		private void ServMenuMap(Mapa mapa)
        {
            var classeServMenu = mapa.RecuperarClasse<ServMenu>();
            classeServMenu.AdicionarCaminhoExibicao(c => c.MenuId);
            classeServMenu.NomeExterno = @"Serv Menu";

            var propServMenuId = classeServMenu.RecuperarPropriedade(c => c.ServMenuId);
            propServMenuId.NomeExterno = @"Serv Menu Chave";

            var propMenuId = classeServMenu.RecuperarPropriedade(c => c.MenuId);
            propMenuId.NomeExterno = @"Menu Chave";

            var propServicoId = classeServMenu.RecuperarPropriedade(c => c.ServicoId);
            propServicoId.NomeExterno = @"Serviço Chave";



        }	

		private void ServicoMap(Mapa mapa)
        {
            var classeServico = mapa.RecuperarClasse<Servico>();
            classeServico.AdicionarCaminhoExibicao(c => c.Descricao);
            classeServico.NomeExterno = @"Serviço";

            var propServicoId = classeServico.RecuperarPropriedade(c => c.ServicoId);
            propServicoId.NomeExterno = @"Chave";

            var propModuloId = classeServico.RecuperarPropriedade(c => c.ModuloId);
            propModuloId.NomeExterno = @"Módulo Chave";

            var propDescricao = classeServico.RecuperarPropriedade(c => c.Descricao);
            propDescricao.NomeExterno = @"Descrição";

            var propPath = classeServico.RecuperarPropriedade(c => c.Path);
            propPath.NomeExterno = @"Path";

            var propExecutavel = classeServico.RecuperarPropriedade(c => c.Executavel);
            propExecutavel.NomeExterno = @"Executavel";

            var propQuerystring = classeServico.RecuperarPropriedade(c => c.Querystring);
            propQuerystring.NomeExterno = @"Querystring";

            var propDescricaoModocompatibilidade = classeServico.RecuperarPropriedade(c => c.DescricaoModocompatibilidade);
            propDescricaoModocompatibilidade.NomeExterno = @"Descrição Modo compatibilChave ade";



        }	

		private void SistemaMap(Mapa mapa)
        {
            var classeSistema = mapa.RecuperarClasse<Sistema>();
            classeSistema.AdicionarCaminhoExibicao(c => c.Descricao);
            classeSistema.NomeExterno = @"Aplicação";

            var propSistemaId = classeSistema.RecuperarPropriedade(c => c.SistemaId);
            propSistemaId.NomeExterno = @"Chave";

            var propDescricao = classeSistema.RecuperarPropriedade(c => c.Descricao);
            propDescricao.NomeExterno = @"Descrição";

            var propEmailResp = classeSistema.RecuperarPropriedade(c => c.EmailResp);
            propEmailResp.NomeExterno = @"E-mail Resp";

            var propServicoId = classeSistema.RecuperarPropriedade(c => c.ServicoId);
            propServicoId.NomeExterno = @"Serviço Chave";

            var propFlgLoginrequerido = classeSistema.RecuperarPropriedade(c => c.FlgLoginrequerido);
            propFlgLoginrequerido.NomeExterno = @"Login RequerIDO";

            var propFlgDesativado = classeSistema.RecuperarPropriedade(c => c.FlgDesativado);
            propFlgDesativado.NomeExterno = @"Desativado";



        }	

		private void LogAcessoMap(Mapa mapa)
        {
            var classeLogAcesso = mapa.RecuperarClasse<LogAcesso>();
            classeLogAcesso.AdicionarCaminhoExibicao(c => c.NomeLogin);
            classeLogAcesso.NomeExterno = @"Log Acesso";

            var propIdLogAcesso = classeLogAcesso.RecuperarPropriedade(c => c.IdLogAcesso);
            propIdLogAcesso.NomeExterno = @"Chave";

            var propDataAcesso = classeLogAcesso.RecuperarPropriedade(c => c.DataAcesso);
            propDataAcesso.NomeExterno = @"Data Acesso";

            var propNomeLogin = classeLogAcesso.RecuperarPropriedade(c => c.NomeLogin);
            propNomeLogin.NomeExterno = @"Nome Login";

            var propFlgAcesso = classeLogAcesso.RecuperarPropriedade(c => c.FlgAcesso);
            propFlgAcesso.NomeExterno = @"Acesso";



        }	

		private void LogsistemasMap(Mapa mapa)
        {
            var classeLogsistemas = mapa.RecuperarClasse<Logsistemas>();
            classeLogsistemas.AdicionarCaminhoExibicao(c => c.CosId);
            classeLogsistemas.NomeExterno = @"Log Aplicação s";

            var propIdLogsistemas = classeLogsistemas.RecuperarPropriedade(c => c.IdLogsistemas);
            propIdLogsistemas.NomeExterno = @"Chave";

            var propDataAcesso = classeLogsistemas.RecuperarPropriedade(c => c.DataAcesso);
            propDataAcesso.NomeExterno = @"Data Acesso";

            var propUsuarId = classeLogsistemas.RecuperarPropriedade(c => c.UsuarId);
            propUsuarId.NomeExterno = @"Usuar Chave";

            var propPerfilId = classeLogsistemas.RecuperarPropriedade(c => c.PerfilId);
            propPerfilId.NomeExterno = @"Perfil Chave";

            var propCosId = classeLogsistemas.RecuperarPropriedade(c => c.CosId);
            propCosId.NomeExterno = @"Cos Chave";

            var propAgeId = classeLogsistemas.RecuperarPropriedade(c => c.AgeId);
            propAgeId.NomeExterno = @"Agente Chave";



        }	

		private void ServidorsistemaMap(Mapa mapa)
        {
            var classeServidorsistema = mapa.RecuperarClasse<Servidorsistema>();
            classeServidorsistema.AdicionarCaminhoExibicao(c => c.UrlServidor);
            classeServidorsistema.NomeExterno = @"servIDO rAplicação";

            var propIdServidorsistema = classeServidorsistema.RecuperarPropriedade(c => c.IdServidorsistema);
            propIdServidorsistema.NomeExterno = @"Chave";

            var propSistemaId = classeServidorsistema.RecuperarPropriedade(c => c.SistemaId);
            propSistemaId.NomeExterno = @"Aplicação Chave";

            var propUrlServidor = classeServidorsistema.RecuperarPropriedade(c => c.UrlServidor);
            propUrlServidor.NomeExterno = @"Url servIDO r";



        }	

		private void TipoStatususuarMap(Mapa mapa)
        {
            var classeTipoStatususuar = mapa.RecuperarClasse<TipoStatususuar>();
            classeTipoStatususuar.AdicionarCaminhoExibicao(c => c.DescricaoTipoStatususuar);
            classeTipoStatususuar.NomeExterno = @"Tipo Status usuar";

            var propIdTipoStatususuar = classeTipoStatususuar.RecuperarPropriedade(c => c.IdTipoStatususuar);
            propIdTipoStatususuar.NomeExterno = @"Chave Tipo Status usuar";

            var propDescricaoTipoStatususuar = classeTipoStatususuar.RecuperarPropriedade(c => c.DescricaoTipoStatususuar);
            propDescricaoTipoStatususuar.NomeExterno = @"Descrição Tipo Status usuar";



        }	

		private void UsuarMap(Mapa mapa)
        {
            var classeUsuar = mapa.RecuperarClasse<Usuar>();
            classeUsuar.AdicionarCaminhoExibicao(c => c.PrivilegioId);
            classeUsuar.NomeExterno = @"Usuar";

            var propUsuarId = classeUsuar.RecuperarPropriedade(c => c.UsuarId);
            propUsuarId.NomeExterno = @"Chave";

            var propPrivilegioId = classeUsuar.RecuperarPropriedade(c => c.PrivilegioId);
            propPrivilegioId.NomeExterno = @"Privilegio Chave";

            var propUsuarNome = classeUsuar.RecuperarPropriedade(c => c.UsuarNome);
            propUsuarNome.NomeExterno = @"Nome";

            var propUsuarSenha = classeUsuar.RecuperarPropriedade(c => c.UsuarSenha);
            propUsuarSenha.NomeExterno = @"Senha";

            var propUsuarEmail = classeUsuar.RecuperarPropriedade(c => c.UsuarEmail);
            propUsuarEmail.NomeExterno = @"E-mail";

            var propUsuarSenhaN = classeUsuar.RecuperarPropriedade(c => c.UsuarSenhaN);
            propUsuarSenhaN.NomeExterno = @"Senha N";

            var propIdTipoStatususuar = classeUsuar.RecuperarPropriedade(c => c.IdTipoStatususuar);
            propIdTipoStatususuar.NomeExterno = @"Chave Tipo Status";

            var propDataStatususuar = classeUsuar.RecuperarPropriedade(c => c.DataStatususuar);
            propDataStatususuar.NomeExterno = @"Data Status";



        }	


        protected override void Dispose(bool disposing)
        {
			base.Dispose(disposing);
        }
		
		public void Dispose()
        {
			Dispose(false);
		}
	}
}
