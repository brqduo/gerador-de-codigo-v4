using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Servico
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServicoMap : EntityTypeConfiguration<Servico>
	{

		public ServicoMap()
		{
		
			this.ToTable("servico", "informix");

			this.HasKey(t => t.ServicoId);

			this.Property(t => t.ServicoId).HasColumnName("servico_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.Property(t => t.ModuloId).HasColumnName("modulo_id").IsOptional().HasMaxLength(10);//.HasColumnType("char");//char(10)//10//string
			this.Property(t => t.Descricao).HasColumnName("descricao").IsRequired().HasMaxLength(40);//.HasColumnType("char");//char(40) not null//40//string
			this.Property(t => t.Path).HasColumnName("path").IsRequired().HasMaxLength(255);//.HasColumnType("char");//char(255) not null//255//string
			this.Property(t => t.Executavel).HasColumnName("executavel").IsRequired().HasMaxLength(50);//.HasColumnType("varchar");//varchar(50) not null//50//string
			this.Property(t => t.Querystring).HasColumnName("querystring").IsOptional().HasMaxLength(255);//.HasColumnType("varchar");//varchar(255)//255//string
			this.Property(t => t.DescricaoModocompatibilidade).HasColumnName("dsc_modocompatibilidade").IsOptional().HasMaxLength(70);//.HasColumnType("varchar");//varchar(70)//70//string
			this.HasOptional(t => t.Modulo).WithMany(t => t.ServicoLista).HasForeignKey(d => d.ModuloId);
	
		}

	}
}
