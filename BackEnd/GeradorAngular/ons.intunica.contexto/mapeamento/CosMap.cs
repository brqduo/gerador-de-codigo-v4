using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Cos
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:11
    /// </remarks>
	public partial class CosMap : EntityTypeConfiguration<Cos>
	{

		public CosMap()
		{
		
			this.ToTable("snm_cos", "informix");

			this.HasKey(t => t.CosId);

			this.Property(t => t.CosId).HasColumnName("cos_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(2);//.HasColumnType("char");//char(2) not null//2//string
			this.Property(t => t.NomeCurto).HasColumnName("nomecurto").IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.Property(t => t.Nomelongo).HasColumnName("nomelongo").IsRequired().HasMaxLength(60);//.HasColumnType("char");//char(60) not null//60//string
			this.Property(t => t.Dtentrada).HasColumnName("dtentrada").IsOptional();//.HasColumnType("date");//date//4//DateTime?
			this.Property(t => t.Dtdesativa).HasColumnName("dtdesativa").IsOptional();//.HasColumnType("date");//date//4//DateTime?
			this.Property(t => t.Nomedes).HasColumnName("nomedes").IsOptional().HasMaxLength(50);//.HasColumnType("varchar");//varchar(50)//50//string
			this.Property(t => t.TipoorgaoId).HasColumnName("tporgao_id").IsRequired();//.HasColumnType("integer");//integer not null//4//int?
			this.Property(t => t.CosIdResp).HasColumnName("cos_id_resp").IsRequired().HasMaxLength(2);//.HasColumnType("char");//char(2) not null//2//string
			this.Property(t => t.ValorLatitudeinicial).HasColumnName("val_latitudeinicial").IsOptional();//.HasColumnType("float");//float//8//float?
			this.Property(t => t.ValorLongitudeinicial).HasColumnName("val_longitudeinicial").IsOptional();//.HasColumnType("float");//float//8//float?
			this.Property(t => t.ValorLatitudefinal).HasColumnName("val_latitudefinal").IsOptional();//.HasColumnType("float");//float//8//float?
			this.Property(t => t.ValorLongitudefinal).HasColumnName("val_longitudefinal").IsOptional();//.HasColumnType("float");//float//8//float?
			this.Property(t => t.IdoOns).HasColumnName("ido_ons").IsRequired().HasMaxLength(32);//.HasColumnType("varchar");//varchar(32) not null//32//string
			this.Property(t => t.Mrid).HasColumnName("mrid").IsOptional().HasMaxLength(43);//.HasColumnType("varchar");//varchar(43)//43//string
	
		}

	}
}
