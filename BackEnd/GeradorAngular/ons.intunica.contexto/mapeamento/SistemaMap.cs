using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Sistema
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class SistemaMap : EntityTypeConfiguration<Sistema>
	{

		public SistemaMap()
		{
		
			this.ToTable("sistema", "informix");

			this.HasKey(t => t.SistemaId);

			this.Property(t => t.SistemaId).HasColumnName("sistema_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.Descricao).HasColumnName("descricao").IsRequired().HasMaxLength(40);//.HasColumnType("char");//char(40) not null//40//string
			this.Property(t => t.EmailResp).HasColumnName("email_resp").IsOptional().HasMaxLength(40);//.HasColumnType("char");//char(40)//40//string
			this.Property(t => t.ServicoId).HasColumnName("servico_id").IsOptional().HasMaxLength(15);//.HasColumnType("char");//char(15)//15//string
			this.Property(t => t.FlgLoginrequerido).HasColumnName("flg_loginrequerido").IsOptional().HasMaxLength(1);//.HasColumnType("char");//char(1)//1//string
			this.Property(t => t.FlgDesativado).HasColumnName("flg_desativado").IsOptional().HasMaxLength(1);//.HasColumnType("char");//char(1)//1//string
			this.HasOptional(t => t.Servico).WithMany(t => t.SistemaLista).HasForeignKey(d => d.ServicoId);
	
		}

	}
}
