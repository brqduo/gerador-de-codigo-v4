using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Usuar
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:04
    /// </remarks>
	public partial class UsuarMap : EntityTypeConfiguration<Usuar>
	{

		public UsuarMap()
		{
		
			this.ToTable("usuar", "informix");

			this.HasKey(t => t.UsuarId);

			this.Property(t => t.UsuarId).HasColumnName("usuar_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(8);//.HasColumnType("char");//char(8) not null//8//string
			this.Property(t => t.PrivilegioId).HasColumnName("privilegio_id").IsRequired().HasMaxLength(1);//.HasColumnType("char");//char(1) not null//1//string
			this.Property(t => t.UsuarNome).HasColumnName("usuar_nome").IsRequired().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100) not null//100//string
			this.Property(t => t.UsuarSenha).HasColumnName("usuar_senha").IsOptional().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100)//100//string
			this.Property(t => t.UsuarEmail).HasColumnName("usuar_email").IsOptional().HasMaxLength(100);//.HasColumnType("char");//char(100)//100//string
			this.Property(t => t.UsuarSenhaN).HasColumnName("usuar_senha_n").IsOptional().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100)//100//string
			this.Property(t => t.IdTipoStatususuar).HasColumnName("id_tpstatususuar").IsOptional();//.HasColumnType("integer");//integer//4//int?
			this.Property(t => t.DataStatususuar).HasColumnName("din_statususuar").IsOptional();//.HasColumnType("datetime");//datetime//3594//DateTime?
			this.HasOptional(t => t.TipoStatususuar).WithMany(t => t.UsuarLista).HasForeignKey(d => d.IdTipoStatususuar);
	
		}

	}
}
