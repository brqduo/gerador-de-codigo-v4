using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Logsistemas
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class LogsistemasMap : EntityTypeConfiguration<Logsistemas>
	{

		public LogsistemasMap()
		{
		
			this.ToTable("tb_logsistemas", "informix");

			this.HasKey(t => t.IdLogsistemas);

			this.Property(t => t.IdLogsistemas).HasColumnName("id_logsistemas").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.DataAcesso).HasColumnName("din_acesso").IsRequired();//.HasColumnType("datetime");//datetime not null//3594//DateTime?
			this.Property(t => t.UsuarId).HasColumnName("usuar_id").IsRequired().HasMaxLength(8);//.HasColumnType("char");//char(8) not null//8//string
			this.Property(t => t.PerfilId).HasColumnName("perfil_id").IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.CosId).HasColumnName("cos_id").IsOptional().HasMaxLength(2);//.HasColumnType("char");//char(2)//2//string
			this.Property(t => t.AgeId).HasColumnName("age_id").IsOptional().HasMaxLength(3);//.HasColumnType("char");//char(3)//3//string
			this.HasRequired(t => t.Perfil).WithMany(t => t.LogsistemasLista).HasForeignKey(d => d.PerfilId);
			this.HasRequired(t => t.Usuar).WithMany(t => t.LogsistemasLista).HasForeignKey(d => d.UsuarId);
	
		}

	}
}
