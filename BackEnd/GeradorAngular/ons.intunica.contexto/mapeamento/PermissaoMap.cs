using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Permissao
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PermissaoMap : EntityTypeConfiguration<Permissao>
	{

		public PermissaoMap()
		{
		
			this.ToTable("permissao", "informix");

			this.HasKey(t => t.PermissaoId);

			this.Property(t => t.PermissaoId).HasColumnName("permissao_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.OperacaoId).HasColumnName("operacao_id").IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.Property(t => t.UsuarId).HasColumnName("usuar_id").IsOptional().HasMaxLength(8);//.HasColumnType("char");//char(8)//8//string
			this.Property(t => t.ServicoId).HasColumnName("servico_id").IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.Property(t => t.PerfilId).HasColumnName("perfil_id").IsOptional().HasMaxLength(10);//.HasColumnType("char");//char(10)//10//string
			this.HasRequired(t => t.Operacao).WithMany(t => t.PermissaoLista).HasForeignKey(d => d.OperacaoId);
			this.HasRequired(t => t.Servico).WithMany(t => t.PermissaoLista).HasForeignKey(d => d.ServicoId);
			this.HasOptional(t => t.Perfil).WithMany(t => t.PermissaoLista).HasForeignKey(d => d.PerfilId);
			this.HasOptional(t => t.Usuar).WithMany(t => t.PermissaoLista).HasForeignKey(d => d.UsuarId);
	
		}

	}
}
