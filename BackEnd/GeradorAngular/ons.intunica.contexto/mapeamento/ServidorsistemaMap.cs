using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Servidorsistema
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServidorsistemaMap : EntityTypeConfiguration<Servidorsistema>
	{

		public ServidorsistemaMap()
		{
		
			this.ToTable("tb_servidorsistema", "informix");

			this.HasKey(t => t.IdServidorsistema);

			this.Property(t => t.IdServidorsistema).HasColumnName("id_servidorsistema").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.SistemaId).HasColumnName("sistema_id").IsOptional().HasMaxLength(10);//.HasColumnType("char");//char(10)//10//string
			this.Property(t => t.UrlServidor).HasColumnName("url_servidor").IsRequired().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100) not null//100//string
			this.HasOptional(t => t.Sistema).WithMany(t => t.ServidorsistemaLista).HasForeignKey(d => d.SistemaId);
	
		}

	}
}
