using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Menu
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class MenuMap : EntityTypeConfiguration<Menu>
	{

		public MenuMap()
		{
		
			this.ToTable("menu", "informix");

			this.HasKey(t => t.MenuId);

			this.Property(t => t.MenuId).HasColumnName("menu_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.MenuIdPai).HasColumnName("menu_id_pai").IsRequired();//.HasColumnType("integer");//integer not null//4//int?
			this.Property(t => t.Ordem).HasColumnName("ordem").IsRequired();//.HasColumnType("smallint");//smallint not null//2//Int16?
			this.Property(t => t.Tipo).HasColumnName("tipo").IsRequired().HasMaxLength(1);//.HasColumnType("char");//char(1) not null//1//string
			this.Property(t => t.Texto).HasColumnName("texto").IsRequired().HasMaxLength(60);//.HasColumnType("char");//char(60) not null//60//string
	
		}

	}
}
