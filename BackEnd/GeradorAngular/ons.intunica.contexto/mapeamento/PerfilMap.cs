using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Perfil
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfilMap : EntityTypeConfiguration<Perfil>
	{

		public PerfilMap()
		{
		
			this.ToTable("perfil", "informix");

			this.HasKey(t => t.PerfilId);

			this.Property(t => t.PerfilId).HasColumnName("perfil_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.Nome).HasColumnName("nome").IsRequired().HasMaxLength(100);//.HasColumnType("char");//char(100) not null//100//string
			this.Property(t => t.SistemaId).HasColumnName("sistema_id").IsOptional().HasMaxLength(10);//.HasColumnType("char");//char(10)//10//string
			this.HasOptional(t => t.Sistema).WithMany(t => t.PerfilLista).HasForeignKey(d => d.SistemaId);
	
		}

	}
}
