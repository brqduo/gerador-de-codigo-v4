using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para ServMenu
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServMenuMap : EntityTypeConfiguration<ServMenu>
	{

		public ServMenuMap()
		{
		
			this.ToTable("serv_menu", "informix");

			this.HasKey(t => t.ServMenuId);

			this.Property(t => t.ServMenuId).HasColumnName("serv_menu_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.MenuId).HasColumnName("menu_id").IsRequired();//.HasColumnType("integer");//integer not null//4//int?
			this.Property(t => t.ServicoId).HasColumnName("servico_id").IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.HasRequired(t => t.Menu).WithMany(t => t.ServMenuLista).HasForeignKey(d => d.MenuId);
			this.HasRequired(t => t.Servico).WithMany(t => t.ServMenuLista).HasForeignKey(d => d.ServicoId);
	
		}

	}
}
