using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Modulo
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class ModuloMap : EntityTypeConfiguration<Modulo>
	{

		public ModuloMap()
		{
		
			this.ToTable("modulo", "informix");

			this.HasKey(t => t.ModuloId);

			this.Property(t => t.ModuloId).HasColumnName("modulo_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.SistemaId).HasColumnName("sistema_id").IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.Descricao).HasColumnName("descricao").IsRequired().HasMaxLength(40);//.HasColumnType("char");//char(40) not null//40//string
			this.HasRequired(t => t.Sistema).WithMany(t => t.ModuloLista).HasForeignKey(d => d.SistemaId);
	
		}

	}
}
