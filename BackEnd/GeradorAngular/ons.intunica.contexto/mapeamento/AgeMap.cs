using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Age
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:09
    /// </remarks>
	public partial class AgeMap : EntityTypeConfiguration<Age>
	{

		public AgeMap()
		{
		
			this.ToTable("snm_age", "informix");

			this.HasKey(t => t.AgeId);

			this.Property(t => t.AgeId).HasColumnName("age_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(3);//.HasColumnType("char");//char(3) not null//3//string
			this.Property(t => t.IdoAge).HasColumnName("ido_age").IsRequired().HasMaxLength(3);//.HasColumnType("char");//char(3) not null//3//string
			this.Property(t => t.GrpecoId).HasColumnName("grpeco_id").IsOptional().HasMaxLength(10);//.HasColumnType("char");//char(10)//10//string
			this.Property(t => t.PaisId).HasColumnName("pais_id").IsRequired().HasMaxLength(2);//.HasColumnType("char");//char(2) not null//2//string
			this.Property(t => t.Dtentrada).HasColumnName("dtentrada").IsOptional();//.HasColumnType("date");//date//4//DateTime?
			this.Property(t => t.Dtdesativa).HasColumnName("dtdesativa").IsOptional();//.HasColumnType("date");//date//4//DateTime?
			this.Property(t => t.Nomelongo).HasColumnName("nomelongo").IsRequired().HasMaxLength(63);//.HasColumnType("char");//char(63) not null//63//string
			this.Property(t => t.NomeCurto).HasColumnName("nomecurto").IsRequired().HasMaxLength(30);//.HasColumnType("varchar");//varchar(30) not null//30//string
			this.Property(t => t.Nomedes).HasColumnName("nomedes").IsOptional().HasMaxLength(50);//.HasColumnType("varchar");//varchar(50)//50//string
			this.Property(t => t.TipoorgaoId).HasColumnName("tporgao_id").IsRequired();//.HasColumnType("integer");//integer not null//4//int?
			this.Property(t => t.IdoOns).HasColumnName("ido_ons").IsRequired().HasMaxLength(32);//.HasColumnType("varchar");//varchar(32) not null//32//string
	
		}

	}
}
