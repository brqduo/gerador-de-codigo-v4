using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para TipoStatususuar
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class TipoStatususuarMap : EntityTypeConfiguration<TipoStatususuar>
	{

		public TipoStatususuarMap()
		{
		
			this.ToTable("tb_tpstatususuar", "informix");

			this.HasKey(t => t.IdTipoStatususuar);

			this.Property(t => t.IdTipoStatususuar).HasColumnName("id_tpstatususuar").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.DescricaoTipoStatususuar).HasColumnName("dsc_tpstatususuar").IsRequired().HasMaxLength(20);//.HasColumnType("varchar");//varchar(20) not null//20//string
	
		}

	}
}
