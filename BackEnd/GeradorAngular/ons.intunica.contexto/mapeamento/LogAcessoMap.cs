using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para LogAcesso
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class LogAcessoMap : EntityTypeConfiguration<LogAcesso>
	{

		public LogAcessoMap()
		{
		
			this.ToTable("tb_logacesso", "informix");

			this.HasKey(t => t.IdLogAcesso);

			this.Property(t => t.IdLogAcesso).HasColumnName("id_logacesso").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.DataAcesso).HasColumnName("din_acesso").IsRequired();//.HasColumnType("datetime");//datetime not null//3594//DateTime?
			this.Property(t => t.NomeLogin).HasColumnName("nom_login").IsRequired().HasMaxLength(8);//.HasColumnType("char");//char(8) not null//8//string
			this.Property(t => t.FlgAcesso).HasColumnName("flg_acesso").IsRequired().HasMaxLength(1);//.HasColumnType("char");//char(1) not null//1//string
	
		}

	}
}
