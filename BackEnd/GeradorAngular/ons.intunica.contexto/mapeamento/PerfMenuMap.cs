using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para PerfMenu
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfMenuMap : EntityTypeConfiguration<PerfMenu>
	{

		public PerfMenuMap()
		{
		
			this.ToTable("perf_menu", "informix");

			this.HasKey(t => t.PerfmenuId);

			this.Property(t => t.PerfmenuId).HasColumnName("perfmenu_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("serial");//serial not null//4//int
			this.Property(t => t.PerfilId).HasColumnName("perfil_id").IsRequired().HasMaxLength(10);//.HasColumnType("char");//char(10) not null//10//string
			this.Property(t => t.MenuId).HasColumnName("menu_id").IsRequired();//.HasColumnType("integer");//integer not null//4//int?
			this.HasRequired(t => t.Menu).WithMany(t => t.PerfMenuLista).HasForeignKey(d => d.MenuId);
			this.HasRequired(t => t.Perfil).WithMany(t => t.PerfMenuLista).HasForeignKey(d => d.PerfilId);
	
		}

	}
}
