using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.intunica.entidade;

namespace ons.intunica.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Operacao
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class OperacaoMap : EntityTypeConfiguration<Operacao>
	{

		public OperacaoMap()
		{
		
			this.ToTable("operacao", "informix");

			this.HasKey(t => t.OperacaoId);

			this.Property(t => t.OperacaoId).HasColumnName("operacao_id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).IsRequired().HasMaxLength(15);//.HasColumnType("char");//char(15) not null//15//string
			this.Property(t => t.Descricao).HasColumnName("descricao").IsRequired().HasMaxLength(40);//.HasColumnType("char");//char(40) not null//40//string
	
		}

	}
}
