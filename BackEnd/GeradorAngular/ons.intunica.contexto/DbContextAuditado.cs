﻿using ons.common.auditoria;
using ons.common.auditoria.Models;
using ons.common.context;
using ons.common.providers.schemas;
using ons.common.utilities.Xml;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;

namespace ons.intunica.contexto
{
    public partial class DbContextAuditado : DbContext, IDbContext
    {
        protected ons.common.providers.IHelperPOP PopHelper;
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(DbContextAuditado));

        //private Type Classe { get; set; }
        public string NomeClasse { get; set; }
        //private string Metodo { get; set; }
        public string Controller { get; set; }
        public string TicketExecutor { get; set; }

        public List<ons.common.auditoria.Models.TrilhaAuditoria> Trilhas = new List<TrilhaAuditoria>();

        public DbContextAuditado(ons.common.providers.IHelperPOP popHelper, string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            PopHelper = popHelper;
        }

        #region Auditoria

        protected static object _lock = new object();
        protected static bool mapaGerado = false;

        /// <summary>
        /// Método parcial que quando implementado construir todo o Mapa das classes, este mapa ser utilizado na geração dos Logs de auditoria.
        /// </summary>
        public virtual void MontarMapa()
        {
            var mapa = ons.common.auditoria.Models.Mapa.Instance;
            mapa.Montar(this);
        }

        private ons.common.auditoria.Models.TrilhaAuditoria GerarTrilhaAuditoria<TPrincipal>()
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

            UsuarioPOP usuario;
            if (TicketExecutor != null)
                usuario = PopHelper.ObterUsuarioPOPPorTicket(TicketExecutor);
            else
                usuario = PopHelper.ObterUsuarioPOP();

            var executor = new ons.common.auditoria.Models.Executor()
            {
                Email = usuario.Email.Trim(),
                Nome = usuario.Nome.Trim(),
                Valor = string.Format("{0} - {1}", usuario.Nome.Trim(), usuario.Login.Trim())
            };

            if (Log.IsDebugEnabled)
            {
                Log.Debug("DbContextAuditado|GerarTrilhaAuditoria|Inciando a geração da trilha de auditoria.");
                stopwatch.Start();
            }

            var trilha = ons.common.auditoria.AuditoriaHelper.Fabricar<TPrincipal>(this, null, Mapa.Instance, executor);

            if (Log.IsDebugEnabled)
            {
                stopwatch.Stop();
                Log.DebugFormat("DbContextAuditado|GerarTrilhaAuditoria|Finalizando a geração da trilha de auditoria.|{0}", stopwatch.ElapsedMilliseconds);
            }   
            return trilha;
        }

        private void SalvarTrilhaAuditoria(TrilhaAuditoria trilha)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            if (Log.IsDebugEnabled)
            {
                Log.Debug("DbContextAuditado|SalvarTrilhaAuditoria|Iniciando a persistência da trilha de auditoria.");
                stopwatch.Start();
            }

            var logAuditoria = new ons.common.auditoria.Models.Logauditoria()
            {
                IdAplicacaoauditada = AuditoriaHelper.ObterAplicacaoAuditada(PopHelper.ApplicationName).IdAplicacaoauditada,
                DinExecucao = trilha.DataEvento,
                NomExecutor = trilha.Executor.Valor,
                NomFuncionalidade = string.Format("Manter {0}", Controller),
                NomRegistro = (trilha.RegistroAlterado != null ? trilha.RegistroAlterado.Valor : trilha.Entidades.First().Identificacao),
                DscTipoacaoexecutada = string.Format("{0} {1}", trilha.Acao, Controller),
                DocOperacaologada = trilha.ToXmlSerialize()
            };
            AuditoriaHelper.SalvarLog(logAuditoria);

            if (Log.IsDebugEnabled)
            {
                stopwatch.Stop();
                Log.DebugFormat("DbContextAuditado|SalvarTrilhaAuditoria|Finalizando a persistência da trilha de auditoria.|{0}", stopwatch.ElapsedMilliseconds);
            }
        }

        #endregion

        public bool SaveChanges<TPrincipal>()
        {
            MontarMapa();

            Trilhas.Add(GerarTrilhaAuditoria<TPrincipal>());
            //var trilha = GerarTrilhaAuditoria<TPrincipal>();

            int qtde = base.SaveChanges();

            //if (trilha.Entidades.Count > 0)
            //{
            //    SalvarTrilhaAuditoria(trilha);
            //}

            return qtde > 0;
        }

        public override int SaveChanges()
        {
            throw new NotSupportedException("SaveChanges() de DbContext não pode ser chamado diretamente. Por favor utilize SaveChanges<TPrincipal>()");
            // return base.SaveChanges();
        }

        public IEntitySet<TEntity> GetEntitySet<TEntity>() where TEntity : class, new()
        {
            return new EntitySet<TEntity>(this);
        }

        public IEnumerable<TEntity> Execute<TEntity>(string command, params KeyValuePair<string, object>[] parameters)
        {
            IEnumerable<TEntity> retorno;

            var parametros = parameters.Select(p => new System.Data.SqlClient.SqlParameter(p.Key, p.Value)).ToArray();

            retorno = Database.SqlQuery<TEntity>(command, parametros.ToArray()).ToList();

            return retorno;
        }

        public TEntityResponse Execute<TRequest, TEntityResponse>(string command, TRequest parameters)
        {
            IEnumerable<TEntityResponse> retorno;

            retorno = Database.SqlQuery<TEntityResponse>(command, parameters).ToList();

            if (retorno.Count() > 0)
                return retorno.ToList()[0];
            else
                return default(TEntityResponse);
        }

        public TEntityResponse Execute<TEntityResponse>(string command)
        {
            IEnumerable<TEntityResponse> retorno;

            retorno = Database.SqlQuery<TEntityResponse>(command).ToList();

            if (retorno.Count() > 0)
                return retorno.ToList()[0];
            else
                return default(TEntityResponse);
        }


        protected override void Dispose(bool disposing)
        {
            try
            {
                if (Trilhas.Count > 0)
                {
                    var tipos = (from t in Trilhas
                                 where t.RegistroAlterado != null
                                 select new { t.RegistroAlterado.NomeExterno, t.RegistroAlterado.NomeEntidade }).Distinct();


                    if (NomeClasse != null)//tenta achar o informado na requisição
                    {
                        var pesquisa = tipos.Where(t => t.NomeEntidade == NomeClasse).FirstOrDefault();
                        if (pesquisa != null)
                            Controller = pesquisa.NomeExterno;
                    }
                    if (Controller == null)//Caso ainda não tenha identificado pega o com maior indice de alterações.
                    {
                        Controller = tipos.FirstOrDefault().NomeExterno;
                        NomeClasse = tipos.FirstOrDefault().NomeEntidade;
                    }


                    ons.common.auditoria.Models.TrilhaAuditoria trilha = new TrilhaAuditoria();
                    trilha.Entidades = new List<Entidade>();

                    foreach (ons.common.auditoria.Models.TrilhaAuditoria item in Trilhas)
                    {
                        trilha.Entidades.AddRange(item.Entidades);

                        if (item.RegistroAlterado != null && item.RegistroAlterado.NomeEntidade == NomeClasse)
                        {
                            switch (item.Acao)
                            {
                                case "A":
                                    trilha.Acao = "Alterar";
                                    break;
                                case "I":
                                    trilha.Acao = "Cadastrar";
                                    break;
                                case "E":
                                    trilha.Acao = "Excluir";
                                    break;
                                default:
                                    trilha.Acao = "Consultar";
                                    break;
                            }

                            trilha.DataEvento = item.DataEvento;
                            trilha.Executor = item.Executor;
                            trilha.RegistroAlterado = item.RegistroAlterado;
                        }
                    }


                    if (trilha.Entidades.Count > 0)
                    {
                        SalvarTrilhaAuditoria(trilha);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug("Erro ao Salvar log de auditoria", ex);
                throw;
            }
            base.Dispose(disposing);
        }

        public void Dispose()
        {
            Dispose(false);
        }

    }
}
