﻿using ons.common.data.filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.comum
{
    public static class DataServiceQueryExtensions
    {
        /// <summary>
        /// Filtra a coleção por um conjunto de valores.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <typeparam name="TProp">Tipo da propriedade do objeto</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="memberExpr">Expressão contendo a propriedade do objeto</param>
        /// <param name="values">Lista de valores da propriedade do objeto</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        public static IQueryable<T> WhereIn<T, TProp>(this IQueryable<T> source, Expression<Func<T, TProp>> memberExpr, IEnumerable<TProp> values) where T : class
        {
            return source.InternalWhereIn(memberExpr, values, false);
        }
        /// <summary>
        /// Filtra a coleção por um conjunto de valores.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <typeparam name="TProp">Tipo da propriedade do objeto</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="memberExpr">Expressão contendo a propriedade do objeto</param>
        /// <param name="values">Lista de valores da propriedade do objeto</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        public static IQueryable<T> WhereNotIn<T, TProp>(this IQueryable<T> source, Expression<Func<T, TProp>> memberExpr, IEnumerable<TProp> values) where T : class
        {
            return source.InternalWhereIn(memberExpr, values, true);
        }
        /// <summary>
        /// Filtra a coleção por um conjunto de valores.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <typeparam name="TProp">Tipo da propriedade do objeto</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="memberExpr">Expressão contendo a propriedade do objeto</param>
        /// <param name="values">Lista de valores da propriedade do objeto</param>
        /// <param name="notIn">Indicador de in ou not in.</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        private static IQueryable<T> InternalWhereIn<T, TProp>(this IQueryable<T> source, Expression<Func<T, TProp>> memberExpr, IEnumerable<TProp> values, bool notIn) where T : class
        {

            var expression = source.CreateExpressionIn(memberExpr, values, notIn);

            return expression != null ? source.Where(expression).AsQueryable<T>() : source;
        }

        public static Expression<Func<T, bool>> CreateExpression<T>(this IQueryable<T> source, Expression<Func<T, bool>> memberExpr) where T : class
        {
            return memberExpr;// Expression.Property(param, memberExpr.Body.ToString());
        }

        public static Expression<Func<T, bool>> CreateExpressionIn<T, TProp>(this IQueryable<T> source, Expression<Func<T, TProp>> memberExpr, IEnumerable<TProp> values, bool notIn) where T : class
        {
            Expression<Func<T, bool>> retorno = null;
            Expression predicate = null;
            var param = memberExpr.Parameters[0];// Expression.Parameter(typeof(T), typeof(T).Name);

            bool IsFirst = true;

            // Cria a comparação para cada um dos valores assim:                 
            // IN:  t => t.Id == 1 | t.Id == 2                

            MemberExpression me = ((MemberExpression)memberExpr.Body);
            //MemberExpression me = Expression.Property(param, ((MemberExpression)memberExpr.Body).Member.Name);
            foreach (TProp val in values)
            {
                ConstantExpression ce = Expression.Constant(val, me.Type);
                Expression comparison;
                if (notIn)
                {
                    comparison = Expression.NotEqual(me, ce);
                }
                else
                {
                    comparison = Expression.Equal(me, ce);
                }

                if (IsFirst)
                {
                    predicate = comparison;
                    IsFirst = false;
                }
                else
                {
                    if (notIn)
                    {
                        predicate = Expression.And(predicate, comparison);
                    }
                    else
                    {
                        predicate = Expression.Or(predicate, comparison);
                    }
                }
            }

            if (predicate != null)
                retorno = Expression.Lambda<Func<T, bool>>(predicate, param);

            return retorno;
        }

        /// <summary>
        /// Filtra a coleção por um conjunto de valores compostos ex.: new { Prop1 = 1, Prop2 = 2 }.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="filterList">lista de dynamic com os valoes de filtro new { Prop1 = 1, Prop2 = 2 }</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        public static IQueryable<T> WhereIn<T>(this IQueryable<T> source, IEnumerable<dynamic> filterList) where T : class
        {
            return InternalWhereIn(source, filterList, false);
        }

        /// <summary>
        /// Filtra a coleção por um conjunto de valores compostos ex.: new { Prop1 = 1, Prop2 = 2 }.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="filterList">lista de dynamic com os valoes de filtro new { Prop1 = 1, Prop2 = 2 }</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        public static IQueryable<T> WhereNotIn<T>(this IQueryable<T> source, IEnumerable<dynamic> filterList) where T : class
        {
            return InternalWhereIn(source, filterList, true);
        }

        /// <summary>
        /// Filtra a coleção por um conjunto de valores compostos ex.: new { Prop1 = 1, Prop2 = 2 }.
        /// </summary>
        /// <typeparam name="T">Tipo do objeto da coleção.</typeparam>
        /// <param name="source">IQueryable<T> da consulta</param>
        /// <param name="filterList">lista de dynamic com os valoes de filtro new { Prop1 = 1, Prop2 = 2 }</param>
        /// <param name="notIn">Indicador de in ou not in.</param>
        /// <returns>IQueryable<T> com o filtro</returns>
        private static IQueryable<T> InternalWhereIn<T>(this IQueryable<T> source, IEnumerable<dynamic> filterList, bool notIn) where T : class
        {

            //pega todas as propriedades contida em T.
            var SourcePropertyInfos = typeof(T).GetProperties();

            //pega as propriedades do anonymous usadas como filtro
            var filterListPropertyInfos = filterList.GetType().GetGenericArguments().First(ga => ga.Name.Contains("AnonymousType")).GetProperties();

            if (filterListPropertyInfos.Count() == 0)
                throw new ArgumentException("Não existem propriedades na lista de filtro.");

            //verifica se as propriedades do filtro existem em T
            foreach (PropertyInfo flpi in filterListPropertyInfos)
            {
                if (!SourcePropertyInfos.Any(spi => spi.Name == flpi.Name))//&& spi.PropertyType == flpi.PropertyType
                    throw new ArgumentException("A Propriedade " + flpi.Name + ", do tipo: " + flpi.PropertyType + " não existe no tipo de origem.");
            }

            Expression predicate = null;

            ParameterExpression param = Expression.Parameter(typeof(T), "t_" + typeof(T).Name);

            foreach (var filter in filterList)
            {
                bool IsFirst = true;

                Expression filterPredicate = null;

                /*
                 * Cria a comparação para cada um dos valores assim:              
                 * IN:  t => t.Id == 1 & t.Id1 == 2                
                 * NOT IN:  t => t.Id != 1 | t.Id1 != 2
                 */
                foreach (PropertyInfo flpi in filterListPropertyInfos)
                {
                    MemberExpression me = Expression.Property(param, flpi.Name);

                    ConstantExpression ce = Expression.Constant(flpi.GetValue(filter, null), flpi.PropertyType);

                    Expression comparison;

                    if (notIn)
                    {
                        comparison = Expression.NotEqual(me, ce);
                    }
                    else
                    {
                        comparison = Expression.Equal(me, ce);
                    }

                    if (IsFirst)
                    {
                        if (notIn)
                        {
                            filterPredicate = (filterPredicate == null) ? comparison : Expression.Or(filterPredicate, comparison);
                        }
                        else
                        {
                            filterPredicate = (filterPredicate == null) ? comparison : Expression.Or(filterPredicate, comparison);
                        }

                        IsFirst = false;
                    }
                    else
                    {
                        if (notIn)
                        {
                            filterPredicate = Expression.Or(filterPredicate, comparison);
                        }
                        else
                        {
                            filterPredicate = Expression.And(filterPredicate, comparison);
                        }
                    }
                }

                /*adiciona a comparação no fim do predicado ficando assim:   :
                 * IN:  (t => t.Id == 1 & t.Id1 == 2) | (t => t.Id == 2 & t.Id1 == 3)
                 * NOT IN:  (t => t.Id != 1 | t.Id1 != 2) &  (t => t.Id != 2 | t.Id1 != 3)
                 */
                if (notIn)
                {
                    predicate = (predicate == null) ? filterPredicate : Expression.And(predicate, filterPredicate);
                }
                else
                {
                    predicate = (predicate == null) ? filterPredicate : Expression.Or(predicate, filterPredicate);
                }
            }

            return source.Where(Expression.Lambda<Func<T, bool>>(predicate, param)).AsQueryable<T>();
        }

        public static IQueryable<TProp> SelectProperty<T, TProp>(this IQueryable<T> source, string field)
        {
            // Criar o parametro "o"
            var xParameter = Expression.Parameter(typeof(T), "o");

            // Criar expressão do tipo "new T()"
            var xNew = Expression.New(typeof(T));

            // property "Field1"
            var mi = typeof(T).GetProperty(field);

            // original value "o.Field1"
            var xOriginal = Expression.Property(xParameter, mi);

            // Seta valor "Field1 = o.Field1"
            //var bindProp = Expression.Bind(mi, xOriginal);


            // inicializa o tipo "new T { Field1 = o.Field1, Field2 = o.Field2 }"
            //var xInit = Expression.MemberInit(xNew, bindProp);

            var lambda = Expression.Lambda<Func<T, TProp>>(xOriginal, xParameter);

            // compile to Func<Data, Data>
            return source.Select(lambda);
        }

        //public static IQueryable<T> Select<T>(this IQueryable<T> source, params string[] fields)
        //{
        //    Type tipoPropriedade = typeof(T);
        //    // Criar o parametro "o"
        //    var xParameter = Expression.Parameter(typeof(T), "o");

        //    // new statement "new Data()"
        //    var xNew = Expression.New(typeof(T));

        //    // cria propriedades
        //    var bindings = fields.Select(o => o.Trim())
        //        .Select(o =>
        //        {

        //            // propriedade "Field1"
        //            var mi = typeof(T).GetProperty(o);
        //            tipoPropriedade = mi.GetType();

        //            // expressao "o.Field1"
        //            var xOriginal = Expression.Property(xParameter, mi);

        //            // seta o valor "Field1 = o.Field1"
        //            return Expression.Bind(mi, xOriginal);
        //        }
        //    );

        //    // inicializa o tipo "new T { Field1 = o.Field1, Field2 = o.Field2 }"
        //    var xInit = Expression.MemberInit(xNew, bindings);

        //    // expression "o => new Data { Field1 = o.Field1, Field2 = o.Field2 }"
        //    var lambda = Expression.Lambda<Func<T, T>>(xInit, xParameter);

        //    // compile to Func<Data, Data>
        //    return source.Select(lambda);

        //}
    }
}
