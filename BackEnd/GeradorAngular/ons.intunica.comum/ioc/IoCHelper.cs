﻿using Castle.DynamicProxy;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ons.intunica.comum.ioc
{
    /// <summary>
    /// Classe de extensão para os métodos auxiliares.
    /// </summary>
    public static class IoCHelper
    {
        /// <summary>
        /// Metodo auxiliar para converter em string os parametros passados para um método.
        /// </summary>
        /// <param name="args">parametros passados para o método.</param>
        /// <returns>String com os parametros.</returns>
        public static string ArgsToString(this object[] args)
        {
            var builder = new StringBuilder();

            if (args != null)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    if (i == args.Length - 1)
                    {
                        builder.Append('\'').Append(args[i]).Append('\'');
                    }
                    else
                    {
                        builder.Append('\'').Append(args[i]).Append('\'').Append(", ");
                    }
                }
            }

            var result = builder.ToString();
            return (result == string.Empty) ? "''" : result;
        }


    }
}
