﻿using Castle.MicroKernel;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.comum.ioc
{
    /// <summary>
    /// Classe para gerêciar a instância do IWindsorContainer.
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// Instancia privada do IWindsorContainer
        /// </summary>
        private static IWindsorContainer Container { get; set; }

        /// <summary>
        /// Método para inicialização
        /// </summary>
        /// <param name="aContainer"></param>
        public static void Initialize(IWindsorContainer aContainer)
        {
            if (Container == null)
            {
                Container = aContainer;
            }
            else
            {
                Container.Parent = aContainer;
                aContainer.AddChildContainer(Container);
            }
        }

        //public virtual void AddChildContainer(IWindsorContainer childContainer)
        //{
        //    childContainer.Parent = Container;
        //    Container.Kernel.AddChildKernel(childContainer.Kernel);
        //}

        /// <summary>
        /// Obtem uma Instancia baseado em uma interface.
        /// </summary>
        /// <typeparam name="T">Tipo da Interface.</typeparam>
        /// <returns>Instância que implementa a interface.</returns>
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }


        /// <summary>
        /// Obter o Ikernel, que expõe toda a funcionalidade que o MicroKernel implementa.
        /// </summary>
        public static IKernel Kernel
        {
            get { return Container.Kernel; }
        }
    }
}
