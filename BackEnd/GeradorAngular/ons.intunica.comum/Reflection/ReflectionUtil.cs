﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ons.intunica.comum.Reflection
{
    public static class ReflectionUtil
    {
        public static IList<Type> GetTypesOfBase<T>(this Assembly assembly)
        {
            IList<Type> retorno = new List<Type>();

            foreach (var tipo in assembly.GetTypes())
            {
                if (tipo.BaseType == typeof(T))
                {
                    retorno.Add(tipo);
                }
            }

            return retorno;
        }
        public static Type GetTypeOfBase<T>(this Assembly assembly)
        {
            Type retorno = null;

            foreach (var tipo in assembly.GetTypes())
            {
                if (tipo.BaseType == typeof(T))
                {
                    retorno = tipo;
                    break;
                }
            }

            return retorno;
        }

        public static D Fill<S, D>(this S source, D destiny)
        {
            if (source != null && destiny != null)
            {
                foreach (var prop in source.GetType().GetProperties().Where(x => x.CanRead))
                {
                    var propDestiny = destiny.GetType().GetProperties().FirstOrDefault(x => x.Name == prop.Name
                        && x.PropertyType == prop.PropertyType && x.CanWrite);

                    if (propDestiny != null)
                    {
                        propDestiny.SetValue(destiny, prop.GetValue(source, null), null);
                    }
                }
            }

            return destiny;
        }
    }
}
