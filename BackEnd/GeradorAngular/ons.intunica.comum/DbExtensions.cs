﻿using ons.common.data.filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.comum
{
    public static class DbExtensions
    {
        public static IQueryable<TEntityObject> Where<TEntityObject, TFilter>(this IQueryable<TEntityObject> source, FilterMap<TFilter, TEntityObject> filter)
            where TFilter : class
            where TEntityObject : class
        {
            if (filter != null)
            {
                Expression<Func<TEntityObject, bool>> exp = filter.ResolveExpression();

                if (exp != null)
                {
                    source = source.Where(exp);
                }
            }

            return source;
        }

        public static IQueryable<TEntityObject> Page<TEntityObject>(this IQueryable<TEntityObject> source, int skip, int top) where TEntityObject : class
        {
            if (top <= 0)
            {
                source = source.Skip(skip);
            }
            else
            {
                source = source.Skip(skip).Take(top);
            }

            return source;
        }
        public static IQueryable<TEntityObject> Page<TEntityObject>(this IQueryable<TEntityObject> source, IFilter filtro) where TEntityObject : class
        {
            return source.Page(filtro.skip, filtro.top);
        }

    }
}
