import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { PessoaEntityModule } from '../Entity/Pessoas/pessoaEntity.module';

@NgModule({
  imports: [
      PessoaEntityModule
  ],
  exports: [HomeComponent],
  declarations: [HomeComponent],
  providers: [],
})
export class HomeModule {

}

