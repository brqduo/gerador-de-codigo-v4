import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ons-home',
  templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
  constructor(public route: Router) { }

  ngOnInit() {

    //
   }

   GotoUsuarios() {
    this.route.navigateByUrl('/usuarios');
   }
GotoPessoas() {
  this.route.navigateByUrl('/pessoas');
}
}
