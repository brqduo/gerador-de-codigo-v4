import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RoutingModule } from './app.routing';
import { AppComponent } from './app.component';
//import { UsuarioEntityModule } from './Entity/Usuarios/usuarioEntity.module';
import { PessoaEntityModule } from './Entity/Pessoas/pessoaEntity.module';

// Ag-Grid
import { AgGridModule } from 'ag-grid-angular';

// Bibliotecas de Ui, ou Scss frameworks
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimeNgModule } from './PrimeNg.module';
import { NgxSpinnerModule } from 'ngx-spinner';

// Home
//import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PessoasCentralRxjsService } from './Entity/Pessoas/services/pessoasCentralRxjsService.service';
import { UsuariosCentralRxjsService } from './Entity/Usuarios/services/usuariosCentralRxjsService.service';
import { HomeModule } from './home/homeModule.module';

@NgModule({
  declarations: [
    AppComponent  ],
  imports: [
    AgGridModule.withComponents([]),
    //FormsModule,
    // Bibliotecas de Ui
    MDBBootstrapModule.forRoot(),
    PrimeNgModule,
    BrowserAnimationsModule,
    
    // Modulos de Entidade
    //UsuarioEntityModule.forRoot(),
    PessoaEntityModule,
    HomeModule,
      // PessoaEntityModule.forRoot(), 
    BrowserModule,
    RoutingModule,
    NgxSpinnerModule,
    ReactiveFormsModule
  ],
  providers: [PessoasCentralRxjsService, UsuariosCentralRxjsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
