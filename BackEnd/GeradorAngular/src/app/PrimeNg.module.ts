import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { MenuModule } from 'primeng/menu';
import { ToolbarModule } from 'primeng/toolbar';
import { DialogModule } from 'primeng/dialog';
import { PanelMenuModule } from 'primeng/panelmenu';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
  imports: [
    CommonModule,
    SplitButtonModule,
    ButtonModule,
    ToastModule,
    PanelModule,
    CardModule,
    MenuModule,
    ToolbarModule,
    DialogModule,
    PanelMenuModule,
    AutoCompleteModule
  ],
  declarations: [

  ],
  providers: [

  ],
  exports: [
    CommonModule,
    SplitButtonModule,
    ButtonModule,
    ToastModule,
    PanelModule,
    CardModule,
    MenuModule,
    ToolbarModule,
    DialogModule,
    PanelMenuModule,
    AutoCompleteModule
  ]
})
export class PrimeNgModule { }
