import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as env from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseDataService<TEntidade>  {

  public dados: any[] = [];

  constructor(public http: HttpClient) {

  }

  public lerDados() {

  }

  public Incluir() {

  }

  public Excluir() {

  }

  public Listar() {

  }
  public abstract empty(): TEntidade;
}

