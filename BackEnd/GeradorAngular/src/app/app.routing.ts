import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PessoaEntityComponent } from './Entity/Pessoas/pessoaEntity.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/homeModule.module#HomeModule'
    //component: HomeComponent
  },
  {
    path: 'pessoas',
    loadChildren: './Entity/Pessoas/pessoaEntity.module#PessoaEntityModule'
  },
  {
    path: 'usuarios',
    loadChildren: './Entity/Usuarios/usuarioEntity.module#UsuarioEntityModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
