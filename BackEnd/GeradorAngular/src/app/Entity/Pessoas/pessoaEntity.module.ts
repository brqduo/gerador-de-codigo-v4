import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PessoaEntityComponent } from './pessoaEntity.component';

import { GridPessoasComponent } from './components/gridPessoas/gridPessoas.component';
import { PesquisaPessoasComponent } from './components/pesquisaPessoas/pesquisaPessoas.component';
import { ComandBarPessoasComponent } from './components/comandBarPessoas/comandBarPessoas.component';
import { PessoaEntityRoutingModule } from './pessoaEntity.routing';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { PrimeNgModule } from '../../PrimeNg.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';


const componentes = [
  PessoaEntityComponent,
  GridPessoasComponent,
  PesquisaPessoasComponent,
  ComandBarPessoasComponent
];
@NgModule({
  imports: [
    CommonModule,
    PessoaEntityRoutingModule,
    HttpClientModule,
    PrimeNgModule,
    FormsModule,
    AgGridModule.withComponents([]),
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule
  ],
  exports: [...componentes],
  declarations: [...componentes],
  providers: [],
  schemas:[NO_ERRORS_SCHEMA]
})
export class PessoaEntityModule {
  static forRoot(): ModuleWithProviders {
    const retorno: ModuleWithProviders = {
      ngModule: PessoaEntityModule,
      providers: [],
    };
    return retorno;

  }
}

