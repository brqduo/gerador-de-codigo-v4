import { Component, OnInit } from '@angular/core';
import { GridOptions, ColDef } from 'ag-grid-community';

@Component({
  selector: 'ons-pessoa-entity',
  templateUrl: 'pessoaEntity.component.html',
  styleUrls: ['./pessoaEntity.component.scss']
})

export class PessoaEntityComponent implements OnInit {
  grid: GridOptions;
  columnDefs: ColDef[] = [
    { headerName: 'Make', field: 'make', width: 100 },
    { headerName: 'Model', field: 'model', width: 100 },
    { headerName: 'Price', field: 'price', width: 100 }
  ];

  rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];
  constructor() {
    this.grid = {
      rowData: null,
      columnDefs: this.columnDefs
    };
  }

  ngOnInit() { }
}

