import { Component, OnInit, ViewChild, Optional } from '@angular/core';
import { PessoasCentralRxjsService } from '../../services/pessoasCentralRxjsService.service';
import { HttpClient } from '@angular/common/http';
import { ModalDirective } from 'angular-bootstrap-md';
import { PessoasService } from '../../services/pessoas.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PerfusuInterface } from '../../Model/perfusu.interface';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'ons-comandbar-pessoas',
  templateUrl: './comandBarPessoas.component.html',
  styleUrls: ['./comandBarPessoas.component.scss']
})


export class ComandBarPessoasComponent implements OnInit {
  @Optional()
  Pesquisa: string;
  count: number;
  rowsSelected: number = 0;
  perfUsuAtual: any;
  alertMSG:any = "";
  inputPesquisa: any = "";
  perfusuForm: any;
  perfusuFormEdit:any;
  @ViewChild('add') modalAdd: ModalDirective;
  @ViewChild('edit') modalEdit: ModalDirective;
  @ViewChild('delete') modaldelete: ModalDirective;
  @ViewChild('alert') modalalert: ModalDirective;

  constructor(public observerSrv: PessoasCentralRxjsService, 
              public pessoasSrv: PessoasService,
              public http: HttpClient,
              private spinner: NgxSpinnerService,
              private formBuilder: FormBuilder) {
    this.count = 0;
    this.observerSrv.countGrid.subscribe((c) => {
      this.count = c;
    })
    this.observerSrv.pesquisaGrid.subscribe((s) => {
      console.log('0000',s);
    });
    this.observerSrv.pesquisaInput.subscribe((s) => {
     this.inputPesquisa = s;
    });
  

    this.observerSrv.rowsSelected.subscribe((s) => {

      this.perfUsuAtual = s;
      this.rowsSelected = s.length;

      console.log("rows:", s);
    });

  }

  ngOnInit() {
    this.createFormGroup();
    this.getCount();
    
  }

  getCount(){

    this.http.get<any>("http://localhost:64673/api/perfusu").subscribe(number =>{
      this.count = number;
    })
   
  }

  createFormGroup() {
    this.perfusuForm = new FormGroup({
      PerfusuId: new FormControl(),
      UsuarId: new FormControl(),
      Usuar: new FormControl(),
      PerfilId: new FormControl(),
      AgeId:  new FormControl(),
      CosId:  new FormControl(),
    });

    this.perfusuFormEdit= new FormGroup({
      PerfusuId: new FormControl(),
      UsuarId: new FormControl(),
      Usuar: new FormControl(),
      PerfilId: new FormControl(),
      AgeId:  new FormControl(),
      CosId:  new FormControl(),
    });
  }
  
  eventPesquisa(valorDePesquisa) {
    console.log('VALOR PESQUISA ',valorDePesquisa);
    //this.observerSrv.pesquisaGrid = valorDePesquisa;
  }

  showModal(type: string) {

    switch (type) {
      case "add":
        this.modalAdd.show();
        break;
        case "edit":
        
        if (this.rowsSelected > 0){
            if(this.rowsSelected == 1)
            {
              
              //this.perfUsuAtual = this.perfUsuAtual[0].data;
              this.perfusuFormEdit.controls['PerfilId'].setValue(this.perfUsuAtual[0].data.PerfilId);
              this.perfusuFormEdit.controls['UsuarId'].setValue(this.perfUsuAtual[0].data.UsuarId);
              this.perfusuFormEdit.controls['Usuar'].setValue(this.perfUsuAtual[0].data.Usuar);
              this.perfusuFormEdit.controls['AgeId'].setValue(this.perfUsuAtual[0].data.AgeId);
              this.perfusuFormEdit.controls['CosId'].setValue(this.perfUsuAtual[0].data.CosId);
              this.modalEdit.show();
            }
            else
            {
              // duas ou mais linhas selecionadas
              this.showAlertModal();
            }
        }
        else
        {
          this.showAlertModal();
        }
        break;
        case "delete":
        if ( this.rowsSelected > 0){
          this.modaldelete.show();
        }
        else
        {
          this.showAlertModal();
        }
        break;
    
      default:
        break;
    }

  }

  showAlertModal(){
    if(this.rowsSelected > 1)
    {
      this.alertMSG = "Somente uma linha pode ser selecionada para essa ação."
    }
    if(this.rowsSelected == 0)
    {
      this.alertMSG = "Nenhuma linha selecionada."
    }
    this.modalalert.show();
  }

  showAlertModalCustom(msg){
    this.alertMSG = msg;

    this.modalalert.show();
  }

  hideModal(type: string) {

    switch (type) {
      case "add":
        this.modalAdd.hide();
        break;
        case "edit":
        this.modalEdit.hide();
        break;
        case "delete":
        this.modaldelete.hide();
        break;
        case "alert":
        this.modalalert.hide();
        break;
    
      default:
        break;
    }

  }

  deletePerf()
  {
    this.spinner.show();
     // debugger
      this.pessoasSrv.delete(this.perfUsuAtual).then(result =>{
        console.log("resul",result);
        // o correto seria o retorno do serviço porém está dando timeout em todas as tentativas.
        
      });

      setTimeout(() => {
        this.modaldelete.hide();
        this.spinner.hide();
        this.showAlertModalCustom("Registro(s) excluído(s)!"); 
        console.log("Próxima Pesquisa: ",this.inputPesquisa);
        if(this.inputPesquisa == "")
        {
          this.pessoasSrv.getDadosGrid(this.inputPesquisa,0,20);
        }
        else
        {
          this.pessoasSrv.getDadosGridZero(this.inputPesquisa);
        }
       // this.pessoasComp.pesquisar("");
      }, 2000);
     
  }

  editPerf()
  {

    let perf: PerfusuInterface = { 
      PerfilId: this.perfusuFormEdit.get("PerfilId").value,
      UsuarId: this.perfusuFormEdit.get("UsuarId").value,
      AgeId: this.perfusuFormEdit.get("AgeId").value,
      CosId: this.perfusuFormEdit.get("CosId").value,
      PerfusuId: this.perfUsuAtual[0].data.PerfusuId,//this.perfusuFormEdit.get("PerfusuId").value,
      Usuar: {
            UsuarId: this.perfusuFormEdit.get("UsuarId").value,
            UsuarNome: this.perfusuFormEdit.get("Usuar").value,  
      }, //this.perfusuFormEdit.get("Usuar").value,
      Perfil: {
         PerfilId: this.perfusuFormEdit.get("PerfilId").value,
         
      }
      
      //this.perfUsuAtual[0].data.Perfil
  };

//     perf.PerfilId = 
//     perf.UsuarId = this.perfusuFormEdit.get("UsuarId").value;
//     perf.AgeId = this.perfusuFormEdit.get("AgeId").value;
//     perf.CosId = this.perfusuFormEdit.get("CosId").value;
//     perf.PerfusuId = this.perfUsuAtual[0].data.PerfusuId;
//     perf.Usuar = this.perfusuFormEdit.get("Usuar").value;
    
    this.pessoasSrv.edit(perf).then(result =>{
      console.log("result edit",result);
      this.modalEdit.hide();
    });
  }

  addPerf()
  {


    let perf: PerfusuInterface = { 
      PerfilId: this.perfusuForm.get("PerfilId").value,
      UsuarId: this.perfusuForm.get("UsuarId").value,
      AgeId: this.perfusuForm.get("AgeId").value,
      CosId: this.perfusuForm.get("CosId").value,
      PerfusuId: null,
      Usuar: {
            UsuarId: this.perfusuForm.get("UsuarId").value,
            UsuarNome: this.perfusuForm.get("Usuar").value,  
      }, //this.perfusuForm.get("Usuar").value,
      Perfil: {
        PerfilId: this.perfusuForm.get("PerfilId").value
     }
      //Perfil: this.perfUsuAtual[0].data.Perfil
  };
    //  let perf:any = {};
  
    // perf.PerfilId = this.perfusuForm.get("PerfilId").value;
    // perf.UsuarId = this.perfusuForm.get("UsuarId").value;
    // perf.AgeId = this.perfusuForm.get("AgeId").value;
    // perf.CosId = this.perfusuForm.get("CosId").value;
    // perf.Usuar = this.perfusuForm.get("Usuar").value;

    this.pessoasSrv.add(perf).then(result =>{
      console.log("result add",result);
      this.modalAdd.hide();
    });

    
  }

}
