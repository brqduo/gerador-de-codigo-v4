import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComandBarPessoasComponent } from './comandBarPessoas.component';

describe('ComandBarPessoasComponent', () => {
  let component: ComandBarPessoasComponent;
  let fixture: ComponentFixture<ComandBarPessoasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComandBarPessoasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComandBarPessoasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
