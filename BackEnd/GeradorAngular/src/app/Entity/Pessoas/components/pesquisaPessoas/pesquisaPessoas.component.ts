import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PessoasCentralRxjsService } from '../../services/pessoasCentralRxjsService.service';
import { PessoasService } from '../../services/pessoas.service';

@Component({
  selector: 'ons-pesquisa-pessoas',
  templateUrl: './pesquisaPessoas.component.html',
  styleUrls: ['./pesquisaPessoas.component.scss']
})
export class PesquisaPessoasComponent implements OnInit {
  buscaAvancada: any = false;
  buscaSimples: any = true;
  inputPesquisa1: any;
  inputPesquisa2: any;
  inputPesquisa3: any;
  inputPesquisaAvancada: any;
  animateStringAvancada = 'hideAdvancedSearch';
  animateStringSimples = 'showAdvancedSearch';
  btnPesquisaLabel = 'Pesquisa Avançada';
  Component: any;
  constructor(private http: HttpClient,
      private observerSrv: PessoasCentralRxjsService,
      private pessoaSrv: PessoasService) {

       

       }

  ngOnInit() {
  }


  pesquisar(target) {

    
     let valuePesquisa = ((this.inputPesquisaAvancada === undefined) ? '' : this.inputPesquisaAvancada.trim());
    if (this.inputPesquisaAvancada !== undefined && this.inputPesquisaAvancada.trim() !== '') {

      this.pessoaSrv.getDadosGridZero(valuePesquisa);
      this.observerSrv.pesquisaInput.next(valuePesquisa);
    }
    else
    {
      this.pessoaSrv.getDadosGrid(valuePesquisa,0,20);
      this.observerSrv.pesquisaInput.next(valuePesquisa);
    }


  }

  pesquisaAvancada() {
    console.log('o q eu recebo?', this.buscaAvancada);


    if (this.buscaAvancada == false) {

      this.btnPesquisaLabel = 'Pesquisa Avançada';
      this.buscaAvancada = true;
      this.buscaSimples = false;

    } else {

      this.btnPesquisaLabel = 'Pesquisa Simples';
      this.buscaAvancada = false;
      this.buscaSimples = true;
    }

  }

  onEnterKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
      // console.log(event.target.value);
    }
  }

  onKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
      // console.log(event.target.value);
    }
    // (keyup)="onKeyDown($event)"
  }

}
