import { UsuarInterface } from './../../Model/usuar.interface';
import { Component, OnInit } from '@angular/core';
import { GridOptions, ColDef, GridApi, IGetRowsParams, Logger } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { PessoasCentralRxjsService } from '../../services/pessoasCentralRxjsService.service';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
@Component({
  selector: 'ons-grid-pessoas',
  templateUrl: './gridPessoas.component.html',
  styleUrls: ['./gridPessoas.component.scss']
})
export class GridPessoasComponent implements OnInit {
  grid: GridOptions;
  filtro = '';
  skpBase = 20;
  private _gridOptions: GridOptions;
  private _selectedRows: any;
  private _currentRow: any;
  private _gridApi: any;


  public get CurrentRow() {
    return this._currentRow;
  }

  public get selectedRows(): any {
    return this._selectedRows;
  }

  public get gridOptions(): GridOptions {
    return this._gridOptions;
  }

  public get gridApi(): any {
    return this._gridApi;
  }

  constructor(
    public http: HttpClient,
    public observerSrv: PessoasCentralRxjsService
  ) {
    this.observerSrv.pesquisaGrid
    .subscribe((res: string) => {
    this.onGridReady(this.grid);
 
      this.grid.api.onFilterChanged();
      this.grid.api.sizeColumnsToFit();
    });

    this.observerSrv.dadosGrid
    .subscribe((dados)=>{
      this.clear();
      let dataSource = {
        getRows(params:any) {
            params.successCallback(dados,(dados.length -1));
        }
    };
      this.grid.api.setDatasource(dataSource);
    });
  }


  clear(){
      let self = this;
      let dataSource = {
          getRows(params:any) {
              params.successCallback([],0);
          }
      };
      this.grid.api.setDatasource(dataSource);
  }

  ngOnInit() {
    this.defaultGridOptions();
  }

  defaultGridOptions(){
    this.grid = {
      rowSelection: 'multiple',
      rowModelType: 'infinite',
      groupSelectsChildren: true,
      rowMultiSelectWithClick: true,
      infiniteInitialRowCount:60,
      maxConcurrentDatasourceRequests:1,
      cacheBlockSize: 20,
      cacheOverflowSize: 2,
      defaultColDef: {
        sortable: true
      },
      onRowDoubleClicked: (event) => {
       // console.log('Ultima linha selecionada', event);
       this.grid.api.deselectAll()
       this.grid.api.selectIndex(event.rowIndex,{},{});
       this.observerSrv.currentRow = event.data;
       // this.utilSrv.ChamarFuncao(event.data, Config.IDSUBJECT.grid, Config.IDSUBJECT.rotaEditar, this.PesquisaId);
        //this.route.navigateByUrl('Editar');
      },
      onRowSelected: (event) => {
        this._currentRow = event.data;
        this.observerSrv.currentRow = this._currentRow;
        this.observerSrv.rowsSelected = this.grid.api.getSelectedNodes();
       // console.log('Ultima linha selecionada', event);
        //this.getRows(event);
      },
      rowData: null,
      pagination: false,
      columnDefs: this.inicializarColunas(),
      isExternalFilterPresent: this.externalFilterPresent.bind(this),
      doesExternalFilterPass: this.externalFilterPass.bind(this)
    };
  }

    async getRowData(startRow: number, endRow: number): Promise<any> {

    //var rowdata = [];
    var req = this.http.get<any>('http://localhost:64673/api/perfusu/Get?skip=' + startRow + '&top='+ 20).toPromise();

      var rowData = []
      await req.then(data => {
        rowData = data;
      });
      return rowData;
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
 
              var dataSource = {
                rowCount: null,
                getRows: (params: IGetRowsParams) =>  {

                    console.log("Getting datasource rows, start: " + params.startRow + ", end: " + params.endRow);
              
                this.getRowData(params.startRow, params.endRow)
                .then(data =>{ 
                  setTimeout(() => {
                  params.successCallback(data);
                console.log("data:", data);
                  }, 1000);
                });
            
                   
                }
              };
              params.api.setDatasource(dataSource);

  }


  externalFilterPresent() {
    console.log('erro aqio', this.filtro);
    
    //return this.filtro.toLowerCase() !== '';
  }

  externalFilterPass(node: any) {
    console.log('filtro', this.filtro)
    console.log('node', node);

    // if (this.filtro !== '') {
    //   if (((node.data.UsuarId + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
    //     ((node.data.PrivilegioId + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
    //     ((node.data.UsuarNome + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1) ||
    //     ((node.data.UsuarSenha + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1)
    //   ) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // } else {
    //   return true;
    // }
    //// return false;
  }

  getCount() {
    this.http.post<any>('http://localhost:64673/api/perfusu', {}).subscribe(data => {
      console.log('numero:', data);
      this.observerSrv.countGrid.subscribe((ok) => {

      });
    });
  }

  inicializarColunas() {
    const coluna: ColDef[] = [
      {
        headerName: '', field: 'select', width: 30, checkboxSelection: true, aggFunc: 'sum'
      },
      {
        headerName: 'Perfil Chave',
        field: 'Perfil',
        width: 100
      },
      {
        headerName: 'Usuar Chave',
        field: 'Usuar',
        width: 100
      },
      {
        headerName: 'Agente Chave',
        field: 'AgeId',
        width: 100
      },
      {
        headerName: 'Cos Chave',
        field: 'CosId',
        width: 100
      },
    ];

    /*
    "PerfusuId": 2,
        "UsuarId": "reinaldo",
        "PerfilId": "CONSISCO  ",
        "AgeId": null,
        "CosId": null,
        "Perfil": "Consulta dados de coleta                                                                            ",
        "Usuar": "Reinaldo Di Tota     
    */
    return coluna;
  }

  // async start() {

  //   if (this.grid.api != undefined) {
  //     console.log('o skip grid undefined', skp);
  //     var skp = this.grid.api.getVirtualRowCount() + 20;
  //     const res = await this.http.get<any>('http://localhost:64673/api/service/data/' + skp).toPromise();

  //     var datasource = {
  //       getRows: (params: IGetRowsParams) => {
  //         //this.info = "Getting datasource rows, start: " + params.startRow + ", end: " + params.endRow;

  //         this.getRowData(skp)
  //           .subscribe(data => params.successCallback(data));

  //       }
  //     };

  //     this.grid.api.setDatasource(datasource);
  //   }
  //   else {
  //     console.log('o skip grid setado', skp);
  //     var skp = 20;
  //     const res = await this.http.get<any>('http://localhost:64673/api/service/data/' + skp).toPromise();


  //     var datasource = {
  //       getRows: (params: IGetRowsParams) => {
  //         //this.info = "Getting datasource rows, start: " + params.startRow + ", end: " + params.endRow;

  //         this.getRowData(skp)
  //           .subscribe(data => {

  //             params.successCallback(data)
  //           });

  //       }
  //     };


  //     this.grid.api.setDatasource(datasource);
  //   }

  //}


  private getRowsData(skp: number): Observable<any[]> {
    // This is acting as a service call that will return just the
    // data range that you're asking for.
    var rowdata = [];
    const res = this.http.get<any>('http://localhost:64673/api/service/data/' + skp).toPromise();
    //console.log('res:',res);
    res.then(function (result) {
      console.log('result', result);
      rowdata = result;
    });


    return Observable.of(rowdata);
  }

}
