export interface Perfil {
    PerfilId: string;
    Nome?: string;
    SistemaId?: string;
    Sistema?: any;
    PerfMenuLista?: Array<any>;
    PerfUsuLista?: Array<any>;
    PermissaoLista?: Array<any>;
    LogsistemasLista?: Array<any>;
  }
  