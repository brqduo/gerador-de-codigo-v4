import { PerfusuInterface } from './perfusu.interface';
export interface UsuarInterface {
  UsuarId: string;
  PrivilegioId?: string;
  UsuarNome?: string;
  UsuarSenha?: string;
  UsuarEmail?: string;
  UsuarSenhaN?: string;
  IdTipoStatususuar?:number;
  DataStatususuar?: string;
  TipoStatususuar?: string;
  PerfUsuLista?: PerfusuInterface[];
  PermissaoLista?: string;
  LogsistemasLista?: string;
}
