import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PerfusuInterface } from '../Model/perfusu.interface';
import { PessoasCentralRxjsService } from './pessoasCentralRxjsService.service';

@Injectable({
  providedIn: 'root'
})
export class PessoasService {

  constructor(public observerSrv: PessoasCentralRxjsService,
    public http: HttpClient) { }


  getDadosGrid(texto,skip,top)
  {
    this.http.post<any>("http://localhost:64673/api/perfusu?skipD="+skip+"&topD="+top,{
      TextoFiltroSimples: texto 
    }).subscribe((retorno) => {
      console.log("PESQUISA:",retorno);
      this.observerSrv.dadosGrid.next(retorno);
      this.http.get<any>("http://localhost:64673/api/perfusu").subscribe(number =>{
        this.observerSrv.countGrid.next(number);
      })
      this.observerSrv.pesquisaGrid.next(retorno);
      this.observerSrv.pesquisaInput.next(texto);
    })
  }

  getDadosGridZero(texto)
  {
    this.http.post<any>("http://localhost:64673/api/perfusu?skipD=0&topD=0",{
      TextoFiltroSimples: texto
    }).subscribe((retorno) => {
      console.log("PESQUISA:",retorno);
      this.observerSrv.dadosGrid.next(retorno);
      let count = ((retorno.length - 1) < 0 ? 0 : (retorno.length - 1));
      this.observerSrv.countGrid.next(count);
      this.observerSrv.pesquisaInput.next(texto);
    })
  }


  delete(PerfUsu){

    let Perfs: PerfusuInterface[] = [];

    PerfUsu.forEach(element => {
      Perfs.push(element.data)
    });


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: Perfs
    };
    return this.http.delete<any>("http://localhost:64673/api/perfusu",options).toPromise();
  }

  edit(PerfUsu){
    return this.http.put<any>("http://localhost:64673/api/perfusu?id=" + PerfUsu.PerfusuId,PerfUsu).toPromise();
  }

  add(PerfUsu){
    return this.http.post<any>("http://localhost:64673/api/perfusu",PerfUsu).toPromise();
  }

}
