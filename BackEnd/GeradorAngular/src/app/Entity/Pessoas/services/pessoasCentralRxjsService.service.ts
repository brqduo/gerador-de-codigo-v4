import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class PessoasCentralRxjsService {
  private _pesquisaGrid = new Subject<string>();
  private _pesquisaInput = new Subject<any>();
  private _dadosGrid = new Subject<any>();
  private _countGrid = new Subject<any>();
  private _currentRow = new Subject<any>();
  private _rowsSelected = new Subject<any>();

  constructor() { }



  public set rowsSelected(rows: any) {
    this._rowsSelected.next(rows);
  }

  public get rowsSelected() {
    return this._rowsSelected;
  }

  public set currentRow(gridRow: any) {
    this._currentRow.next(gridRow);
  }

  public get currentRow() {
    return this._currentRow;
  }


  public set pesquisaGrid(valorParaPesquisa: any) {
    this._pesquisaGrid.next(valorParaPesquisa);
  }

  public get pesquisaGrid() {
    return this._pesquisaGrid;
  }

  public set countGrid(countPesquisa) {
    this._countGrid.next(countPesquisa);
  }

  public get countGrid() {
    return this._countGrid;
  }

  public set dadosGrid(dados) {
    this._dadosGrid.next(dados);
  }

  public get dadosGrid() {
    return this._dadosGrid;
  }

  public get pesquisaInput(){
    return this._pesquisaInput;
  }

  public set pesquisaInput(value) {
    this._pesquisaInput.next(value);
  }

}

