import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PessoaEntityComponent } from './pessoaEntity.component';
const routes: Routes = [{
  path: '',
  component: PessoaEntityComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
  declarations: [],
})
export class PessoaEntityRoutingModule { }
