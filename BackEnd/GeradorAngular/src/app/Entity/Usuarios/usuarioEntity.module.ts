import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioEntityComponent } from './usuarioEntity.component';

import { GridUsuariosComponent } from './components/gridUsuarios/gridUsuarios.component';
import { ComandBarUsuariosComponent } from './components/comandBarUsuarios/comandBarUsuarios.component';
import { PesquisaUsuariosComponent } from './components/pesquisaUsuarios/pesquisaUsuarios.component';

import { UsuarioEntityRoutingModule } from './usuarioEntity.routing';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { PrimeNgModule } from '../../PrimeNg.module';
import { FormsModule } from '@angular/forms';


const componentes = [
  UsuarioEntityComponent,
  PesquisaUsuariosComponent,
  ComandBarUsuariosComponent,
  GridUsuariosComponent
];
@NgModule({
  imports: [
    CommonModule,
    UsuarioEntityRoutingModule,
    HttpClientModule,
    PrimeNgModule,
    FormsModule,
    AgGridModule.withComponents([]),
    MDBBootstrapModule.forRoot()
  ],
  exports: [...componentes],
  declarations: [...componentes],
  providers: [],
})
export class UsuarioEntityModule {
  static forRoot(): ModuleWithProviders {
    const retorno: ModuleWithProviders = {
      ngModule: UsuarioEntityModule,
      providers: []
    };
    return retorno;
  }
}
