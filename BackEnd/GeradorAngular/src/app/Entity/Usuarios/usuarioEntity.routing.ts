import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioEntityComponent } from './usuarioEntity.component';

const routes: Routes = [{
  path: '',
  component: UsuarioEntityComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
  declarations: [],
})
export class UsuarioEntityRoutingModule { }
