import { Component, OnInit } from '@angular/core';
import { UsuariosCentralRxjsService } from '../../services/usuariosCentralRxjsService.service';

@Component({
  selector: 'ons-comandbar-usuarios',
  templateUrl: './comandBarUsuarios.component.html',
  styleUrls: ['./comandBarUsuarios.component.scss']
})
export class ComandBarUsuariosComponent implements OnInit {
  Pesquisa: string;
  constructor(public observerSrv: UsuariosCentralRxjsService) { }

  ngOnInit() {
  }
  eventPesquisa(valorDePesquisa) {
    // console.log(valorDePesquisa);
    this.observerSrv.pesquisaGrid = valorDePesquisa;
  }

}
