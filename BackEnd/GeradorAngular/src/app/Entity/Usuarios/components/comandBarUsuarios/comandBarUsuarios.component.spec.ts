import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComandBarUsuariosComponent } from './comandBarUsuarios.component';

describe('ComandBarUsuariosComponent', () => {
  let component: ComandBarUsuariosComponent;
  let fixture: ComponentFixture<ComandBarUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComandBarUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComandBarUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
