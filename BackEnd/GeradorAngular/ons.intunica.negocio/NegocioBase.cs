﻿using ons.intunica.negocio.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons.intunica.comum;
using ons.common.data.filter;
using ons.intunica.contexto.contrato;
using System.Configuration;

namespace ons.intunica.negocio
{

	/// <summary>
	/// implementação base da camada de negócio
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public abstract class NegocioBase<TEntidade> : INegocioBase<TEntidade>
	 where TEntidade : class, new()
    {
        #region " private  "

        protected ons.common.providers.IHelperPOP PopHelper;
        public abstract IQueryable<TEntidade> AplicarOrdem(IQueryable<TEntidade> query);
        public abstract IQueryable<TEntidade> AplicarAutorizacao(IQueryable<TEntidade> query);
        public abstract void ValidarCriar(TEntidade entity);
        public abstract void ValidarAtualizar(TEntidade entity);
        public abstract void ValidarExcluir(TEntidade entity);

        internal IDbContext Context
        {
            get;
            private set;
        }

        private IEntitySet<TEntidade> entitySet;

        internal IEntitySet<TEntidade> EntitySet()
        {
            if (entitySet == null)
            {
                entitySet = Context.GetEntitySet<TEntidade>();
            }

            return entitySet;
        }

        #endregion " private  "

        public NegocioBase(IDbContext context, ons.common.providers.IHelperPOP popHelper) 
		{
            Context = context;
			PopHelper = popHelper;
		}

        #region " CUD "

        /// <summary>
        /// Adiciona entidade dada ao contexto subjacente ao conjunto no estado adicionada de tal forma que irá ser inserido no banco de dados quando SaveChanges é chamado.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntidade Criar(TEntidade entity)
        {
            ValidarCriar(entity);
            return EntitySet().Add(entity);
        }

        /// <summary>
        /// Atualiza no contexto a entidade (entity). e marca como alterada.
        /// </summary>
        /// <param name="entity">Entity to attach</param>
        public void Atualizar(TEntidade entity)
        {
            ValidarAtualizar(entity);
            EntitySet().Update(entity);
        }

        public void Salvar()
        {
            Context.SaveChanges<TEntidade>();
        }

        /// <summary>
        /// Marca a entidade dada como Excluída A entidade será excluída do banco de dados Quando SaveChanges for chamado. 
        /// Note-se que a entidade deve existir no contexto em algum outro estado antes deste método ser chamado.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Excluir(TEntidade entity)
        {
            ValidarExcluir(entity);
            EntitySet().Remove(entity);
        }

        /// <summary>
        /// Cria uma nova instância de uma entidade para o tipo deste conjunto. 
        /// Lembre que este exemplar não é adicionado ou anexado ao conjunto. 
        /// A instância retornada será uma procuração se o contexto subjacente é configurado para criar proxies e do tipo de entidade atende aos requisitos para a criação de um proxy.
        /// </summary>
        /// <returns></returns>
        public TEntidade Novo()
        {
            return EntitySet().Create();
        }

        #endregion " CUD "

        /// <summary>
        /// Metodo para listar.
        /// </summary>
        /// <returns>Lista de objetos</returns>
        public IQueryable<TEntidade> Query()
        {
            IQueryable<TEntidade> consulta = EntitySet().Query();

            consulta = AplicarAutorizacao(consulta);
            consulta = AplicarOrdem(consulta);

            return consulta;
        }


        public void Dispose()
        {
            Context = null;
            entitySet = null;
            PopHelper = null;
        }
    }
}


