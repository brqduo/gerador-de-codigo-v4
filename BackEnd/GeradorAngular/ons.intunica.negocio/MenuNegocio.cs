using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class MenuNegocio : NegocioBase<Menu>, IMenuNegocio
	{
        public MenuNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Menu Carregar(int menuId)
        {
           var retorno = Query().Where(item => item.MenuId.Equals(menuId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Menu> AplicarOrdem(IQueryable<Menu> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Menu> retorno = query;

			retorno = query.OrderBy(b => b.Tipo);

            return retorno;
        }
		
		public override IQueryable<Menu> AplicarAutorizacao(IQueryable<Menu> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Menu> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Menu" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Menu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Menu deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.MenuIdPai == null)
                throw new BusinessException(@"O campo Chave Pai deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Ordem == null)
                throw new BusinessException(@"O campo Ordem deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Tipo.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Tipo deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Tipo != null && entity.Tipo.Length > 1)
                throw new BusinessException(@"O campo Tipo deve ter tamanho máximo de 1 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Texto.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Texto deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Texto != null && entity.Texto.Length > 60)
                throw new BusinessException(@"O campo Texto deve ter tamanho máximo de 60 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Menu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Menu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Menu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Menu deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PerfMenuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Perf Menu.", common.schemas.MessageType.ERROR);

            if (entity.ServMenuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Serv Menu.", common.schemas.MessageType.ERROR);

	
        }
	}
}
