using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServMenuNegocio : NegocioBase<ServMenu>, IServMenuNegocio
	{
        public ServMenuNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public ServMenu Carregar(int servMenuId)
        {
           var retorno = Query().Where(item => item.ServMenuId.Equals(servMenuId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<ServMenu> AplicarOrdem(IQueryable<ServMenu> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<ServMenu> retorno = query;

			retorno = query.OrderBy(b => b.MenuId);

            return retorno;
        }
		
		public override IQueryable<ServMenu> AplicarAutorizacao(IQueryable<ServMenu> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<ServMenu> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar ServMenu" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(ServMenu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Serv Menu deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.MenuId == null)
                throw new BusinessException(@"O campo Menu Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ServicoId == null)
                throw new BusinessException(@"O campo Serviço Chave deve ser informado.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(ServMenu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(ServMenu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(ServMenu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Serv Menu deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
