using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServidorsistemaNegocio : NegocioBase<Servidorsistema>, IServidorsistemaNegocio
	{
        public ServidorsistemaNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Servidorsistema Carregar(int idServidorsistema)
        {
           var retorno = Query().Where(item => item.IdServidorsistema.Equals(idServidorsistema)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Servidorsistema> AplicarOrdem(IQueryable<Servidorsistema> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Servidorsistema> retorno = query;

			retorno = query.OrderBy(b => b.UrlServidor);

            return retorno;
        }
		
		public override IQueryable<Servidorsistema> AplicarAutorizacao(IQueryable<Servidorsistema> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Servidorsistema> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Servidorsistema" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Servidorsistema entity)
        {
			if(entity == null)
                throw new BusinessException(@"servIDO rAplicação deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.UrlServidor.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Url servIDO r deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.UrlServidor != null && entity.UrlServidor.Length > 100)
                throw new BusinessException(@"O campo Url servIDO r deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Servidorsistema entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Servidorsistema entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Servidorsistema entity)
        {
			if(entity == null)
                throw new BusinessException(@"servIDO rAplicação deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
