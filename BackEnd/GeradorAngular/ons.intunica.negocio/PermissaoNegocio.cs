using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PermissaoNegocio : NegocioBase<Permissao>, IPermissaoNegocio
	{
        public PermissaoNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Permissao Carregar(int permissaoId)
        {
           var retorno = Query().Where(item => item.PermissaoId.Equals(permissaoId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Permissao> AplicarOrdem(IQueryable<Permissao> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Permissao> retorno = query;

			retorno = query.OrderBy(b => b.OperacaoId);

            return retorno;
        }
		
		public override IQueryable<Permissao> AplicarAutorizacao(IQueryable<Permissao> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Permissao> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Permissao" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Permissao entity)
        {
			if(entity == null)
                throw new BusinessException(@"Permissão deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.OperacaoId == null)
                throw new BusinessException(@"O campo Operação Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ServicoId == null)
                throw new BusinessException(@"O campo Serviço Chave deve ser informado.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Permissao entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Permissao entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Permissao entity)
        {
			if(entity == null)
                throw new BusinessException(@"Permissão deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
