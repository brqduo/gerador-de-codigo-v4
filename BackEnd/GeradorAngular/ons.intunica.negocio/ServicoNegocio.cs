using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class ServicoNegocio : NegocioBase<Servico>, IServicoNegocio
	{
        public ServicoNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Servico Carregar(string servicoId)
        {
           var retorno = Query().Where(item => item.ServicoId.Equals(servicoId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Servico> AplicarOrdem(IQueryable<Servico> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Servico> retorno = query;

			retorno = query.OrderBy(b => b.Descricao);

            return retorno;
        }
		
		public override IQueryable<Servico> AplicarAutorizacao(IQueryable<Servico> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Servico> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Servico" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Servico entity)
        {
			if(entity == null)
                throw new BusinessException(@"Serviço deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.ServicoId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ServicoId != null && entity.ServicoId.Length > 15)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 15 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Descricao.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Descrição deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Descricao != null && entity.Descricao.Length > 40)
                throw new BusinessException(@"O campo Descrição deve ter tamanho máximo de 40 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Path.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Path deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Path != null && entity.Path.Length > 255)
                throw new BusinessException(@"O campo Path deve ter tamanho máximo de 255 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Executavel.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Executavel deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Executavel != null && entity.Executavel.Length > 50)
                throw new BusinessException(@"O campo Executavel deve ter tamanho máximo de 50 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Querystring != null && entity.Querystring.Length > 255)
                throw new BusinessException(@"O campo Querystring deve ter tamanho máximo de 255 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.DescricaoModocompatibilidade != null && entity.DescricaoModocompatibilidade.Length > 70)
                throw new BusinessException(@"O campo Descrição Modo compatibilChave ade deve ter tamanho máximo de 70 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Servico entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Servico entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Servico entity)
        {
			if(entity == null)
                throw new BusinessException(@"Serviço deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PermissaoLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Permissão.", common.schemas.MessageType.ERROR);

            if (entity.ServMenuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Serv Menu.", common.schemas.MessageType.ERROR);

            if (entity.SistemaLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Aplicação.", common.schemas.MessageType.ERROR);

	
        }
	}
}
