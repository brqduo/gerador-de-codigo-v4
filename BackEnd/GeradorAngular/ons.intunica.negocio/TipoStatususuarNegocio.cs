using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class TipoStatususuarNegocio : NegocioBase<TipoStatususuar>, ITipoStatususuarNegocio
	{
        public TipoStatususuarNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public TipoStatususuar Carregar(int idTipoStatususuar)
        {
           var retorno = Query().Where(item => item.IdTipoStatususuar.Equals(idTipoStatususuar)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<TipoStatususuar> AplicarOrdem(IQueryable<TipoStatususuar> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<TipoStatususuar> retorno = query;

			retorno = query.OrderBy(b => b.DescricaoTipoStatususuar);

            return retorno;
        }
		
		public override IQueryable<TipoStatususuar> AplicarAutorizacao(IQueryable<TipoStatususuar> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<TipoStatususuar> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar TipoStatususuar" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(TipoStatususuar entity)
        {
			if(entity == null)
                throw new BusinessException(@"Tipo Status usuar deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.DescricaoTipoStatususuar.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Descrição Tipo Status usuar deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.DescricaoTipoStatususuar != null && entity.DescricaoTipoStatususuar.Length > 20)
                throw new BusinessException(@"O campo Descrição Tipo Status usuar deve ter tamanho máximo de 20 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(TipoStatususuar entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(TipoStatususuar entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(TipoStatususuar entity)
        {
			if(entity == null)
                throw new BusinessException(@"Tipo Status usuar deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.UsuarLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Usuar.", common.schemas.MessageType.ERROR);

	
        }
	}
}
