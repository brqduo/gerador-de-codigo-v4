using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:04
    /// </remarks>
	public partial class UsuarNegocio : NegocioBase<Usuar>, IUsuarNegocio
	{
        public UsuarNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Usuar Carregar(string usuarId)
        {
           var retorno = Query().Where(item => item.UsuarId.Equals(usuarId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Usuar> AplicarOrdem(IQueryable<Usuar> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Usuar> retorno = query;

			retorno = query.OrderBy(b => b.PrivilegioId);

            return retorno;
        }
		
		public override IQueryable<Usuar> AplicarAutorizacao(IQueryable<Usuar> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Usuar> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Usuar" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Usuar entity)
        {
			if(entity == null)
                throw new BusinessException(@"Usuar deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.UsuarId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.UsuarId != null && entity.UsuarId.Length > 8)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 8 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.PrivilegioId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Privilegio Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PrivilegioId != null && entity.PrivilegioId.Length > 1)
                throw new BusinessException(@"O campo Privilegio Chave deve ter tamanho máximo de 1 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.UsuarNome.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.UsuarNome != null && entity.UsuarNome.Length > 100)
                throw new BusinessException(@"O campo Nome deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.UsuarSenha != null && entity.UsuarSenha.Length > 100)
                throw new BusinessException(@"O campo Senha deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.UsuarEmail != null && entity.UsuarEmail.Length > 100)
                throw new BusinessException(@"O campo E-mail deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.UsuarSenhaN != null && entity.UsuarSenhaN.Length > 100)
                throw new BusinessException(@"O campo Senha N deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Usuar entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Usuar entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Usuar entity)
        {
			if(entity == null)
                throw new BusinessException(@"Usuar deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PerfUsuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Perf Usu.", common.schemas.MessageType.ERROR);

            if (entity.PermissaoLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Permissão.", common.schemas.MessageType.ERROR);

            if (entity.LogsistemasLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Log Aplicação s.", common.schemas.MessageType.ERROR);

	
        }
	}
}
