using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class SistemaNegocio : NegocioBase<Sistema>, ISistemaNegocio
	{
        public SistemaNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Sistema Carregar(string sistemaId)
        {
           var retorno = Query().Where(item => item.SistemaId.Equals(sistemaId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Sistema> AplicarOrdem(IQueryable<Sistema> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Sistema> retorno = query;

			retorno = query.OrderBy(b => b.Descricao);

            return retorno;
        }
		
		public override IQueryable<Sistema> AplicarAutorizacao(IQueryable<Sistema> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Sistema> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Sistema" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Sistema entity)
        {
			if(entity == null)
                throw new BusinessException(@"Aplicação deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.SistemaId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.SistemaId != null && entity.SistemaId.Length > 10)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 10 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Descricao.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Descrição deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Descricao != null && entity.Descricao.Length > 40)
                throw new BusinessException(@"O campo Descrição deve ter tamanho máximo de 40 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.EmailResp != null && entity.EmailResp.Length > 40)
                throw new BusinessException(@"O campo E-mail Resp deve ter tamanho máximo de 40 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.FlgLoginrequerido != null && entity.FlgLoginrequerido.Length > 1)
                throw new BusinessException(@"O campo Login RequerIDO deve ter tamanho máximo de 1 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.FlgDesativado != null && entity.FlgDesativado.Length > 1)
                throw new BusinessException(@"O campo Desativado deve ter tamanho máximo de 1 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Sistema entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Sistema entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Sistema entity)
        {
			if(entity == null)
                throw new BusinessException(@"Aplicação deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ModuloLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Módulo.", common.schemas.MessageType.ERROR);

            if (entity.PerfilLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Perfil.", common.schemas.MessageType.ERROR);

            if (entity.ServidorsistemaLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com servIDO rAplicação.", common.schemas.MessageType.ERROR);

	
        }
	}
}
