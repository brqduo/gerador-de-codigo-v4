using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class OperacaoNegocio : NegocioBase<Operacao>, IOperacaoNegocio
	{
        public OperacaoNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Operacao Carregar(string operacaoId)
        {
           var retorno = Query().Where(item => item.OperacaoId.Equals(operacaoId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Operacao> AplicarOrdem(IQueryable<Operacao> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Operacao> retorno = query;

			retorno = query.OrderBy(b => b.Descricao);

            return retorno;
        }
		
		public override IQueryable<Operacao> AplicarAutorizacao(IQueryable<Operacao> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Operacao> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Operacao" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Operacao entity)
        {
			if(entity == null)
                throw new BusinessException(@"Operação deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.OperacaoId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.OperacaoId != null && entity.OperacaoId.Length > 15)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 15 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Descricao.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Descrição deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Descricao != null && entity.Descricao.Length > 40)
                throw new BusinessException(@"O campo Descrição deve ter tamanho máximo de 40 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Operacao entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Operacao entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Operacao entity)
        {
			if(entity == null)
                throw new BusinessException(@"Operação deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PermissaoLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Permissão.", common.schemas.MessageType.ERROR);

	
        }
	}
}
