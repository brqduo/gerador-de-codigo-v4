using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public partial class ModuloNegocio : NegocioBase<Modulo>, IModuloNegocio
	{
        public ModuloNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Modulo Carregar(string moduloId)
        {
           var retorno = Query().Where(item => item.ModuloId.Equals(moduloId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Modulo> AplicarOrdem(IQueryable<Modulo> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Modulo> retorno = query;

			retorno = query.OrderBy(b => b.Descricao);

            return retorno;
        }
		
		public override IQueryable<Modulo> AplicarAutorizacao(IQueryable<Modulo> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Modulo> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Modulo" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Modulo entity)
        {
			if(entity == null)
                throw new BusinessException(@"Módulo deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.ModuloId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ModuloId != null && entity.ModuloId.Length > 10)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 10 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.SistemaId == null)
                throw new BusinessException(@"O campo Aplicação Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Descricao.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Descrição deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Descricao != null && entity.Descricao.Length > 40)
                throw new BusinessException(@"O campo Descrição deve ter tamanho máximo de 40 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Modulo entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Modulo entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Modulo entity)
        {
			if(entity == null)
                throw new BusinessException(@"Módulo deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.ServicoLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Serviço.", common.schemas.MessageType.ERROR);

	
        }
	}
}
