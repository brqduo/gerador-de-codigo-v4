using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:09
    /// </remarks>
	public partial class AgeNegocio : NegocioBase<Age>, IAgeNegocio
	{
        public AgeNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Age Carregar(string ageId)
        {
           var retorno = Query().Where(item => item.AgeId.Equals(ageId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Age> AplicarOrdem(IQueryable<Age> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Age> retorno = query;

			retorno = query.OrderBy(b => b.Nomelongo);

            return retorno;
        }
		
		public override IQueryable<Age> AplicarAutorizacao(IQueryable<Age> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Age> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Age" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Age entity)
        {
			if(entity == null)
                throw new BusinessException(@"Agente deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.AgeId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.AgeId != null && entity.AgeId.Length > 3)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 3 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.IdoAge.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo IDO deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdoAge != null && entity.IdoAge.Length > 3)
                throw new BusinessException(@"O campo IDO deve ter tamanho máximo de 3 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.GrpecoId != null && entity.GrpecoId.Length > 10)
                throw new BusinessException(@"O campo GRPECO Chave deve ter tamanho máximo de 10 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.PaisId == null)
                throw new BusinessException(@"O campo Pai sChave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Nomelongo.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome longo deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Nomelongo != null && entity.Nomelongo.Length > 63)
                throw new BusinessException(@"O campo Nome longo deve ter tamanho máximo de 63 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.NomeCurto.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome Curto deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeCurto != null && entity.NomeCurto.Length > 30)
                throw new BusinessException(@"O campo Nome Curto deve ter tamanho máximo de 30 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Nomedes != null && entity.Nomedes.Length > 50)
                throw new BusinessException(@"O campo Nome des deve ter tamanho máximo de 50 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.TipoorgaoId == null)
                throw new BusinessException(@"O campo Tipo Orgão Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdoOns.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo IDO ONS deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdoOns != null && entity.IdoOns.Length > 32)
                throw new BusinessException(@"O campo IDO ONS deve ter tamanho máximo de 32 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Age entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Age entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Age entity)
        {
			if(entity == null)
                throw new BusinessException(@"Agente deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
