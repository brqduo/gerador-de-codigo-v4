using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfMenuNegocio : NegocioBase<PerfMenu>, IPerfMenuNegocio
	{
        public PerfMenuNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public PerfMenu Carregar(int perfmenuId)
        {
           var retorno = Query().Where(item => item.PerfmenuId.Equals(perfmenuId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<PerfMenu> AplicarOrdem(IQueryable<PerfMenu> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<PerfMenu> retorno = query;

			retorno = query.OrderBy(b => b.PerfilId);

            return retorno;
        }
		
		public override IQueryable<PerfMenu> AplicarAutorizacao(IQueryable<PerfMenu> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<PerfMenu> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar PerfMenu" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(PerfMenu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perf Menu deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.PerfilId == null)
                throw new BusinessException(@"O campo Perfil Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.MenuId == null)
                throw new BusinessException(@"O campo Menu Chave deve ser informado.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(PerfMenu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(PerfMenu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(PerfMenu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perf Menu deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
