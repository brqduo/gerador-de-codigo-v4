using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:11
    /// </remarks>
	public partial class CosNegocio : NegocioBase<Cos>, ICosNegocio
	{
        public CosNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Cos Carregar(string cosId)
        {
           var retorno = Query().Where(item => item.CosId.Equals(cosId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Cos> AplicarOrdem(IQueryable<Cos> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Cos> retorno = query;

			retorno = query.OrderBy(b => b.NomeCurto);

            return retorno;
        }
		
		public override IQueryable<Cos> AplicarAutorizacao(IQueryable<Cos> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Cos> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Cos" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Cos entity)
        {
			if(entity == null)
                throw new BusinessException(@"Cos deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.CosId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.CosId != null && entity.CosId.Length > 2)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 2 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.NomeCurto.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome Curto deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeCurto != null && entity.NomeCurto.Length > 15)
                throw new BusinessException(@"O campo Nome Curto deve ter tamanho máximo de 15 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Nomelongo.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome longo deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Nomelongo != null && entity.Nomelongo.Length > 60)
                throw new BusinessException(@"O campo Nome longo deve ter tamanho máximo de 60 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Nomedes != null && entity.Nomedes.Length > 50)
                throw new BusinessException(@"O campo Nome des deve ter tamanho máximo de 50 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.TipoorgaoId == null)
                throw new BusinessException(@"O campo Tipo Orgão Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.CosIdResp == null)
                throw new BusinessException(@"O campo Chave Resp deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdoOns.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo IDO ONS deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdoOns != null && entity.IdoOns.Length > 32)
                throw new BusinessException(@"O campo IDO ONS deve ter tamanho máximo de 32 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Mrid != null && entity.Mrid.Length > 43)
                throw new BusinessException(@"O campo mrChave deve ter tamanho máximo de 43 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Cos entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Cos entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Cos entity)
        {
			if(entity == null)
                throw new BusinessException(@"Cos deve ser informado.", common.schemas.MessageType.ERROR);

           

	
        }
	}
}
