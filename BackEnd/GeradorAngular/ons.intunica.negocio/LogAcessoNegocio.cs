using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class LogAcessoNegocio : NegocioBase<LogAcesso>, ILogAcessoNegocio
	{
        public LogAcessoNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public LogAcesso Carregar(int idLogAcesso)
        {
           var retorno = Query().Where(item => item.IdLogAcesso.Equals(idLogAcesso)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<LogAcesso> AplicarOrdem(IQueryable<LogAcesso> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<LogAcesso> retorno = query;

			retorno = query.OrderBy(b => b.NomeLogin);

            return retorno;
        }
		
		public override IQueryable<LogAcesso> AplicarAutorizacao(IQueryable<LogAcesso> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<LogAcesso> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar LogAcesso" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(LogAcesso entity)
        {
			if(entity == null)
                throw new BusinessException(@"Log Acesso deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.DataAcesso == null)
                throw new BusinessException(@"O campo Data Acesso deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeLogin.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome Login deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeLogin != null && entity.NomeLogin.Length > 8)
                throw new BusinessException(@"O campo Nome Login deve ter tamanho máximo de 8 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.FlgAcesso.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Acesso deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.FlgAcesso != null && entity.FlgAcesso.Length > 1)
                throw new BusinessException(@"O campo Acesso deve ter tamanho máximo de 1 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(LogAcesso entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(LogAcesso entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(LogAcesso entity)
        {
			if(entity == null)
                throw new BusinessException(@"Log Acesso deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
