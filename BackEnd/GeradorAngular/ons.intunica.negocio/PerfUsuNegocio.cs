using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfUsuNegocio : NegocioBase<PerfUsu>, IPerfUsuNegocio
	{
        public PerfUsuNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public PerfUsu Carregar(int perfusuId)
        {
           var retorno = Query().Where(item => item.PerfusuId.Equals(perfusuId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<PerfUsu> AplicarOrdem(IQueryable<PerfUsu> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<PerfUsu> retorno = query;

			retorno = query.OrderBy(b => b.AgeId);

            return retorno;
        }
		
		public override IQueryable<PerfUsu> AplicarAutorizacao(IQueryable<PerfUsu> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<PerfUsu> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar PerfUsu" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(PerfUsu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perf Usu deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.UsuarId == null)
                throw new BusinessException(@"O campo Usuar Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PerfilId == null)
                throw new BusinessException(@"O campo Perfil Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.AgeId != null && entity.AgeId.Length > 3)
                throw new BusinessException(@"O campo Agente Chave deve ter tamanho máximo de 3 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.CosId != null && entity.CosId.Length > 2)
                throw new BusinessException(@"O campo Cos Chave deve ter tamanho máximo de 2 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(PerfUsu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(PerfUsu entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(PerfUsu entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perf Usu deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}
