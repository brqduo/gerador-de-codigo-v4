using ons.intunica.entidade;
using ons.intunica.comum;
using ons.intunica.negocio.contrato;
using ons.intunica.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.intunica.negocio
{

	/// <summary>
	/// Camada de negócio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	public partial class PerfilNegocio : NegocioBase<Perfil>, IPerfilNegocio
	{
        public PerfilNegocio(IIntunicaDbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Perfil Carregar(string perfilId)
        {
           var retorno = Query().Where(item => item.PerfilId.Equals(perfilId)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Perfil> AplicarOrdem(IQueryable<Perfil> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Perfil> retorno = query;

			retorno = query.OrderBy(b => b.Nome);

            return retorno;
        }
		
		public override IQueryable<Perfil> AplicarAutorizacao(IQueryable<Perfil> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Perfil> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Perfil" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Perfil entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perfil deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.PerfilId.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Chave deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PerfilId != null && entity.PerfilId.Length > 10)
                throw new BusinessException(@"O campo Chave deve ter tamanho máximo de 10 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.Nome.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.Nome != null && entity.Nome.Length > 100)
                throw new BusinessException(@"O campo Nome deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Perfil entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Perfil entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Perfil entity)
        {
			if(entity == null)
                throw new BusinessException(@"Perfil deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PerfMenuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Perf Menu.", common.schemas.MessageType.ERROR);

            if (entity.PerfUsuLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Perf Usu.", common.schemas.MessageType.ERROR);

            if (entity.PermissaoLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Permissão.", common.schemas.MessageType.ERROR);

            if (entity.LogsistemasLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Log Aplicação s.", common.schemas.MessageType.ERROR);

	
        }
	}
}
