﻿using System;
using System.Collections.Generic;

namespace ons.intunica.contexto.contrato
{

    /// <summary>
    /// interface do DbContext de acesso a dados.
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public interface IDbContext : IDisposable
    {
        /// <summary>
        /// Metodo que salva os dados
        /// </summary>
        /// <typeparam name="TPrincipal">Tipo do objeto principal que foi alterado.</typeparam>
        /// <returns>Indicador de sucesso.</returns>
        bool SaveChanges<TPrincipal>();

        /// <summary>
        /// Obtem um "DbSet" de manipulação dos dados de uma entidade.
        /// </summary>
        /// <typeparam name="TEntity">Entidade a ser manipulada.</typeparam>
        /// <returns>"DbSet" de manipulação dos dados de uma entidade</returns>
        IEntitySet<TEntity> GetEntitySet<TEntity>() where TEntity : class, new();

        /// <summary>
        /// Executa comandos diretos fugindo da estrutura contida em um IEntitySet
        /// </summary>
        /// <typeparam name="TElement">Tipo de retorno</typeparam>
        /// <param name="command">Comando a ser executado</param>
        /// <param name="parameters">Parametros. Opcional.</param>
        /// <returns>Lista contendo o resultado.</returns>
        IEnumerable<TElement> Execute<TElement>(string command, params KeyValuePair<string, object>[] parameters);

        /// <summary>
        /// Executa chamada a api via Post
        /// </summary>
        /// <typeparam name="TRequest">Tipo de objeto que vai no body da mensagem</typeparam>
        /// <typeparam name="TEntityResponse">Tipo de retorno</typeparam>
        /// <param name="command">Comando a ser executado</param>
        /// <param name="parameters">Parametros. Opcional.</param>
        /// <returns>Resultado do tipo informado em TEntityResponse.</returns>
        TEntityResponse Execute<TRequest, TEntityResponse>(string command, TRequest parameters);

        /// <summary>
        /// Executa chamada a api via Get
        /// </summary>
        /// <typeparam name="TEntityResponse">Tipo de retorno</typeparam>
        /// <param name="command">Comando a ser executado</param>
        /// <returns>Resultado do tipo informado em TEntityResponse.</returns>
        TEntityResponse Execute<TEntityResponse>(string command);
    }
}


