using System;
using System.Collections.Generic;

namespace ons.intunica.contexto.contrato
{

	/// <summary>
	/// interface do DbContext de acesso a dados do Intunica
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public interface IIntunicaDbContext : IDbContext
    {
        
    }
}


