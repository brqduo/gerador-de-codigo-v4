﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.contexto.contrato
{
    /// <summary>
    /// interface do EntitySet de acesso a dados da entidade.
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public interface IEntitySet<TEntity> : IDisposable
        where TEntity : class, new()
    {

        // Summary:
        //     Adds the given entity to the context underlying the set in the Added state
        //     such that it will be inserted into the database when SaveChanges is called.
        //
        // Parameters:
        //   entity:
        //     The entity to add.
        //
        // Returns:
        //     The entity.
        //
        // Remarks:
        //     Note that entities that are already in the context in some other state will
        //     have their state set to Added. Add is a no-op if the entity is already in
        //     the context in the Added state.
        TEntity Add(TEntity entity);

        //
        // Summary:
        //     Attaches the given entity to the context underlying the set. That is, the
        //     entity is placed into the context in the Unchanged state, just as if it had
        //     been read from the database.
        //
        // Parameters:
        //   entity:
        //     The entity to attach.
        //
        // Returns:
        //     The entity.
        //
        // Remarks:
        //     Attach is used to repopulate a context with an entity that is known to already
        //     exist in the database.  SaveChanges will therefore not attempt to insert
        //     an attached entity into the database because it is assumed to already be
        //     there.  Note that entities that are already in the context in some other
        //     state will have their state set to Unchanged. Attach is a no-op if the entity
        //     is already in the context in the Unchanged state.
        TEntity Attach(TEntity entity);

        //
        // Summary:
        //     Creates a new instance of an entity for the type of this set.  Note that
        //     this instance is NOT added or attached to the set.  The instance returned
        //     will be a proxy if the underlying context is configured to create proxies
        //     and the entity type meets the requirements for creating a proxy.
        //
        // Returns:
        //     The entity instance, which may be a proxy.
        TEntity Create();

        //
        // Summary:
        //     Marks the given entity as Deleted such that it will be deleted from the database
        //     when SaveChanges is called. Note that the entity must exist in the context
        //     in some other state before this method is called.
        //
        // Parameters:
        //   entity:
        //     The entity to remove.
        //
        // Returns:
        //     The entity.
        //
        // Remarks:
        //     Note that if the entity exists in the context in the Added state, then this
        //     method will cause it to be detached from the context. This is because an
        //     Added entity is assumed not to exist in the database such that trying to
        //     delete it does not make sense.
        void Remove(TEntity entity);

        /// <summary>
        /// Atualiza no contexto a entidade (entity). e marca como alterada.
        /// </summary>
        /// <param name="entity">Entity to attach</param>
        void Update(TEntity entity);


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TProp">Tipo da propriedade</typeparam>
        /// <param name="entity">Entity to attach</param>
        /// <param name="propertyName">Propriedade</param>
        /// <returns></returns>
        TProp GetOriginalValues<TProp>(TEntity entity, System.Linq.Expressions.Expression<Func<TEntity, TProp>> propertyName);

        /// <summary>
        /// IQueryable de consulta da entidade.
        /// </summary>
        /// <returns></returns>
        System.Linq.IQueryable<TEntity> Query();
    }
}
