using ons.intunica.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.negocio.contrato
{

	/// <summary>
	/// Repositorio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	public interface IOperacaoNegocio : INegocioBase<Operacao>
	{

		
		Operacao Carregar(string operacaoId);
	}
}
