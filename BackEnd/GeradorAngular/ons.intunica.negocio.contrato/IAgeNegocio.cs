using ons.intunica.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.negocio.contrato
{

	/// <summary>
	/// Repositorio para: 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:09
    /// </remarks>
	public interface IAgeNegocio : INegocioBase<Age>
	{

		
		Age Carregar(string ageId);
	}
}
