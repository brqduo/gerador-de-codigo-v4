﻿using ons.common.data.filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.intunica.negocio.contrato
{

	/// <summary>
	/// interface base da camada de negócio
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation
    /// </remarks>
    public interface INegocioBase<TEntidade>  : IDisposable
	 where TEntidade : class, new()
    {
        /// <summary>
        /// Adiciona entidade dada ao contexto subjacente ao conjunto no estado adicionada de tal forma que irá ser inserido no banco de dados quando SaveChanges é chamado.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntidade Criar(TEntidade entity);
        void Salvar();
        /// <summary>
        /// Atualiza no contexto a entidade (entity). e marca como alterada.
        /// </summary>
        /// <param name="entity">Entity to attach</param>
        void Atualizar(TEntidade entity);

        /// <summary>
        /// Marca a entidade dada como Excluída A entidade será excluída do banco de dados Quando SaveChanges for chamado. 
        /// Note-se que a entidade deve existir no contexto em algum outro estado antes deste método ser chamado.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        void Excluir(TEntidade entity);

        /// <summary>
        /// Cria uma nova instância de uma entidade para o tipo deste conjunto. 
        /// Lembre que este exemplar não é adicionado ou anexado ao conjunto. 
        /// A instância retornada será uma procuração se o contexto subjacente é configurado para criar proxies e do tipo de entidade atende aos requisitos para a criação de um proxy.
        /// </summary>
        /// <returns></returns>
        TEntidade Novo();

        IQueryable<TEntidade> AplicarOrdem(IQueryable<TEntidade> query);
        IQueryable<TEntidade> AplicarAutorizacao(IQueryable<TEntidade> query);
        void ValidarCriar(TEntidade entity);
        void ValidarAtualizar(TEntidade entity);
        void ValidarExcluir(TEntidade entity);

        IQueryable<TEntidade> Query();
    }
}


