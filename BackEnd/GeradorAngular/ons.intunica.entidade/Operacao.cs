using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Operacao", Namespace = "http://schemas.ons.org.br/2013/04/Operacao")]
	[DataServiceKey("OperacaoId")]
	public partial class Operacao
	{
	
		public Operacao()
		{
			PermissaoLista = new List<Permissao>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "OperacaoId")]
		public virtual string OperacaoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Descricao")]
		public virtual string Descricao { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PermissaoLista")]
		public virtual IList<Permissao> PermissaoLista { get; set; }
	

		#endregion

	}
}
