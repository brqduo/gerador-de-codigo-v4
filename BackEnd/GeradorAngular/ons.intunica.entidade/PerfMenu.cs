using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "PerfMenu", Namespace = "http://schemas.ons.org.br/2013/04/PerfMenu")]
	[DataServiceKey("PerfmenuId")]
	public partial class PerfMenu
	{
	
		public PerfMenu()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfmenuId")]
		public virtual int PerfmenuId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfilId")]
		public virtual string PerfilId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "MenuId")]
		public virtual int? MenuId { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Menu")]
		public virtual Menu Menu { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Perfil")]
		public virtual Perfil Perfil { get; set; }
	

		#endregion

	}
}
