using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "TipoStatususuar", Namespace = "http://schemas.ons.org.br/2013/04/TipoStatususuar")]
	[DataServiceKey("IdTipoStatususuar")]
	public partial class TipoStatususuar
	{
	
		public TipoStatususuar()
		{
			UsuarLista = new List<Usuar>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdTipoStatususuar")]
		public virtual int IdTipoStatususuar { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DescricaoTipoStatususuar")]
		public virtual string DescricaoTipoStatususuar { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "UsuarLista")]
		public virtual IList<Usuar> UsuarLista { get; set; }
	

		#endregion

	}
}
