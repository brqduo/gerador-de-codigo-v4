using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Permissao", Namespace = "http://schemas.ons.org.br/2013/04/Permissao")]
	[DataServiceKey("PermissaoId")]
	public partial class Permissao
	{
	
		public Permissao()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PermissaoId")]
		public virtual int PermissaoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "OperacaoId")]
		public virtual string OperacaoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarId")]
		public virtual string UsuarId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ServicoId")]
		public virtual string ServicoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfilId")]
		public virtual string PerfilId { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Operacao")]
		public virtual Operacao Operacao { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Servico")]
		public virtual Servico Servico { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Perfil")]
		public virtual Perfil Perfil { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Usuar")]
		public virtual Usuar Usuar { get; set; }
	

		#endregion

	}
}
