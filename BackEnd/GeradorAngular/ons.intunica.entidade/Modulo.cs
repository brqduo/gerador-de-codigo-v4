using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Modulo", Namespace = "http://schemas.ons.org.br/2013/04/Modulo")]
	[DataServiceKey("ModuloId")]
	public partial class Modulo
	{
	
		public Modulo()
		{
			ServicoLista = new List<Servico>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ModuloId")]
		public virtual string ModuloId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "SistemaId")]
		public virtual string SistemaId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Descricao")]
		public virtual string Descricao { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Sistema")]
		public virtual Sistema Sistema { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "ServicoLista")]
		public virtual IList<Servico> ServicoLista { get; set; }
	

		#endregion

	}
}
