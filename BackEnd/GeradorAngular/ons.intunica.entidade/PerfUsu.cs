using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{


    /// <summary>
    /// 
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
    [Serializable()]
    [DataContract(Name = "PerfUsu", Namespace = "http://schemas.ons.org.br/2013/04/PerfUsu")]
    [DataServiceKey("PerfusuId")]
    public partial class PerfUsu
    {

        public PerfUsu()
        {
        }


        #region "Propriedades"

        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "PerfusuId")]
        public virtual int PerfusuId { get; set; }
        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "UsuarId")]
        public virtual string UsuarId { get; set; }
        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "PerfilId")]
        public virtual string PerfilId { get; set; }
        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "AgeId")]
        public virtual string AgeId { get; set; }
        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "CosId")]
        public virtual string CosId { get; set; }
        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "Cos")]
        public virtual Cos Cos { get; set; }

        /// <summary>
        ///	
        ///</summary>
        [DataMember(Name = "Age")]
        public virtual Age Age { get; set; }

        /// <summary>
        /// [relationNotAssTables]
        ///	
        ///</summary>
        [DataMember(Name = "Perfil")]
        public virtual Perfil Perfil { get; set; }
        /// <summary>
        /// [relationNotAssTables]
        ///	
        ///</summary>
        [DataMember(Name = "Usuar")]
        public virtual Usuar Usuar { get; set; }


        #endregion

    }
}
