using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Perfil", Namespace = "http://schemas.ons.org.br/2013/04/Perfil")]
	[DataServiceKey("PerfilId")]
	public partial class Perfil
	{
	
		public Perfil()
		{
			PerfMenuLista = new List<PerfMenu>();
			PerfUsuLista = new List<PerfUsu>();
			PermissaoLista = new List<Permissao>();
			LogsistemasLista = new List<Logsistemas>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfilId")]
		public virtual string PerfilId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Nome")]
		public virtual string Nome { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "SistemaId")]
		public virtual string SistemaId { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Sistema")]
		public virtual Sistema Sistema { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PerfMenuLista")]
		public virtual IList<PerfMenu> PerfMenuLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PerfUsuLista")]
		public virtual IList<PerfUsu> PerfUsuLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PermissaoLista")]
		public virtual IList<Permissao> PermissaoLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "LogsistemasLista")]
		public virtual IList<Logsistemas> LogsistemasLista { get; set; }
	

		#endregion

	}
}
