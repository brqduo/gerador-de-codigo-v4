using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Servico", Namespace = "http://schemas.ons.org.br/2013/04/Servico")]
	[DataServiceKey("ServicoId")]
	public partial class Servico
	{
	
		public Servico()
		{
			PermissaoLista = new List<Permissao>();
			ServMenuLista = new List<ServMenu>();
			SistemaLista = new List<Sistema>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ServicoId")]
		public virtual string ServicoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ModuloId")]
		public virtual string ModuloId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Descricao")]
		public virtual string Descricao { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Path")]
		public virtual string Path { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Executavel")]
		public virtual string Executavel { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Querystring")]
		public virtual string Querystring { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DescricaoModocompatibilidade")]
		public virtual string DescricaoModocompatibilidade { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Modulo")]
		public virtual Modulo Modulo { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PermissaoLista")]
		public virtual IList<Permissao> PermissaoLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "ServMenuLista")]
		public virtual IList<ServMenu> ServMenuLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "SistemaLista")]
		public virtual IList<Sistema> SistemaLista { get; set; }
	

		#endregion

	}
}
