using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:11
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Cos", Namespace = "http://schemas.ons.org.br/2013/04/Cos")]
	[DataServiceKey("CosId")]
	public partial class Cos
	{
	
		public Cos()
		{
            PerfUsuLista = new List<PerfUsu>();
        }


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "CosId")]
		public virtual string CosId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "NomeCurto")]
		public virtual string NomeCurto { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Nomelongo")]
		public virtual string Nomelongo { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Dtentrada")]
		public virtual DateTime? Dtentrada { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Dtdesativa")]
		public virtual DateTime? Dtdesativa { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Nomedes")]
		public virtual string Nomedes { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "TipoorgaoId")]
		public virtual int? TipoorgaoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "CosIdResp")]
		public virtual string CosIdResp { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ValorLatitudeinicial")]
		public virtual float? ValorLatitudeinicial { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ValorLongitudeinicial")]
		public virtual float? ValorLongitudeinicial { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ValorLatitudefinal")]
		public virtual float? ValorLatitudefinal { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ValorLongitudefinal")]
		public virtual float? ValorLongitudefinal { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdoOns")]
		public virtual string IdoOns { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Mrid")]
		public virtual string Mrid { get; set; }

        /// <summary>
        /// [relation1xNTables]
        ///	
        ///</summary>
        [DataMember(Name = "PerfUsuLista")]
        public virtual IList<PerfUsu> PerfUsuLista { get; set; }

        #endregion

    }
}
