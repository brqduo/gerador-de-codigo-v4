using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "ServMenu", Namespace = "http://schemas.ons.org.br/2013/04/ServMenu")]
	[DataServiceKey("ServMenuId")]
	public partial class ServMenu
	{
	
		public ServMenu()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ServMenuId")]
		public virtual int ServMenuId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "MenuId")]
		public virtual int? MenuId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ServicoId")]
		public virtual string ServicoId { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Menu")]
		public virtual Menu Menu { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Servico")]
		public virtual Servico Servico { get; set; }
	

		#endregion

	}
}
