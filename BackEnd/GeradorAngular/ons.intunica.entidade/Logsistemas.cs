using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Logsistemas", Namespace = "http://schemas.ons.org.br/2013/04/Logsistemas")]
	[DataServiceKey("IdLogsistemas")]
	public partial class Logsistemas
	{
	
		public Logsistemas()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdLogsistemas")]
		public virtual int IdLogsistemas { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataAcesso")]
		public virtual DateTime? DataAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarId")]
		public virtual string UsuarId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PerfilId")]
		public virtual string PerfilId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "CosId")]
		public virtual string CosId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "AgeId")]
		public virtual string AgeId { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Perfil")]
		public virtual Perfil Perfil { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Usuar")]
		public virtual Usuar Usuar { get; set; }
	

		#endregion

	}
}
