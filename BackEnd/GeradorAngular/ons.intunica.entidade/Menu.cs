using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:02
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Menu", Namespace = "http://schemas.ons.org.br/2013/04/Menu")]
	[DataServiceKey("MenuId")]
	public partial class Menu
	{
	
		public Menu()
		{
			PerfMenuLista = new List<PerfMenu>();
			ServMenuLista = new List<ServMenu>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "MenuId")]
		public virtual int MenuId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "MenuIdPai")]
		public virtual int? MenuIdPai { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Ordem")]
		public virtual Int16? Ordem { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Tipo")]
		public virtual string Tipo { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Texto")]
		public virtual string Texto { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PerfMenuLista")]
		public virtual IList<PerfMenu> PerfMenuLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "ServMenuLista")]
		public virtual IList<ServMenu> ServMenuLista { get; set; }
	

		#endregion

	}
}
