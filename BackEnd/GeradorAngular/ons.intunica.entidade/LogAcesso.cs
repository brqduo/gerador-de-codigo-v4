using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "LogAcesso", Namespace = "http://schemas.ons.org.br/2013/04/LogAcesso")]
	[DataServiceKey("IdLogAcesso")]
	public partial class LogAcesso
	{
	
		public LogAcesso()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdLogAcesso")]
		public virtual int IdLogAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataAcesso")]
		public virtual DateTime? DataAcesso { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "NomeLogin")]
		public virtual string NomeLogin { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "FlgAcesso")]
		public virtual string FlgAcesso { get; set; }
	

		#endregion

	}
}
