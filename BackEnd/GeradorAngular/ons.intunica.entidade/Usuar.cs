using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
	/// White Brasil - Code Generation: 2019/01/23 10:48:04
	/// </remarks>
	[Serializable()] 
	[DataContract(Name = "Usuar", Namespace = "http://schemas.ons.org.br/2013/04/Usuar")]
	[DataServiceKey("UsuarId")]
	public partial class Usuar
	{
	
		public Usuar()
		{
			PerfUsuLista = new List<PerfUsu>();
			PermissaoLista = new List<Permissao>();
			LogsistemasLista = new List<Logsistemas>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarId")]
		public virtual string UsuarId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PrivilegioId")]
		public virtual string PrivilegioId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarNome")]
		public virtual string UsuarNome { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarSenha")]
		public virtual string UsuarSenha { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarEmail")]
		public virtual string UsuarEmail { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UsuarSenhaN")]
		public virtual string UsuarSenhaN { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdTipoStatususuar")]
		public virtual int? IdTipoStatususuar { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "DataStatususuar")]
		public virtual DateTime? DataStatususuar { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "TipoStatususuar")]
		public virtual TipoStatususuar TipoStatususuar { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PerfUsuLista")]
		public virtual IList<PerfUsu> PerfUsuLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PermissaoLista")]
		public virtual IList<Permissao> PermissaoLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "LogsistemasLista")]
		public virtual IList<Logsistemas> LogsistemasLista { get; set; }
	

		#endregion

	}
}
