using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Servidorsistema", Namespace = "http://schemas.ons.org.br/2013/04/Servidorsistema")]
	[DataServiceKey("IdServidorsistema")]
	public partial class Servidorsistema
	{
	
		public Servidorsistema()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdServidorsistema")]
		public virtual int IdServidorsistema { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "SistemaId")]
		public virtual string SistemaId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "UrlServidor")]
		public virtual string UrlServidor { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Sistema")]
		public virtual Sistema Sistema { get; set; }
	

		#endregion

	}
}
