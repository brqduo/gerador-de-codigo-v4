using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/02/08 16:41:08
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Age", Namespace = "http://schemas.ons.org.br/2013/04/Age")]
	[DataServiceKey("AgeId")]
	public partial class Age
	{
	
		public Age()
		{
            PerfUsuLista = new List<PerfUsu>();
        }


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "AgeId")]
		public virtual string AgeId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdoAge")]
		public virtual string IdoAge { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "GrpecoId")]
		public virtual string GrpecoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "PaisId")]
		public virtual string PaisId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Dtentrada")]
		public virtual DateTime? Dtentrada { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Dtdesativa")]
		public virtual DateTime? Dtdesativa { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Nomelongo")]
		public virtual string Nomelongo { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "NomeCurto")]
		public virtual string NomeCurto { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Nomedes")]
		public virtual string Nomedes { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "TipoorgaoId")]
		public virtual int? TipoorgaoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdoOns")]
		public virtual string IdoOns { get; set; }

        /// <summary>
        /// [relation1xNTables]
        ///	
        ///</summary>
        [DataMember(Name = "PerfUsuLista")]
        public virtual IList<PerfUsu> PerfUsuLista { get; set; }

        #endregion

    }
}
