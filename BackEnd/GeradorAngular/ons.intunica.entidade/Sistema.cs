using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.intunica.entidade
{

	/// <summary>
	/// 
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2019/01/23 10:48:03
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Sistema", Namespace = "http://schemas.ons.org.br/2013/04/Sistema")]
	[DataServiceKey("SistemaId")]
	public partial class Sistema
	{
	
		public Sistema()
		{
			ModuloLista = new List<Modulo>();
			PerfilLista = new List<Perfil>();
			ServidorsistemaLista = new List<Servidorsistema>();
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "SistemaId")]
		public virtual string SistemaId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "Descricao")]
		public virtual string Descricao { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "EmailResp")]
		public virtual string EmailResp { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "ServicoId")]
		public virtual string ServicoId { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "FlgLoginrequerido")]
		public virtual string FlgLoginrequerido { get; set; }
		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "FlgDesativado")]
		public virtual string FlgDesativado { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	
		///</summary>
		[DataMember(Name = "Servico")]
		public virtual Servico Servico { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "ModuloLista")]
		public virtual IList<Modulo> ModuloLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "PerfilLista")]
		public virtual IList<Perfil> PerfilLista { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	
		///</summary>
		[DataMember(Name = "ServidorsistemaLista")]
		public virtual IList<Servidorsistema> ServidorsistemaLista { get; set; }
	

		#endregion

	}
}
