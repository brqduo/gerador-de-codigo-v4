﻿using ons.intunica.comum.ioc;
using GeradorAngular.ioc;
using ons.intunica.servico.ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GeradorAngular
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            if (log4net.LogManager.GetCurrentLoggers().Length == 0)
            {
                log4net.Config.XmlConfigurator.Configure();
            }

            //IoC.Initialize(new NegocioContainer(new DataConfiguration()));
            IoC.Initialize(new WebContainer());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(IoC.Kernel));

        }
    }
}
